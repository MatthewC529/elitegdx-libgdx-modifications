package com.treehouseelite.elitegdx.map.pixel;

import java.lang.reflect.InvocationTargetException;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.treehouseelite.elitegdx.entity.SimpleEntity;
import com.treehouseelite.elitegdx.math.Maths;
import com.treehouseelite.elitegdx.util.Colour;

public class PixmapMap {

	public final ArrayMap<Integer, PixelDataPacket<?>> indicate = new ArrayMap<Integer, PixelDataPacket<?>>();
	private Pixmap mapImage;
	private int tileSize;

	public PixmapMap(final FileHandle mapHandle, final int standardImageSize){
		if(!Maths.isPowerOfTwo(standardImageSize)) throw new IllegalStateException("Image Size must be a Power of Two! As Required by OpenGL!");
		mapImage = new Pixmap(mapHandle);
		tileSize = standardImageSize;
	}

	public void newMap(final FileHandle mapHandle,final int tileSize){
		if(!Maths.isPowerOfTwo(tileSize)) throw new IllegalStateException("Tile Size must be a power of two! As Required by OpenGL!");
		mapImage = new Pixmap(mapHandle);
		this.tileSize = tileSize;
	}

	public void newMap(final FileHandle mapHandle){
		newMap(mapHandle, tileSize);
	}

	public void addIndicator(Colour indication, PixelDataPacket<?> packet){
		indicate.put(indication.getRGBA8888(), packet);
	}

	public void clearIndicator(Colour indicator){
		indicate.removeIndex(indicator.getRGBA8888());
	}

	public void clearAllIndicators(){
		indicate.clear();
	}

	public Array<RenderPacket> getRenderPackets(){
		Array<RenderPacket> temp = new Array<RenderPacket>();

		for(int y = 0; y < mapImage.getHeight() ; y++){
			for(int x = 0; x < mapImage.getWidth() ; x ++){
				int pix8 = mapImage.getPixel(x, y);
				if(indicate.containsKey(pix8)){
					PixelDataPacket<?> packet = indicate.get(pix8);
					temp.add(new RenderPacket(new Vector2(x * tileSize, y * tileSize), packet));
				}
			}
		}

		return temp;
	}

	public void renderItems(SpriteBatch batch){
		if(!batch.isDrawing()) throw new IllegalStateException("SpriteBatch must be started before rendering!");
		else{
			for(int y = 0; y < mapImage.getHeight() ; y++){
				for(int x = 0; x < mapImage.getWidth() ; x ++){
					int pix8 = mapImage.getPixel(x, y);
					if(indicate.containsKey(pix8)){
						Class<?> clazz = indicate.get(pix8).clazz;
						PixelDataPacket<?> packet = indicate.get(pix8);
						try {
							((SimpleEntity)clazz.getConstructor(packet.parameterTypes).newInstance(packet.parameters[0] == null  || packet.parameters[0].getClass() == Texture.class ? new Object[]{x * tileSize, y * tileSize,packet.parameters[0]} : assembleParamList(packet.parameters, x , y))).draw(batch);
						} catch (Exception e){
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	private Object[] assembleParamList(Object[] params, float x, float y){
		Object[] temp = new Object[params.length + 2];
		temp[0] = x * tileSize;
		temp[1] = y * tileSize;
		for(int i = 2; i < temp.length; i++){
			temp[i] = params[i-2];
		}

		return temp;
	}

}
