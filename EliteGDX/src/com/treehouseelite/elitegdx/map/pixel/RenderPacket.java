package com.treehouseelite.elitegdx.map.pixel;

import com.badlogic.gdx.math.Vector2;

public class RenderPacket {

	public final PixelDataPacket<?> pixelData;
	public final Vector2 pos;

	public RenderPacket(Vector2 Pos, PixelDataPacket<?> packet){
		pixelData = packet;
		this.pos = Pos;
	}

	public RenderPacket(float x, float y, PixelDataPacket<?> packet){
		pixelData = packet;
		this.pos = new Vector2(x, y);
	}

}
