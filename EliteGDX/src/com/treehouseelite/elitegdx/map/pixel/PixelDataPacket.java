package com.treehouseelite.elitegdx.map.pixel;

import com.badlogic.gdx.graphics.Texture;

public class PixelDataPacket<T> {

	public final Class<T> clazz;
	public Class<?>[] parameterTypes = {float.class, float.class, Texture.class};
	public Object[] parameters = {null};

	public PixelDataPacket(Class<T> clazz, Texture tex){
		this.clazz = clazz;
		parameters[0] = tex;
	}

	public PixelDataPacket setParamTypes(Class<?>... types){
		parameterTypes = types;
		return this;
	}

	public PixelDataPacket setParams(Object... params){
		parameters = params;
		return this;
	}
}
