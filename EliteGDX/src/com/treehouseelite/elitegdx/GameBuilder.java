package com.treehouseelite.elitegdx;

import com.badlogic.gdx.Game;
import com.treehouseelite.elitegdx.util.Referenceable;

/**
 * A class that can be used ONCE in place of Manager, it will allow for easy initialization through <br />
 * a simple Builder method chain.
 *  
 * @author Matthew Crocco
 */
public final class GameBuilder {


	private static boolean used = false;
	private static boolean created = false;

	public GameBuilder(){
		if(created) throw new IllegalStateException("GameBuilder Already in Use!");
		if(used) throw new IllegalStateException("GameBuilder Already Used!");

		created = true;
	}


	/**
	 * Quick Initialize using a Referenceable Reference File and Game instance
	 * @param ref
	 * @param game
	 */
	public void initialize(Referenceable ref, Game game){
		if(used) throw new IllegalStateException("GameBuilder Already Used!");

		Manager.init(ref, game);
		used = true;
	}

	/**
	 * Set Log Directory
	 * @param path
	 * @return
	 */
	public GameBuilder setLogDirectory(String path){
		if(used) throw new IllegalStateException("GameBuilder Already Used!");

		Manager.LDIR = path;
		return this;
	}



}
