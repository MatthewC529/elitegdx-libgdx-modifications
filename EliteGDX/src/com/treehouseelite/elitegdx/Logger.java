package com.treehouseelite.elitegdx;

import java.io.BufferedWriter;   
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

import lombok.EqualsAndHashCode;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.treehouseelite.elitegdx.ex.LoggerException;
import com.treehouseelite.elitegdx.math.Maths;
import com.treehouseelite.elitegdx.util.Analyzable;
import com.treehouseelite.elitegdx.util.Reference;
import com.treehouseelite.elitegdx.util.StringUtils;

//TODO Migrate to Apache Commons File IO. It is so much cleaner!
/**
 * An abstracted Logging System developed ideally for Treehouse Elite games but
 * can be used<br />
 * for any type of software from calculators to games. <br />
 * <br />
 * Now supports writing Exceptions in full and has had its logging methods
 * rewritten and simplified. <br />
 * <br />
 * 
 * @author Matthew Crocco (Treehouse Elite)
 * @version Elite Logging System v2.1
 * @since May 25th, 2014
 */
public final @EqualsAndHashCode class Logger {

	protected Logger(String projectName, String Version, String logDir) {
		init(projectName, Version, logDir);
	}

	public static enum Level {
		STAT("[STAT]"), INFO("[INFO]"), ALERT("[ALERT]"), SEVERE("[!SEVERE!]"), FINE(
				"[FINE]"), FINER("[FINER]"), FINEST("[FINEST]"), FATAL(
						"[!!!FATAL!!!]"), DEV("[DEV]"), NECESSARY("[FORCED]"), BLANK(
								"blank");

		private final String d;
		private static final Array<Level> levels = new Array<Level>();

		static {
			levels.addAll(Level.values());
		}

		private Level(String desc) {
			d = desc;
		}

		public String getDescription() {
			return d;
		}

		public static Array<Level> getList() {
			return levels;
		}
	}

	private static enum DevMsg {
		COKE("\"This code needs refactoring... get on that?\"", 0), ADEPT(
				"\"Merry Christmas! If its Christmas... and you celebrate it...\"",
				1), SIGNATURE(
						"\"Please dont Close this document! It gets dark...\"", 2), WILLIAM(
								"\"Never Forget: William The First\"", 3), NAMES(
										"\"All of my workers are random... really I promise...\"", 4), WHY(
												"\"I do this because its fun and Notch did it...\"", 5);

		String msg;
		int assign;

		private DevMsg(String joke, int assignment) {
			msg = joke;
			assign = assignment;
		}

		public String getMsg() {
			return msg;
		}
	}

	private static File dir = new File("elogs");
	private static File statDir;
	public static final String suffix = ".elog", statsuffix = ".estat";
	private static final int MAX_FILE_COUNT = 10;
	private static File currentLog;
	private static File currentStat;
	private static Level[] debugDepth = { Level.STAT, Level.INFO,
		Level.ALERT, Level.FINE, Level.FINER, Level.FINEST,
		Level.SEVERE, Level.FATAL };
	private static String fSep = System.getProperty("file.separator");
	public static String pName = Manager.LIB_NAME;
	public static String pVersion = Manager.LIB_VERSION;
	public static String pAcronym = "ELOG";
	public static String userDir = System.getProperty("user.home");

	// My Workers... they keep to themselves ;)
	private static BufferedWriter Tom, StatWriter;

	private static boolean sleeping = false;
	private static AtomicBoolean disposed = new AtomicBoolean(false);

	static{
		fSep = System.getProperty("file.separator");
	}

	/**
	 * Begins the Initialization Process for the Logging System, creating the Directories and Files (As well as some Housekeeping)
	 * 
	 * @param project
	 *            - Project Name
	 * @param version
	 *            - Project Version
	 * @param logDir
	 *            - Desired Log Directory (May be rejected and default to
	 *            ~\*User_Home_Directory*\*game_directory*\elogs)
	 */
	public static void init(final String project, final String version, final String logDir) {
		pName = project;
		pVersion = version;
		pAcronym = StringUtils.makeAcronym(pName, " ");
		if (logDir != null)
			dir = new File(logDir);
		else
			dir = new File(String.format("%s/%s_%s/logs", userDir, project.length() > 18 ? pAcronym : project, version));

		statDir = new File(dir.getParentFile(), "stat");
		statDir.mkdirs();

		try{init();}catch(IOException e){e.printStackTrace();}
	}

	/**
	 * Put Log through initialization process
	 * @throws IOException 
	 */
	public static void init() throws IOException {
		System.out.println(StringUtils.separator(27, "ELITE LOG"));
		System.out.println(String.format("Initializing %s now...", Logger.class.getName()));
		checkDir();
		currentLog = getWorkableFile();
		checkFilePopulation(".elog", 10);
		Tom = new BufferedWriter(new FileWriter(currentLog));
		Tom.flush();

		wakeUpWorkers();

		Tom.write(StringUtils.separator(90, '='));
		Tom.newLine();
		Tom.write(String.format("Game: %s -- Version: %s -- C: %s\r\n", Manager.getProjectName(), Manager.getProjectVersion(), Manager.getCopyright()));
		Tom.write(StringUtils.separator(90, '='));
		Tom.newLine();
		Tom.write("Dev Message: " + getRandomMessage());
		Tom.newLine();
		Tom.write(StringUtils.separator(90, '='));
		Tom.flush();

		log("Initialization of EliteGDX Logging System Complete! - Tom", Level.FINEST);
		System.out.println(StringUtils.separator(27, "ELITE LOG"));
	}

	private static boolean checkDir() {
		System.out.println("Checking if our home is there...");
		if (dir.exists())
			return true;
		else {
			System.out.println("NOPE!... Building home at "
					+ dir.getAbsolutePath() + "...");
			dir.mkdirs();
			return true;
		}
	}
	/**
	 * Makes an overcomplicated name for a simple log file because why not?
	 * 
	 * @return The New Log File
	 */
	private static File getWorkableFile() {
		System.out.println("Assembling overly complicated log file for this instance...");
		Random r = new Random(System.currentTimeMillis());
		DateFormat date = new SimpleDateFormat("MM-dd-yyyy");
		DateFormat time = new SimpleDateFormat("HH#mm#ss");
		Calendar c = Calendar.getInstance();
		Date curTime = c.getTime();
		String name = String.format("%s_%s__%s---%s%s", pAcronym, date.format(curTime), time.format(curTime), (int)(r.nextInt(999999) * Maths.abs(r.nextGaussian())), suffix);
		System.out.println("Well we decided to name it " + name + "...");
		return new File(dir, name);
	}

	public static void initializeStats(){
		String name = currentLog.getName().substring(0, currentLog.getName().lastIndexOf('.')) + ".estat";
		currentStat = new File(statDir + fSep + name);
		try {
			checkFilePopulation(".estat", 5);
			currentStat.createNewFile();
			StatWriter = new BufferedWriter(new FileWriter(currentStat));
			StatWriter.write(StringUtils.separator(90, '='));
			StatWriter.newLine();
			StatWriter.write(String.format("Game: %s -- Version: %s -- C: %s\r\n", Manager.getProjectName(), Manager.getProjectVersion(), Manager.getCopyright()));
			StatWriter.write(StringUtils.separator(90, '='));
			StatWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void checkFilePopulation(final String suffix, int limit){
		if((suffix.equals(".estat") ? currentStat : currentLog) == null || (suffix.equals(".estat") ? !currentStat.getParentFile().exists() : !currentLog.getParentFile().exists())) return;
		
		File dir = (suffix.equals(".estat") ? currentStat.getParentFile() : currentLog.getParentFile());
		Array<FileInfo> files = new Array<FileInfo>(true, 30, FileInfo.class);
		FilenameFilter f = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.contains(suffix);
			}
		};
		
		for(File entry: dir.listFiles(f))
			files.add(new FileInfo(entry));
		
		while(files.size >= limit)
			files.pop().file.delete();
	}

	private static boolean wakingUp = false;

	/**
	 * Awakens the workers if they were put to sleep, also instantiates the
	 * workers in the initialization process. If the workers are NOT waking up
	 * then a instantiation message is written to the log file.
	 */
	private static void wakeUpWorkers() {
		System.out.println("Waking up Bob, Tom and the Intern...");
		try {
			if (!wakingUp)
				Tom = new BufferedWriter(new FileWriter(currentLog, false));
			else{
				Tom = new BufferedWriter(new FileWriter(currentLog, true));
				if(StatWriter != null)
					StatWriter = new BufferedWriter(new FileWriter(currentStat, true));
			}
			Tom.flush();
		} catch (IOException e) {
			e.printStackTrace(System.out);
			throw new LoggerException("Failure to Wake Up Workers in " + Logger.class.getSimpleName() + "!");
		}
	}

	/**
	 * For possible times when the program is force closed and you dont need a
	 * log system, prevents perma-lock of files. This is for the VERY thorough
	 * since the files can also be released through the task manager but this
	 * prevents it from happening ever, if you dont put them to sleep the
	 * program may have issues deleting log files when the program is closed
	 * abruptly!
	 */
	public static void sleep() {
		try {
			log("sleeping...", Level.FINEST);
			Tom.close();
			StatWriter.close();
			sleeping = true;
		} catch (IOException e) {
			e.printStackTrace();
			throw new LoggerException("Failure to Put Workers to Sleep!");
		}

	}

	/**
	 * Used after sleep function to wake the writer, reader and scanner back up.
	 * Refer to Sleep documentation for why this exists. A call to this method
	 * when sleep isnt used will result in nothing happening.
	 * 
	 * @return True if it can occur, false if not.
	 */
	public static boolean wakeUp() {
		if (!areWorkersAvailable()) {
			wakingUp = true;
			sleeping = false;
			wakeUpWorkers();
			return true;
		} else
			return false;
	}

	/**
	 * use log method
	 */
	@Deprecated
	public static boolean write(String msg, Level level, String... commands) {
		log(msg, level, commands);
		return true;
	}

	/** Use Log Method */
	@Deprecated
	public static boolean write(String msg, Level level) {
		return write(msg, level, "NONE");
	}

	/** Use Log Method */
	@Deprecated
	public static boolean write(String msg) {
		return write(msg, Level.INFO, "NONE");
	}

	/**
	 * USE THIS INSTEAD OF WRITE <br />
	 * <br />
	 * Log a message to the current log file and the console with the Tag of the
	 * LogLevel passed in. <br />
	 * Currently does not support commands except for NONE and null!<br />
	 * <br />
	 * COMMANDS AND STRING FORMATTING IS CURRENT UNSUPPORTED AS OF 2/21/14 <br />
	 * THIS WILL BE FIXED SOON NOW THAT I HAVE TIME! <br />
	 * <br />
	 * 
	 * @param msg
	 *            - Message to Print with the [INFO] Tag
	 */
	public static void log(Object msg) {
		log(msg, Level.INFO);
	}

	/**
	 * USE THIS INSTEAD OF WRITE <br />
	 * <br />
	 * Log a message to the current log file and the console with the Tag of the
	 * LogLevel passed in. <br />
	 * Currently does not support commands except for NONE and null!<br />
	 * <br />
	 * COMMANDS AND STRING FORMATTING IS CURRENT UNSUPPORTED AS OF 2/21/14 <br />
	 * THIS WILL BE FIXED SOON NOW THAT I HAVE TIME! <br />
	 * <br />
	 * 
	 * @param msg
	 *            - Message to Print
	 * @param level
	 *            - LogLevel to Use for the Tag (i.e. [INFO], [STAT], etc.)
	 */
	public static void log(Object msg, Level level) {
		log(msg, level, "NONE");
	}

	/**
	 * USE THIS INSTEAD OF WRITE <br />
	 * <br />
	 * Log a message to the current log file and the console with the Tag of the
	 * LogLevel passed in. <br />
	 * Currently does not support commands except for NONE and null!<br />
	 * <br />
	 * COMMANDS AND STRING FORMATTING IS CURRENT UNSUPPORTED AS OF 2/21/14 <br />
	 * THIS WILL BE FIXED SOON NOW THAT I HAVE TIME! <br />
	 * <br />
	 * 
	 * @param msg
	 * @param level
	 * @param commands
	 */
	public static void log(Object msg, Level level, String... commands) {
		String[] comms = commands;
		if(disposed.get()) return;
		if (areWorkersAvailable()) {
			try {
				if (!comms[0].equalsIgnoreCase("NONE") || comms[0] != null) {
				} // TODO Commands

				if(properLevel(debugDepth, level) && level == Level.STAT){
					if(currentStat == null)
						initializeStats();

					StatWriter.newLine();
					StatWriter.write(assembleLogText(msg.toString(), level));
					StatWriter.flush();
					currentStat.setLastModified(System.currentTimeMillis());
				}else if (properLevel(debugDepth, level) && !level.equals(Level.BLANK)) {
					Tom.newLine();
					Tom.write(assembleLogText(msg.toString(), level));
					Tom.flush();
					System.out.println(assembleLogText(msg.toString(), level));
					currentLog.setLastModified(System.currentTimeMillis());
				} else if (Level.BLANK.equals(level)) {
					Tom.newLine();
					Tom.flush();
					currentLog.setLastModified(System.currentTimeMillis());
				}
			} catch (IOException e) {
				e.printStackTrace(System.out);
				throw new LoggerException("Failure to Write to Log!");
			}
		}
	}

	/**
	 * Finalizer for EliteLog, Logs a Final message and disposes of the
	 * necessary objects and closes all open streams.
	 */
	public static final void finishUp() {
		disposed.set(true);
		try {
			log("Elite Log System Finalized Successfully!",Level.FINEST);
			Tom.newLine();
			Tom.write(StringUtils.separator(90, '='));
			Tom.newLine();
			Tom.write(String.format("%s v%s -- %s\r\n", Reference.LOGGING_SYSTEM, Reference.LOGGING_VERSION, Reference.COPYRIGHT));
			Tom.write(StringUtils.separator(90, '='));
			Tom.flush();
			Tom.close();

			if(StatWriter != null){
				StatWriter.write(StringUtils.separator(90, '='));
				StatWriter.newLine();
				StatWriter.write(String.format("%s v%s -- %s\r\n", Reference.LOGGING_SYSTEM, Reference.LOGGING_VERSION, Reference.COPYRIGHT));
				StatWriter.write(StringUtils.separator(90, '='));
				StatWriter.flush();
				StatWriter.close();
			}
		} catch (IOException e) {
			System.err.println(Logger.class.getSimpleName() + " -- There was an error... but I wouldnt worry about it...");
			System.err.println(Logger.class.getSimpleName() + " -- Tom was sleeping and I spilled coffee on him and he refused to write, he will be fine soon.");
		}
	}

	/**
	 * Assembles The Message to Print to the Log File, attaches the level, date
	 * and time.
	 * 
	 * @param msg
	 *            - String to print
	 * @param level
	 *            - Debug Level
	 * @return String to print to log
	 */
	private static String assembleLogText(String msg, Level level) {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy -- HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		return String.format("%s : %s -- %s", df.format(cal.getTime()),
				level.getDescription(), msg);
	}

	/**
	 * Gets Random, PreSet, Dev Messages. Akin to the Minecraft Title Menu
	 * Messages
	 * 
	 * @return Random Dev Message
	 */
	private static String getRandomMessage() {
		Random dice = new Random();
		final int num = dice.nextInt(6);
		String msg = DevMsg.NAMES.getMsg();
		Iterator<DevMsg> dev= Arrays.asList(DevMsg.values()).iterator();
		
		while(dev.hasNext()){
			DevMsg m = dev.next();
			if(m.assign == num){
				msg = m.msg;
				break;
			}
		}

		return msg;
	}

	/**
	 * Returns the instance of the log file current being used.
	 * 
	 * @return Current Log File Instance
	 */
	public static final File getCurrentFile() {
		return currentLog;
	}

	/**
	 * Returns the Current Log File's Name
	 * 
	 * @return - Current Log File's Name
	 */
	public static final String getCurrentFileName() {
		return currentLog.getName();
	}

	/**
	 * Returns the Absolute Path of the Log File currently in use.
	 * 
	 * @return Absolute Path of Current Log File
	 */
	public static final String getCurrentFilePath() {
		return currentLog.getAbsolutePath();
	}

	/**
	 * Marks a file as ERRORED or LOCKED if a process fails.
	 * 
	 * @param file
	 *            - File to Mark
	 * @param mark
	 *            - The Mark
	 */
	private static void mark(File file, String mark) {
		String suf = "";
		if (mark.equalsIgnoreCase("locked"))
			suf = ".LOCKEDLOG";
		file.renameTo(new File(file.getName().substring(0,
				file.getName().lastIndexOf('.'))
				+ " " + mark.toUpperCase() + ".elog" + suf));
	}

	public static final boolean areWorkersAvailable() {
		return !sleeping;
	}

	/**
	 * Checks if the message is appropriate for the assigned depth. The Depth is
	 * ALL by default.
	 * 
	 * @param depth
	 * @param level
	 * @return
	 */
	public static final boolean properLevel(Level[] depth, Level level) {
		if (depth.length == 8)
			return true;
		for (Level d : depth)
			if (d == level)
				return true;

		return false;
	}

	/**
	 * Sets debug depth, used to determine if a log message should be written
	 * (if it is appropriate according to the depth). For Example, a message
	 * with a level of ALERT will not be printed to the log if ALERT is not in
	 * the Debug Depth array.
	 * 
	 * @param levels
	 *            - The appropriate debug levels
	 */
	public static final void setDebugDepth(String[] levels) {
		ArrayList<Level> tempdepth = new ArrayList<Level>();
		tempdepth.add(Level.DEV);
		tempdepth.add(Level.NECESSARY);
		for (String s : levels) {
			for (int i = 0; i < Level.getList().size; i++) {
				if (Level.getList().get(i).getDescription().contains(s)) {
					tempdepth.add(Level.getList().get(i));
					break;
				}
			}
		}
		debugDepth = tempdepth.toArray(new Level[tempdepth.size()]);
		System.out.println("Completed assigning the debug levels...");
		printLevels(levels);
	}

	/**
	 * Debug text to check what levels are being assigned.
	 * 
	 * @param desc
	 */
	private static void printLevels(String[] desc) {
		boolean wasSleeping = false;
		StringBuilder sb = new StringBuilder("Debug Depth: ");
		if (sleeping) {
			wakeUp();
			write("Temporarily waking up workers to print debug depth...",
					Level.FINEST);
			wasSleeping = true;
		}

		for (String s : desc) {
			System.out.print(s + ", ");
			if (s.equalsIgnoreCase(desc[desc.length - 1]))
				sb.append(s).append(" ");
			else
				sb.append(s).append(", ");
		}

		System.out.println("assigned...");
		write(sb.append("assigned...").toString(), Level.FINEST);

		if (wasSleeping)
			sleep();
	}

	public static File getLogDirectory() {
		return dir;
	}

	@Deprecated
	public static void writeException(Exception e) {
		exception(e);
	}

	/**
	 * Print a Severe Log Message, Equivalent to err(msg)
	 * 
	 * @param msg
	 *            - Message to Print with the [SEVERE] log tag
	 */
	public static void error(Object msg) {
		log(msg, Level.SEVERE);
	}

	/**
	 * Print a Severe Log Message, Equivalent to error(msg)
	 * 
	 * @param msg
	 *            - Message to Print with the [SEVERE] log tag
	 */
	public static void err(Object msg) {
		log(msg, Level.SEVERE);
	}

	/**
	 * Print an Exception in Full, Equivalent to err(e) and exception(e)
	 * 
	 * @param e
	 *            - Exception to Print in Full
	 */
	public static void error(Exception e) {
		exception(e);
	}

	/**
	 * Print an Exception in Full, Equivalent to error(e) and exception(e)
	 * 
	 * @param e
	 *            - Exception to Print in Full
	 */
	public static void err(Exception e) {
		exception(e);
	}

	/**
	 * Prints an Exception in Full, Equivalent to error(e) and err(e)
	 * 
	 * @param e
	 *            - Exception to Print in Full
	 */
	public static void exception(Exception e) {
		System.err.println(e.getLocalizedMessage());
		System.err.println("\nEXCEPTION ~~~ \n");
		e.printStackTrace(System.out);
		System.err.println("\nEXCEPTION ~~~ \n");
		Logger.log("", Level.BLANK);
		Logger.log("EXCEPTION ~~~ ", Level.DEV);
		Logger.log(e.getLocalizedMessage(), Level.NECESSARY);
		for (StackTraceElement element : e.getStackTrace()) {
			System.err.println(element.toString());
			Logger.log(element.toString(), Level.NECESSARY);
		}
		Logger.log("EXCEPTION ~~~ ", Level.DEV);
		Logger.log("", Level.BLANK);
	}

	public static void alert(Object msg){
		log(msg, Level.ALERT);
		System.err.println("[WARN] -- " + msg);
	}

	public static void warn(Object msg){
		alert(msg);
	}

	public static void warning(Object msg){
		alert(msg);
	}

	public static void info(Object msg){
		log(msg, Level.INFO);
	}

	public static void stat(Object msg){
		log(msg, Level.STAT);
	}

	public static void statistic(Object msg){
		log(msg, Level.STAT);
	}

	public static void dev(Object msg){
		log(msg, Level.DEV);
	}

	public static void necessary(Object msg){
		log(msg, Level.NECESSARY);
	}

	public static void blank(Object msg){
		log(msg, Level.BLANK);
	}

	public static void sev(Object msg){
		log(msg, Level.SEVERE);
		System.err.println("[SEVERE] -- " + msg);
	}

	public static void severe(Object msg){
		sev(msg);
	}

	public static void fine(Object msg){
		log(msg, Level.FINE);
	}

	public static void finer(Object msg){
		log(msg, Level.FINER);
	}

	public static void finest(Object msg){
		log(msg, Level.FINEST);
	}

	public static void fatal(Object msg){
		log(msg, Level.FATAL);
		System.err.println("[!!FATAL!!] -- " + msg);
	}

	public static void stat(Analyzable obj){
		log(obj.getClass().getSimpleName()+".class | "+obj.getPrintableStatistics(), Level.STAT);
	}

	public static void statistic(Analyzable obj){
		log(obj.getClass().getSimpleName()+".class | "+obj.getPrintableStatistics(), Level.STAT);
	}

	public static boolean isDead(){
		return disposed.get();
	}
	
	private static class FileInfo implements Comparable<FileInfo>{
		public final File file;
		public final long age;
		
		public FileInfo(File f){
			this.file = f;
			this.age = f.lastModified();
		}
		
		@Override
		public int compareTo(FileInfo o) {
			if(o.age == age) return 0;
			if(o.age > age) return -1;
			
			return 1;
		}
		
	}
	
}
