package com.treehouseelite.elitegdx;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.treehouseelite.elitegdx.math.Maths;

/**
 * A Nearly Complete Animator using LibGDX Functions. <br />
 * <br />
 * A Static Class that creates a LibGDX animation for you or can provide you the <br />
 * KeyFrames as TextureRegions from your Sprite Sheet.
 * 
 * @author Matthew Crocco
 * @since Wednesday April 16th, 2014
 * 
 */
public final class Animator extends Throwable {
	protected static final long serialVersionUID = -158691345161L;

	private static int NUM_COL, NUM_ROWS;
	private static Texture sheet;
	private static TextureRegion[] keyFrames;
	private static int frameSize = 32; // Defaults to 32x32 tiles

	/** DO NOT INSTANTIATE **/
	private Animator() {}

	/**
	 * Creates and returns KeyFrames (TextureRegion Array) from a SpriteSheet
	 * 
	 * @param SpriteSheet
	 *            - Sprite Sheet Texture
	 * @param columns
	 *            - Number of Columns to Cut
	 * @param rows
	 *            - Number of Rows to Cut
	 * @param FrameSize
	 *            - Size of Frames to Cut (Power of 2)
	 * @return Animation KeyFrames (TextureRegion Array)
	 * @see com.badlogic.gdx.graphics.g2d.TextureRegion TextureRegion
	 */
	public static TextureRegion[] createKeyFrames(Texture SpriteSheet, int columns, int rows, int FrameSize) {
		frameSize = FrameSize;
		sheet = SpriteSheet;
		if (frameSize == -1 || !Maths.isPowerOfTwo(frameSize))
			throw new IllegalStateException("BAD TILE SIZE -- SpriteSheet Tile Size MUST be a Power Of Two!");
		
		NUM_COL = columns;
		NUM_ROWS = rows;
		TextureRegion[][] temp = TextureRegion.split(sheet, frameSize, frameSize);
		keyFrames = new TextureRegion[NUM_COL * NUM_ROWS];

		int index = 0;
		for (int y = 0; y < NUM_ROWS; y++) {
			for (int x = 0; x < NUM_COL; x++) {
				keyFrames[index] = temp[x][y];
				index++;
			}
		}

		return keyFrames;
	}

	/**
	 * Returns keyFrames from a given Sprite Sheet given the columns and rows,
	 * auto-determines the frame size. <br />
	 * Not Recommended, determines frame size by dividing the width by the
	 * number of columns, if your rows are off then the animation will be off as
	 * well. <br />
	 * In other words: Higher Possibility of Error
	 * 
	 * @param SpriteSheet
	 *            - Sprite Sheet Texture
	 * @param columns
	 *            - Number of Columns
	 * @param rows
	 *            - Number of Rows
	 * @return Key Frames for Animation (TextureRegion Array)
	 * @see com.badlogic.gdx.graphics.g2d.TextureRegion TextureRegion
	 */
	public static TextureRegion[] createKeyFrames(Texture SpriteSheet,int columns, int rows) {
		return createKeyFrames(SpriteSheet,columns,rows, (Maths.isPowerOfTwo(SpriteSheet.getWidth() / columns) && Maths.isPowerOfTwo(SpriteSheet.getHeight() / rows)) ? SpriteSheet.getWidth() / columns : -1);
	}

	/**
	 * Returns keyFrames from a given Sprite Sheet given the Frame Size,
	 * auto-determines the number of rows and columns.
	 * 
	 * @param SpriteSheet
	 *            - Sprite Sheet Texture
	 * @param frameSize
	 *            - Size of Frames to be Cut
	 * @return Key Frames for Animation (TextureRegion Array)
	 * @see com.badlogic.gdx.graphics.g2d.TextureRegion TextureRegion
	 */
	public static TextureRegion[] createKeyFrames(Texture SpriteSheet,int frameSize) {
		return createKeyFrames(SpriteSheet,sliceSheet(SpriteSheet, frameSize)[0],sliceSheet(SpriteSheet, frameSize)[1], frameSize);
	}

	/**
	 * Constructs LibGDX Animation
	 * 
	 * @param SpriteSheet
	 *            - Sprite Sheet Texture
	 * @param columns
	 *            - Number of Columns in Sprite Sheet to cut
	 * @param rows
	 *            - Number of Rows in Sprite Sheet to cut
	 * @param FrameSize
	 *            - Size of the Frames to Cut
	 * @param fps
	 *            - FPS of the Animation
	 * @return LibGDX Animation
	 * @see com.badlogic.gdx.graphics.g2d.Animation Animation.java
	 */
	public static Animation createAnimation(Texture SpriteSheet, int columns,int rows, int FrameSize, float fps) {
		return new Animation(fps, createKeyFrames(SpriteSheet, columns, rows,FrameSize));
	}

	/**
	 * Constructs LibGDX from a given Sprite Sheet, columns and rows.
	 * Auto-determines Frame Size. <br />
	 * Not Recommend, see createKeyFrames for reason.
	 * 
	 * @param SpriteSheet
	 *            - Sprite Sheet Texture
	 * @param columns
	 *            - Number of Columns in Sprite Sheet to Cut
	 * @param rows
	 *            - Number of Rows in Sprite Sheet to Cut
	 * @param fps
	 *            - FPS of the Animation
	 * @return LibGDX Animation
	 * @see com.badlogic.gdx.graphics.g2d.Animation Animation.java
	 * @see com.treehouseelite.elitegdx.Animator#createKeyFrames(com.badlogic.gdx.graphics.Texture,
	 *      int, int) EliteAnimator.createKeyFrames()
	 */
	public static Animation createAnimation(Texture SpriteSheet, int columns,int rows, float fps) {
		return new Animation(fps, createKeyFrames(SpriteSheet, columns, rows,SpriteSheet.getWidth() / columns));
	}

	/**
	 * Constructs LibGDX from a given Sprtie Sheet and Frame Size
	 * 
	 * @param SpriteSheet
	 *            - Sprite Sheet Texture
	 * @param frameSize
	 *            - Size of Frames
	 * @param fps
	 *            - FPS of Animation
	 * @return LibGDX Animation
	 * @see com.badlogic.gdx.graphics.g2d.Animation Animation.java
	 */
	public static Animation createAnimation(Texture SpriteSheet, int frameSize,float fps) {
		if (Maths.isPowerOfTwo(SpriteSheet.getHeight())&& Maths.isPowerOfTwo(SpriteSheet.getWidth()))
			return new Animation(fps, createKeyFrames(SpriteSheet,sliceSheet(SpriteSheet, frameSize)[0],sliceSheet(SpriteSheet, frameSize)[1], frameSize));
		else
			throw new IllegalStateException("BAD TILE SIZE -- Tile Size of Sprite Sheet MUST be a Power Of Two!");
	}

	/**
	 * Create an Animation from a T[] and a given Frames Per Second.
	 * @param keyFrames -- Array of Objects (Extending TextureRegion) creating Animation
	 * @param fps -- Frames Per Second
	 * @return Animation
	 */
	public static <T extends TextureRegion> Animation createAnimation(T[] keyFrames, float fps){
		return new Animation(fps, keyFrames);
	}

	/**
	 * Create an Animation from a given T[], Frames Per Second and Play Mode <br />
	 * The 'T' Object must Extend TextureRegion!
	 * 
	 * @param keyFrames -- Array of Objects (Extending TextureRegion) creating Animation
	 * @param fps -- Frames Per Second
	 * @param mode -- PlayMode to start animation with
	 * @return Animation
	 */
	public static <T extends TextureRegion> Animation createAnimation(T[] keyFrames, float fps, PlayMode mode){
		Array<T> arr = new Array<T>();
		arr.addAll(keyFrames);
		return new Animation(fps, arr, mode);
	}

	/**
	 * Create an Animation from an Array of KeyFrames and a given FPS
	 * @param keyFrames -- Key Frames (Object must Extend Texture Region!)
	 * @param fps -- Frames Per Second
	 * @return Animation
	 */
	public static Animation createAnimation(Array<? extends TextureRegion> keyFrames, float fps){
		return new Animation(fps, keyFrames);
	}

	/**
	 * Create an Animation from an Array of KeyFrames, an FPS and a Play Mode to start with
	 * @param keyFrames -- Key Frames (Object Must Extend TextureRegion!)
	 * @param fps -- Frames Per Second
	 * @param mode -- Play Mode to Start At
	 * @return Animation
	 */
	public static Animation createAnimation(Array<? extends TextureRegion> keyFrames, float fps, PlayMode mode){
		return new Animation(fps, keyFrames, mode);
	}

	/** Returns the Columns and Rows of a Sprite Sheet 
	 * snip snip!*/
	private static int[] sliceSheet(Texture sheet, int frameSize) {
		return new int[] { sheet.getWidth() / frameSize,sheet.getHeight() / frameSize };
	}
}
