package com.treehouseelite.elitegdx.math;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;

@EqualsAndHashCode
@ToString
public class BoundingBox2D implements Serializable, Disposable, CollisionShape<Rectangle>{

	private static final long serialVersionUID = -3773474048446610563L;
	Rectangle r;

	public BoundingBox2D(Sprite sprite){
		r = sprite.getBoundingRectangle();
	}


	public BoundingBox2D set(BoundingBox2D shape) {
		r = shape.r;
		return this;
	}

	public BoundingBox2D set(Rectangle r){
		this.r = r;
		return this;
	}

	@Override
	public boolean contains(Rectangle shape) {
		if(r.contains(shape)) return true;
		return true;
	}

	@Override
	public boolean intersects(Rectangle shape) {
		if(r.overlaps(shape)) return true;
		return false;
	}

	@Override
	public boolean encloses(Rectangle shape){
		Vector2 bLeft = new Vector2(shape.x, shape.y);
		Vector2 tLeft = new Vector2(shape.x, shape.y + shape.height);
		Vector2 bRight = new Vector2(shape.x + shape.width, shape.y);
		Vector2 tRight = new Vector2(shape.x + shape.width, shape.y + shape.height);

		if(r.contains(bLeft) && r.contains(tLeft) && r.contains(bRight) && r.contains(tRight))
			return true;
		else
			return false;
	}

	@Override
	public Rectangle getShape(){
		return r;
	}

	@Override
	public void dispose(){
		r = null;
	}

}
