package com.treehouseelite.elitegdx.math;

public interface CollisionShape<T> {

	T getShape();
	boolean contains(T other);
	boolean intersects(T other);
	boolean encloses(T other);

}
