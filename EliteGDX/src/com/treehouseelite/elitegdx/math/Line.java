package com.treehouseelite.elitegdx.math;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Line {

	private Vector2[] points;
	public final float slope;
	public final Vector2 start, end;
	private int pIndex = 0;

	public Line(Vector2 start, Vector2 end){
		this.start = start;
		this.end = end;

		float dX = end.x - start.x;
		float dY = end.y - start.y;
		slope = dY/dX;

		points = new Vector2[Math.round(dX)];

		if(start.x > end.x){
			for(int i = Math.round(end.x); i < start.x; i++){
				//Calculate Point using Y-Intercept Formula
				//y - start.y = slope * (x - start.x) < -- Point-Slope Formula
				//y = slope * (x - start.x) + start.y < -- Y-Intercept Formula
				points[pIndex] = new Vector2(i, slope * (i - start.x) + start.y);
				pIndex++;
			}
		}else{
			for(int i = Math.round(start.x); pIndex < points.length; i++){
				//Calculate Point using Y-Intercept Formula
				//y - end.y = slope * (x - end.x) < -- Point-Slope Formula
				//y = slope * (x - end.x) + end.y < -- Y-Intercept Formula
				points[pIndex] = new Vector2(i, slope * (i - end.x) + end.y);
				pIndex++;
			}
		}

	}

	public Line(Vector3 start, Vector3 end){
		this(new Vector2(start.x, start.y), new Vector2(end.x, end.y));
	}

	public Vector2[] getPoints(){
		return points;
	}
}
