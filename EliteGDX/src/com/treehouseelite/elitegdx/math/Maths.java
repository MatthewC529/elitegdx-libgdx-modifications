package com.treehouseelite.elitegdx.math;

import java.util.Random;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

@SuppressWarnings("all")
/**
 * An alternative to the java.lang.Math class, uses several bit-wise
 * calculations to conduct mathematical calculations efficiently. <br />
 * Mostly solves Java SE's odd inefficiency when it comes to Trigonometric
 * functions (sine and cosine only at this time, tangent may be added).<br />
 * <br />
 * Also provides possibly more efficient ceil, floor and round methods as well
 * as Pi, Pau, Tau and e in relatively precise float values.<br />
 * <br />
 * More Recently functions for square roots, power of two roots, any logarithm
 * (using Math.log for Change of base), power and so on have been created using
 * pretty efficient methods of calculation albeit relying on recursion for
 * simplification.<br />
 * <br />
 * Only other library used is Java SE and the only instantiation is for a
 * java.util.Random object.
 * 
 * @author Matthew Crocco (Treehouse Elite), Obvious inspiration and
 *         trigonometry code taken from LibGDX Math Utils! (Unknown at time of
 *         creation)
 * 
 */
public final class Maths {

	// Prevent Instantiation
	private Maths() {
	}

	/**Eulers Number*/
	public static final float E = 2.71828182F;

	/**
	 * Defined as the Ratio of a Circles Radius to its Circumference. <br />
	 * 2pi = C/r
	 */
	public static final float TAU = 6.283185302F;

	/**
	 * A rational compromise in the Pi vs Tau debate by Randall Munroe at <a
	 * href="http://xkcd.com/1292/">XKCD</a><br />
	 * Also its an awesome sounding word... and why not include it? <br />
	 * 1.5*pi (Has No Formal Definition)
	 */
	public static final float PAU = 4.71238898F;

	/** The ratio of a circles circumference to its diameter. <br />
	 * pi = C/d */
	public static final float PI = 3.14159265F;

	private static final float degreesToRadians = TAU / 360;
	private static final float degRad = degreesToRadians;
	private static final float radDeg = 360 / TAU;

	private static final int SIN_BITS = 14;
	private static final int SIN_MASK = ~(-1 << SIN_BITS);
	private static final int SIN_COUNT = SIN_MASK + 1;
	private static final float degIndex = SIN_COUNT / 360;
	private static final float radIndex = SIN_COUNT / TAU;

	private static class Sine {
		protected static final float[] valTable = new float[SIN_COUNT];
		static {
			for (int i = 0; i < SIN_COUNT; i++)
				valTable[i] = (float) Math.sin((i + 0.5) / SIN_COUNT * TAU);
			for (int i = 0; i < 360; i += 90)
				valTable[(int) (i * degIndex) & SIN_MASK] = (float) Math.sin(i * degreesToRadians);
		}
	}

	/** Convert degrees to radians */
	public static float toRadians(float degrees) {
		return degrees * degRad;
	}

	/** Convert degrees to radians */
	public static float toRadians(int degrees) {
		return toRadians(degrees);
	}

	/** Convert radians to degrees */
	public static float toDegrees(float radians) {
		return radians * radDeg;
	}

	/** Convert radians to degrees */
	public static float toDegrees(int radians) {
		return toDegrees(radians);
	}

	/** Get the value of sin(radians) in a quick and efficient manner */
	public static float sin(float radians) {
		return Sine.valTable[(int) (radians * radIndex) & SIN_MASK];
	}

	/** Get the value of sin(degrees) in a quick and efficient manner */
	public static float sinD(float degrees) {
		return Sine.valTable[(int) (degrees * degIndex) & SIN_MASK];
	}

	/** Get the value of cos(radians) in a quick and efficient manner */
	public static float cos(float radians) {
		return Sine.valTable[(int) ((radians + PI / 2) * radIndex) & SIN_MASK];
	}

	/** Get value of cos(degrees) in a quick and efficient manner */
	public static float cosD(float degrees) {
		return Sine.valTable[(int) ((degrees + 90) * degIndex) & SIN_MASK];
	}

	public static float tan(float radians) {
		return sin(radians) / cos(radians);
	}

	public static float tanD(float degrees) {
		return sin(toRadians(degrees)) / cos(toRadians(degrees));
	}

	public static float sec(float radians) {
		return 1 / cos(radians);
	}

	public static float csc(float radians) {
		return 1 / sin(radians);
	}

	public static float cot(float radians) {
		return 1 / tan(radians);
	}

	// ----------------------------------------------------------------------

	private static final Random r = new Random();

	/**
	 * Returns a random float between [0.0, 1.0)
	 */
	public static float random() {
		return r.nextFloat();
	}

	/**
	 * Returns a random integer between [0, range] inclusive
	 */
	public static int random(int range) {
		return r.nextInt(range + 1);
	}

	/**
	 * Returns a random integer between [start, end] inclusive
	 */
	public static int random(int start, int end) {
		return start + r.nextInt(end - start + 1);
	}

	/** Returns a random boolean value */
	public static boolean randomBool() {
		return r.nextBoolean();
	}

	/**
	 * Returns true if a random value falls below the given chance. False if
	 * equal to or greater than that chance.<br />
	 * Values must be between [0.0,1.0]
	 */
	public static final boolean randomBool(float pivot) {
		return Maths.random() < pivot;
	}

	/**
	 * Returns a random float between [0, range)
	 * 
	 * @param range
	 *            - max value (exclusive)
	 * @return Random Float
	 */
	public static final float random(float range) {
		return r.nextFloat() * range;
	}

	/**
	 * Get a random value between [start, end)
	 * 
	 * @param start
	 *            - start inclusive
	 * @param end
	 *            - end exclusive
	 * @return Random Float between those two values
	 */
	public static final float random(float start, float end) {
		return start + r.nextFloat() * (end - start);
	}

	// ----------------------------------------------------------------

	// Interesting Bit-Hack I suppose....
	/**
	 * Uses a common bit-hack to efficiently and quickly gather the next highest
	 * Power of Two
	 * 
	 * @param value
	 *            - starting value
	 * @return Next Power Of Two as an int
	 */
	public static final int nextPowerOfTwo(int value) {
		if (value == 0)
			return 1;

		value--;
		value |= value >> 1;
		value |= value >> 2;
		value |= value >> 4;
		value |= value >> 8;
		value |= value >> 16;
	    return value + 1;
	}

	/**
	 * Uses some Bit-wise magic to efficiently return whether or not a value is
	 * a Power of Two (useful for animators and maps which require it!).<br />
	 * Particularly helpful for OpenGL where images are a power of two.
	 * 
	 * @param value
	 *            - value to test
	 * @return True if it is, false if not.
	 */
	public static final boolean isPowerOfTwo(int value) {
		return value != 0 && (value & value - 1) == 0;
	}

	private static final int BIGINT = 16 * 1024;
	private static final double BIGFLOOR = BIGINT;
	private static final double CEILING = 0.99999999;
	private static final double BIGCEIL = 16384.999999999996;
	private static final double BIGROUND = BIGINT + 0.5f;

	/**
	 * Floors a float value, all realistic values work fine but certain extreme
	 * values are affected by some inaccuracy.
	 */
	public static final int floors(float value) {
		return (int) (value + BIGFLOOR) - BIGINT;
	}

	/**
	 * Floors a float value, IT MUST BE POSITIVE, all realistic values work fine
	 * but extreme values can be somewhat inaccurate.
	 */
	public static final int floorPositive(float value) {
		return (int) value;
	}

	/**
	 * 
	 */
	public static final int floor(float value) {
		int isolate = Integer.parseInt(Float.toString(value).substring(
				Float.toString(value).indexOf('.') + 1,
				Float.toString(value).indexOf('.') + 2));
		if (isolate >= 5)
			return (int) (value - 0.5);
		else
			return (int) value;
	}

	/**
	 * Gets the ceiling of a float value, all realistic values work fine but
	 * extreme values can be somewhat inaccurate.
	 */
	public static final int ceils(float value) {
		return (int) (value + BIGCEIL) - BIGINT;
	}

	/**
	 * Gets the ceiling of a float value, IT MUST BE POSITIVE, all realistic
	 * values work fine but extreme values can be somewhat inaccurate.
	 */
	public static final int ceilPositive(float value) {
		return (int) (value + CEILING);
	}

	/**
	 * 
	 */
	public static final int ceil(float value) {
		int isolate = Integer.parseInt(Float.toString(value).substring(
				Float.toString(value).indexOf('.') + 1,
				Float.toString(value).indexOf('.') + 2));
		if (isolate >= 5)
			return (int) value;
		else
			return (int) (value + 0.5);
	}

	/**
	 * Rounds either up or down to the nearest integer for any float value, all
	 * realistic values work fine but extreme values can be somewhat inaccurate.
	 */
	public static final long round(float value) {
		return (long) (value + BIGROUND) - BIGINT;
	}

	public static final long round(double value) {
		return (long)((long) (value + BIGROUND) - BIGROUND);
	}

	/**
	 * Rounds either up or down to the nearest integer for POSITIVE float
	 * values, all realistic values work fine but extreme values can be somewhat
	 * inaccurate.
	 */
	public static final long roundPositive(float value) {
		return (long) (value + 0.5f);
	}

	/**
	 * Round a Float to the nth decimal place (n = places)
	 * 
	 * @param value
	 *            - Value to round
	 * @param places
	 *            - Decimal places to round to
	 * @return Value rounded to places-th decimal place.
	 */
	public static final float round(float value, int places) {
		float scientific = 1 * (pow(10, places));
		long mutated = Math.round(value * scientific);
		return mutated / scientific;
	}

	/**
	 * Round a Double to the nth decimal place (n = places)
	 * 
	 * @param value
	 *            - Value to round
	 * @param places
	 *            - Decimal places to round to
	 * @return Value rounded to places-th decimal place.
	 */
	public static final double round(double value, int places) {
		double scientific = 1 * (pow(10, places));
		long mutated = Math.round(value * scientific);
		return mutated / scientific;
	}

	/**
	 * Raise a float to the nth power (n = power)<br />
	 * <br />
	 * Recently became able to support negative and decimal powers.
	 * 
	 * @param base
	 *            - Value to apply an exponent to
	 * @param power
	 *            - Power of the exponent
	 * @return Value raised to the power-th power
	 */
	public static final float pow(float base, float power) {
		if (power == 0)
			return 1;
		if (power == 1)
			return base;
		if (power == -1)
			return (1 / base);
		
		if(Maths.abs(power) % 1 > 0 && Maths.abs(power) < 1){
			return (float) Math.pow(Math.E, power * Math.log(base));
		}

		// NOW HANDLES NEGATIVES :D
		if (power < 0) {
			return (1 / base)
					* pow(base, (power < -1 ? power + 1 : power - power));
		} else {
			return base * pow(base, (power > 1 ? power - 1 : power - power));
		}
	}

	/**
	 * Raise a float to the nth power (n = power)
	 * 
	 * @param base
	 *            - Value to apply an exponent to
	 * @param power
	 *            - Power of the exponent
	 * @return Value raised to the power-th power
	 */
	public static final double pow(double base, double power) {
		if (power == 0)
			return 1;
		if (power == 1)
			return base;
		if (power == -1)
			return (1 / base);
		
		//Handles Fractional Exponents
		if(Maths.abs(power) % 1 > 0 && Maths.abs(power) < 1){
			return Math.pow(Math.E, power * Math.log(base));
		}
		
		// NOW HANDLES NEGATIVES :D
		if (power < 0) {
			return (1 / base)
					* pow(base, (power < -1 ? power + 1 : power - power));
		} else {
			return base * pow(base, (power > 1 ? power - 1 : power - power));
		}
	}

	/**
	 * Get the Square Root of Base and round to the Round-th decimal place. <br />
	 * <br />
	 * If you try to square root a negative you get an imaginary number, at the
	 * moment this yields a return value of -1... <br />
	 * <br />
	 * I mean... Technically that is correct for....... if you dont think too
	 * much.
	 * 
	 * @param base
	 *            - Value to get the square root of
	 * @param round
	 *            - Round to this decimal place. If this = 0 then dont round, if
	 *            this = -1 then round to the first integer.
	 * @return Square root of base
	 */
	public static float sqrt(float val) {
		return xroot(2, val);
	}
	/**
	 * Get the Square Root of Base
	 * 
	 * @param base
	 *            - Value to get the square root of
	 * @return Square root of base
	 */
	public static double sqrt(double base) {
		return xroot(2, base);
	}
	
	/**
	 * Get the Square Root of the Base to the Round-th decimal places
	 * @param val - Value to Square Root
	 * @param round - Places to Round to
	 * @return
	 */
	public static double sqrt(double val, int round){
		double x = log(val)/2;
		return round(pow(10, x), round);
	}

	/**
	 * finds the cube root of any reasonable number, at extremes extremely
	 * inaccurate values will be returned. [x^(1/3)] <br />
	 * <br />
	 * Actually parses a Float from a Double value that was converted to a
	 * String. <br />
	 * Process: 3rd Root of value as double -> String -> Float parsed from
	 * String <br />
	 * <br />
	 * This is done to maintain precision since the double method ends up being
	 * more accurate.
	 * 
	 * @param value
	 *            - Value to get the Cube Root of
	 * @return cube root of Value
	 */
	public static float cbrt(float value) {
		return Float.parseFloat(Double.toString(round(xroot(3, (double) value), 6)));
	}

	/**
	 * Finds the Cube Root of any Reasonable Number, at extremes extremely
	 * inaccurate values will be returned. [x^(1/3)] <br />
	 * <br />
	 * Calls xroot(3, value)
	 * 
	 * @param value
	 * @return Cube Root of Value
	 */
	public static double cbrt(double value) {
		return xroot(3, value);
	}

	/**
	 * Returns the Xth root of Val <br />
	 * <br />
	 * If root = 0 returns Float.POSITIVE_INFINITY <br />
	 * If root = 1 returns val <br />
	 * <br />
	 * If xth root is obviously inaccurate, derive Float from a Double. (See
	 * calcFloatRoot) <br />
	 * 
	 * 
	 * @param root
	 *            - Root to derive
	 * @param val
	 *            - Value to get the root of
	 * @return Xth Root of Val
	 */
	public static float xroot(float root, float val) {
		if(root == 0) return Float.POSITIVE_INFINITY;
		if(root == 1) return val;
		
		float x = log(val)/root;
		return pow(10, x);
	}

	/**
	 * Returns the Xth Root of Val <br />
	 * <br />
	 * If root = 0 returns Double.POSITIVE_INFINITY <br />
	 * If root = 1 returns val <br />
	 * 
	 * @param root
	 * @param val
	 * @return
	 */
	public static double xroot(double root, double val) {
		if(root == 0) return Double.POSITIVE_INFINITY;
		if(root == 1) return val;
		
		double x = log(val)/root;
		return pow(10, x);
	}

	/**
	 * Get the absolute value of any number.
	 * 
	 * @param val
	 * @return Absolute Value of Val
	 */
	public static float abs(float val) {
		if (val < 0)
			return -val;
		else
			return val;
	}

	/**
	 * Get the absolute value of any number.
	 * 
	 * @param val
	 * @return Absolute Value of Val
	 */
	public static double abs(double val) {
		if (val < 0)
			return -val;
		else
			return val;
	}

	/**
	 * Get the absolute value of any number.
	 * 
	 * @param val
	 * @return Absolute Value of Val
	 */
	public static long abs(long val){
		if(val < 0)
			return -val;
		else
			return val;
	}

	/**
	 * Get the absolute value of any number.
	 * 
	 * @param val
	 * @return Absolute Value of Val
	 */
	public static int abs(int val){
		if(val < 0)
			return -val;
		else
			return val;
	}

	/**
	 * Get the decimal log of any number. <br />
	 * <br />
	 * Works using the Change of Base Formula with the Natural Logarithm. <br />
	 * Such that log10(5) = ln(5)/ln(10)
	 * 
	 * @param val
	 * @return Decimal Logarithm of Val
	 */
	public static double log(double val) {
		if (val == 1)
			return 0;
		if (val == 10)
			return 1;
		return (Math.log(val) / Math.log(10));
	}

	/**
	 * Get the decimal log of any number. <br />
	 * <br />
	 * Works using the Change of Base Formula with the Natural Logarithm. <br />
	 * Such that log10(5) = ln(5)/ln(10)
	 * 
	 * @param val
	 * @return Decimal Logarithm of Val
	 */
	public static float log(float val) {
		if (val == 1)
			return 0;
		if (val == 10)
			return 1;
		return ln(val) / ln(10);
	}

	/**
	 * Gets the natural log of any number.
	 * 
	 * @param val
	 * @return Natural Logarithm of Val
	 */
	public static double ln(double val) {
		if (val == 1)
			return 0;
		// Added since passing my value for Eulers Number caused an
		// output of 0.9999...94 due to accuracy loss associated
		// with float <-> double conversions.
		if (val == E || val == Math.E)
			return 1;
		return Math.log(val);
	}

	/**
	 * Gets the natural log of any number.
	 * 
	 * @param val
	 * @return Natural Logarithm of Val
	 */
	public static float ln(float val) {
		if (val == 1)
			return 0;
		// Added since passing my value for Eulers Number caused an
		// output of 0.9999...94 due to accuracy loss associated
		// with float <-> double conversions.
		if (val == E || val == Math.E)
			return 1;
		return (float) Math.log(val);
	}

	/**
	 * Get the base logarithm of any number. <br />
	 * <br />
	 * Works using the Change of Base Formula with the Natural Logarithm.<br />
	 * Such that log3.7(12) = ln(12)/ln(3.7)
	 * 
	 * @param base
	 * @param val
	 * @return The base logarithm of val
	 */
	public static double logx(double base, double val) {
		if (val == 1)
			return 0;
		if (val == base)
			return 1;
		return ln(val) / ln(base);
	}

	/**
	 * Get the base logarithm of any number. <br />
	 * <br />
	 * Works using the Change of Base Formula with the Natural Logarithm.<br />
	 * Such that log3.7(12) = ln(12)/ln(3.7)
	 * 
	 * @param base
	 * @param val
	 * @return The base logarithm of val
	 */
	public static float logx(float base, float val) {
		if (val == 1)
			return 0;
		if (val == base)
			return 1;
		return ln(val) / ln(base);
	}

	/**
	 * Calculates The Hypotenuse/Resultant Vector of given X and Y Components
	 * 
	 * @param x
	 *            - X Component
	 * @param y
	 *            - Y Component
	 * @return Resultant Vector/Hypotenuse
	 */
	public static float hypot(float x, float y) {
		return sqrt(pow(x, 2) + pow(y, 2));
	}

	/**
	 * Calculates The Hypotenuse/Resultant Vector of given X and Y Components
	 * 
	 * @param x
	 *            - X Component
	 * @param y
	 *            - Y Component
	 * @return Resultant Vector/Hypotenuse
	 */
	public static double hypot(double x, double y) {
		return sqrt(pow(x, 2) + pow(y, 2));
	}

	private static float hypot(Vector2 vector){
		return hypot(vector.x, vector.y);
	}

	/**
	 * Calculates Resultant Vector of given X and Y Components
	 * 
	 * @param x
	 *            - X Component
	 * @param y
	 *            - Y Component
	 * @return Resultant Vector/Hypotenuse
	 */
	public static float calcMagnitude(float x, float y) {
		return hypot(x, y);
	}

	/**
	 * Calculates Resultant Vector of given X and Y Components
	 * 
	 * @param x
	 *            - X Component
	 * @param y
	 *            - Y Component
	 * @return Resultant Vector/Hypotenuse
	 */
	public static double calcMagnitude(double x, double y) {
		return hypot(x, y);
	}

	/*public static float calcMagnitude(Vector2D vector){
		return hypot(vector.x, vector.y);
	}*/

	/**
	 * Calculate Magnitude of a 2D Vector
	 * @param vector
	 * @return Magnitude of vector
	 */
	public static float calcMagnitude(Vector2 vector){
		return hypot(vector.x, vector.y);
	}

	/**
	 * Calculates the Magnitude of a 3D Vector
	 * @param vector
	 * @return Magnitude of vector
	 */
	public static float calcMagnitude(Vector3 vector){
		return sqrt(pow(vector.x, 2) + pow(vector.y, 2) + pow(vector.z, 2));
	}

	/**
	 * Clamp a Value between Min and Max
	 * @param val - Value to clamp
	 * @param min - Minumum
	 * @param max - Maximum
	 * @return Scaled Value
	 */
	public static double clamp(double val, double min, double max){
		if(val > max) return max;
		if(val < min) return min;
		return val;
	}

	/**
	 * Clamp a Value between Min and Max
	 * @param val - Value to clamp
	 * @param min - Minumum
	 * @param max - Maximum
	 * @return Scaled Value
	 */
	public static float clamp(float val, float min, float max){
		if(val > max) return max;
		if(val < min) return min;
		return val;
	}

	/**
	 * Clamp a Value between Min and Max
	 * @param val - Value to clamp
	 * @param min - Minumum
	 * @param max - Maximum
	 * @return Scaled Value
	 */
	public static long clamp(long val, long min, long max){
		if(val > max) return max;
		if(val < min) return min;
		return val;
	}

	/**
	 * Clamp a Value between Min and Max
	 * @param val - Value to clamp
	 * @param min - Minumum
	 * @param max - Maximum
	 * @return Scaled Value
	 */
	public static int clamp(int val, int min, int max){
		if(val > max) return max;
		if(val < min) return min;
		return val;
	}

	/**
	 * Clamp a Value between Min and Max
	 * @param val - Value to clamp
	 * @param min - Minumum
	 * @param max - Maximum
	 * @return Scaled Value
	 */
	public static short clamp(short val, short min, short max){
		if(val > max) return max;
		if(val < min) return min;
		return val;
	}

	/**
	 * Normalize a Value to between 0 and 1. <br />
	 * if val > max return 1 <br />
	 * if val < min return 0 <br />
	 * else return scaled value. <br />
	 * @param val - Value to Normalize
	 * @param min - Minimum Value
	 * @param max - Maximum Value
	 * @return
	 */
	public static float normalize(float val, float min, float max){
		if(val > max) return 1;
		if(val < min) return 0;
		return (val - min)/(max-min);
	}

	/**
	 * Normalize a Value to between 0 and 1. <br />
	 * if val > max return 1 <br />
	 * if val < min return 0 <br />
	 * else return scaled value. <br />
	 * @param val - Value to Normalize
	 * @param min - Minimum Value
	 * @param max - Maximum Value
	 * @return
	 */
	public static float normalize(int val, int min, int max){
		if(val > max) return 1;
		if(val < min) return 0;
		return (val - min)/(max-min);
	}

	/**
	 * Normalize a Value to between 0 and 1. <br />
	 * if val > max return 1 <br />
	 * if val < min return 0 <br />
	 * else return scaled value. <br />
	 * @param val - Value to Normalize
	 * @param min - Minimum Value
	 * @param max - Maximum Value
	 * @return
	 */
	public static double normalize(double val, double min, double max){
		if(val > max) return 1;
		if(val < min) return 0;
		return (val - min)/(max-min);
	}

	/**
	 * Normalize a Value to between 0 and 1. <br />
	 * if val > max return 1 <br />
	 * if val < min return 0 <br />
	 * else return scaled value. <br />
	 * @param val - Value to Normalize
	 * @param min - Minimum Value
	 * @param max - Maximum Value
	 * @return
	 */
	public static float normalize(long val, long min, long max){
		if(val > max) return 1;
		if(val < min) return 0;
		return (val - min)/(max-min);
	}

	/**
	 * Normalize a Value to between 0 and 1. <br />
	 * if val > max return 1 <br />
	 * if val < min return 0 <br />
	 * else return scaled value. <br />
	 * @param val - Value to Normalize
	 * @param min - Minimum Value
	 * @param max - Maximum Value
	 * @return
	 */
	public static float normalize(short val, short min, short max){
		if(val > max) return 1;
		if(val < min) return 0;
		return (val - min)/(max-min);
	}

	public static Vector2 rotateVector(Vector2 origin, Vector2 vector, float radians){

		float x = vector.x - origin.x;
		float y = vector.y - origin.y;
		x = (x * Maths.cos(radians)) + (y * Maths.sin(radians));
		y = (y * Maths.cos(radians)) + (x * Maths.sin(radians));

		x = x + origin.x;
		y = y + origin.y;

		return new Vector2(x, y);

	}

	public static Vector2 rotateVector(Vector2 vector, float radians){
		return rotateVector(Vector2.Zero, vector, radians);
	}

	public static Vector2 rotateVector_Degrees(Vector2 origin, Vector2 vector, float degrees){
		return rotateVector(origin, vector, Maths.toRadians(degrees));
	}

	public static Vector2 rotateVector_Degrees(Vector2 vector, float degrees){
		return rotateVector(vector, Maths.toRadians(degrees));
	}

}
