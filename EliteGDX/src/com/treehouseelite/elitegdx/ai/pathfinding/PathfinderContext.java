package com.treehouseelite.elitegdx.ai.pathfinding;

import java.util.Objects;

import com.badlogic.gdx.math.Vector2;
import com.treehouseelite.elitegdx.ai.pathfinding.heuristics.Heuristic;

/**
 * Description of the current Cntext for the Given Pathfinder <br />
 * Provides data about Pathfinder at time of creation.
 * 
 * @author Matthew Crocco
 */
public class PathfinderContext{

	public final String pathfinderName;
	public final String heuristicName;
	public final Heuristic heuristic;
	public final Pathfinder pathfinder;
	public final Node curNode;
	public final Vector2 curTilePosition;
	public final float tileX, tileY;

	public PathfinderContext(Pathfinder pathfinder, Heuristic heuristic){
		if(pathfinder == null) throw new IllegalArgumentException("A Pathfinder Context MUST be given a Pathfinder Object!");
		this.pathfinder = pathfinder;
		this.heuristic = heuristic;

		pathfinderName = pathfinder.getPathfinderName();
		if(heuristic == null)
			this.heuristicName = "null";
		else
			this.heuristicName = heuristic.getHeuristicName();

		curNode = pathfinder.getCurrentNode();
		curTilePosition = curNode.getPoint();
		tileX = curTilePosition.x;
		tileY = curTilePosition.y;
	}

	public PathfinderContext(Pathfinder pathfinder){
		this(pathfinder, pathfinder.getHeuristic());
	}

	public PathfinderContext(Pathfinder pathfinder, Heuristic heuristic, Node curNode){
		if(pathfinder == null) throw new IllegalArgumentException("A Pathfinder Context MUSt be given a Pathfinder Object!");
		this.pathfinder = pathfinder;
		this.heuristic = heuristic;

		pathfinderName = pathfinder.getPathfinderName();
		if(heuristic == null)
			this.heuristicName = "null";
		else
			this.heuristicName = heuristic.getHeuristicName();

		this.curNode = curNode;
		curTilePosition = curNode.getPoint();
		tileX = curTilePosition.x;
		tileY = curTilePosition.y;
	}

	public PathfinderContext(Pathfinder pathfinder, Node curNode){
		this(pathfinder, pathfinder.getHeuristic(), curNode);
	}

}
