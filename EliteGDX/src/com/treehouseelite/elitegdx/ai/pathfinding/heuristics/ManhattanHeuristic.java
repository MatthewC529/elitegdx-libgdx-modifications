package com.treehouseelite.elitegdx.ai.pathfinding.heuristics;

import com.badlogic.gdx.math.Vector2;
import com.treehouseelite.elitegdx.ai.pathfinding.Node;
import com.treehouseelite.elitegdx.math.Maths;

/**
 * Heuristic based on Manhattan Distance, Will cause the Pathfinder to follow a straight-line path <br />
 * No Diagonals will exist in the path. (Manhattan Roads = Grid Design)
 * 
 * @author Matthew Crocco
 */
public class ManhattanHeuristic implements Heuristic{

	@Override
	public int getValue(float x, float y, float x2, float y2) {
		return (int) (Maths.abs(x - x2) + Maths.abs(y - y2));
	}

	@Override
	public int getValue(Vector2 p1, Vector2 p2) {
		return (int) (Maths.abs(p1.x - p2.x) + Maths.abs(p1.y - p2.y));
	}

	@Override
	public String getHeuristicName() {
		return "Manhattan Distance Heuristic";
	}

	@Override
	public int getValue(Node start, Node end) {
		return getValue(start.getPoint(), end.getPoint());
	}

}
