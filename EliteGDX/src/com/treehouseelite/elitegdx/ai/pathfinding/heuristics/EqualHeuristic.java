package com.treehouseelite.elitegdx.ai.pathfinding.heuristics;

import com.badlogic.gdx.math.Vector2;
import com.treehouseelite.elitegdx.ai.pathfinding.Node;

/**
 * A Heuristic where every distance is equal to 1. <br />
 * <br />
 * This SHOULD turn the AStarPathfinder into a Djikstra Pathfinder
 * 
 * @author Matthew Crocco
 */
public class EqualHeuristic implements Heuristic {

	@Override
	public int getValue(float x, float y, float x2, float y2) {
		return 1;
	}

	@Override
	public int getValue(Vector2 pointOne, Vector2 pointTwo) {
		return 1;
	}

	@Override
	public String getHeuristicName() {
		return "None (Equal Weighting Heuristic)";
	}

	@Override
	public int getValue(Node start, Node end) {
		return 1;
	}

}
