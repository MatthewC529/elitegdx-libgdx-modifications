package com.treehouseelite.elitegdx.ai.pathfinding.heuristics;

import com.badlogic.gdx.math.Vector2;
import com.treehouseelite.elitegdx.ai.pathfinding.Node;
import com.treehouseelite.elitegdx.math.Maths;

/**
 * A Heuristic where Distance Cost is determined by Euclidean Distance <br />
 * Accurate but expensive in numerous calculations due to the use of sqrt(). <br />
 * <br />
 * The SimpleHeuristic is the replacement, does not use sqrt()
 * @author Matthew Crocco
 *
 */
public class EuclideanHeuristic implements Heuristic{

	@Override
	public int getValue(float x, float y, float x2, float y2) {
		float dx = x - x2;
		float dy = y - y2;

		return (int) Maths.sqrt((dx * dx) + (dy * dy));
	}

	@Override
	public int getValue(Vector2 p1, Vector2 p2) {
		float dx = p1.x - p2.x;
		float dy = p1.y - p2.y;

		return (int) Maths.sqrt((dx * dx) + (dy * dy));
	}

	@Override
	public String getHeuristicName() {
		return "Euclidean Distance Heuristic";
	}

	@Override
	public int getValue(Node start, Node end) {
		return getValue(start.getPoint(), end.getPoint());
	}



}
