package com.treehouseelite.elitegdx.ai.pathfinding.heuristics;

import com.badlogic.gdx.math.Vector2;
import com.treehouseelite.elitegdx.ai.pathfinding.Node;

/**
 * A Simplified Euclidean Distance Heuristic. <br />
 * The only difference is that this heuristic does not use the costly Square Root Function.
 * 
 * @author Matthew
 *
 */
public class SimpleHeuristic implements Heuristic{

	@Override
	public int getValue(float x, float y, float x2, float y2) {
		float dx = x - x2;
		float dy = y - y2;

		return (int) ((dx*dx) + (dy*dy));
	}

	@Override
	public int getValue(Vector2 p1, Vector2 p2) {
		float dx = p1.x - p2.x;
		float dy = p1.y - p2.y;

		return (int) ((dx*dx) + (dy*dy));
	}

	@Override
	public String getHeuristicName() {
		return "Simple Euclidean Distance Heuristic";
	}

	@Override
	public int getValue(Node start, Node end) {
		return getValue(start.getPoint(), end.getPoint());
	}

}
