package com.treehouseelite.elitegdx.ai.pathfinding.heuristics;

import com.badlogic.gdx.math.Vector2;
import com.treehouseelite.elitegdx.ai.pathfinding.Node;

/**
 * Describes a Heuristic and how Distance Costs are calculated
 * 
 * @author Matthew Crocco
 */
public interface Heuristic {

	/**The Inexpensive SimpleHeuristic, (Euclidean Distance)^2*/
	public static Heuristic DEFAULT_HEURISTIC = new SimpleHeuristic();

	/**
	 * Determine Heuristic Cost between two points
	 * @param x
	 * @param y
	 * @param x2
	 * @param y2
	 * @return Heuristic Cost
	 */
	int getValue(float x, float y, float x2, float y2);
	
	/**
	 * Determine Heuristic Cost between two points
	 * @param pointOne
	 * @param pointTwo
	 * @return Heuristic Cost
	 */
	int getValue(Vector2 pointOne, Vector2 pointTwo);
	
	/**
	 * Determine Heuristic Cost between two Nodes
	 * @param start
	 * @param end
	 * @return Heuristic Cost
	 */
	int getValue(Node start, Node end);
	
	/**
	 * Get Name of Current Heuristic
	 * @return
	 */
	String getHeuristicName();
}
