package com.treehouseelite.elitegdx.ai.pathfinding;

import java.util.Comparator;
import java.util.PriorityQueue;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.treehouseelite.elitegdx.ai.pathfinding.heuristics.EqualHeuristic;
import com.treehouseelite.elitegdx.ai.pathfinding.heuristics.Heuristic;
import com.treehouseelite.elitegdx.ai.pathfinding.heuristics.SimpleHeuristic;

/**
 * Attempt at an AStarPathfinder relying on Tiled Maps and provided Nodes!
 * 
 * @author Matthew Crocco
 */
public @EqualsAndHashCode class AStarPathfinder implements Pathfinder{

	private Heuristic heuristic;
	private Node[][] nodes;
	private Array<Node> closed;
	private PriorityQueue<Node> fringe;
	private @Getter Node currentNode;
	private @Getter Path currentPath;
	private @Getter int searchDistance = -1; 
	private @Getter boolean allowDiagonal = false;

	public AStarPathfinder(Node[][] nodes, Heuristic heuristic){
		this.closed = new Array<Node>();
		this.fringe = new PriorityQueue<Node>(0);
		this.nodes = nodes;
		this.heuristic = heuristic;
	}

	public AStarPathfinder(Node[][] nodes){
		this(nodes, Heuristic.DEFAULT_HEURISTIC);
	}
	
	/**
	 * Set Maximum Search Depth for this instance
	 */
	public AStarPathfinder setMaxSearchDistance(int distance){
		this.searchDistance = distance;
		return this;
	}
	
	/**
	 * Set Allow Diagonal Movement (This is False for Pacman of course!)
	 */
	public AStarPathfinder setAllowDiagonal(boolean diagonal){
		this.allowDiagonal = diagonal;
		return this;
	}

	@Override
	public Path findPath(float x, float y, float x2, float y2) {
		currentPath.clear();
		fringe.clear();
		closed.clear();
		fringe = new PriorityQueue<Node>(nodes.length * nodes[0].length, new AStarComparator(new Vector2(x2, y2), heuristic));
		currentNode = getNodeAt(new Vector2(x, y));
		Node target = getNodeAt(new Vector2(x2, y2));
		
		Pathfinders.connectParents(nodes, currentNode.getPoint());
		currentPath.append(currentNode);
		
		while(!currentNode.getPoint().epsilonEquals(target.getPoint(), 16) || currentPath.size() == searchDistance){
			for(Node n: currentNode.getChildren()){
				if(n == null) continue;
				if(closed.contains(n, false)) continue;
				if(fringe.contains(n)) continue;
				if(!isValidMovementDirection(n)) continue;
				
				fringe.add(n);
			}
			
			if(fringe.size() == 0) break;
			
			closed.add(currentNode);
			currentNode = fringe.remove();
			currentPath.append(currentNode);
			
		}
		
		return currentPath;
	}

	@Override
	public Path findPath(Vector2 start, Vector2 target) {
		return findPath(start.x, start.y, target.x, target.y);
	}

	@Override
	public Array<Node> findNodesForPath(float x, float y, float x2, float y2) {
		return findPath(x, y, x2, y2).getNodeArrayGDX();
	}

	@Override
	public Array<Node> findNodesForPath(Vector2 start, Vector2 target) {
		return findNodesForPath(start.x, start.y, target.x, target.y);
	}

	@Override
	public String getPathfinderName() {
		return (heuristic instanceof EqualHeuristic) ? "Djikstra Pathfinder" : "A* Pathfinder";
	}

	@Override
	public Heuristic getHeuristic(){
		return heuristic;
	}

	@Override
	public AStarPathfinder setHeuristic(Heuristic newHeuristic){
		heuristic = newHeuristic;
		return this;
	}
	
	public boolean isValidMovementDirection(Node n){
		
		if(allowDiagonal) return true;
		if(n.equals(currentNode)) return false;
		
		if(n.x != currentNode.x && n.y != currentNode.y) return false;
		
		return true;
		
	}
	
	public Node getNodeAt(Vector2 pos){
		
		for(Node[] arr: nodes)
			for(Node n: arr)
				if(n.getPoint().epsilonEquals(pos, 16)) return n;
				
		
		return null;
		
	}

}

/**
 * AStarComparator used to Compare Nodes for the PriorityQueue
 * 
 * @author matthew
 */
class AStarComparator implements Comparator<Node>{
	
	public final Vector2 target;
	public final Heuristic heuristic;
	
	public AStarComparator(Vector2 target, Heuristic heuristic){
		this.target = target;
		this.heuristic = heuristic;
	}
	
	@Override
	public int compare(Node o1, Node o2) {
		float d1 = heuristic.getValue(new Vector2(o1.x, o1.y), target);
		float d2 = heuristic.getValue(new Vector2(o2.x, o2.y), target);
		
		if(d1 < d2) return -1;
		if(d1 > d2) return 1;
		
		return 0;
	}
}
