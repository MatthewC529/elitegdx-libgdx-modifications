package com.treehouseelite.elitegdx.ai.pathfinding;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.treehouseelite.elitegdx.util.collections.ArrayUtils;

/**
 * Represents an arbitrary position in Tiled Map space for AI Pathfinding.<br />
 * <br />
 * Consists of Vector2 Coordinates, a Reference to the Node parent and all of its children.
 * 
 * @author Matthew Crocco
 */
public @EqualsAndHashCode class Node {
	
	//Possible Locations of Current Nodes
	public static final int NORTHWEST = 0, NORTH = 1, NORTHEAST = 2,
							EAST = 3, SOUTHEAST = 4, SOUTH = 5,
							SOUTHWEST = 6, WEST = 7;

	public final @Getter float x, y;
	private Node parent;
	private Node[] children;
	private boolean root = false;

	public Node(float x, float y, Node parent){
		this.x = x;
		this.y = y;
		this.parent = parent;
		this.children = new Node[8];
		
		for(int i = 0; i < children.length; i++){
			children[i] = null;
		}
	}

	public Node(float x, float y){
		this(x, y, null);
	}

	public Node getParent(){
		return parent;
	}

	public boolean hasParent(){
		return parent != null;
	}

	public void setParent(Node n){
		parent = n;
	}
	
	public Node getChild(int direction){
		return direction >= children.length || direction < 0 ? null : children[direction];
	}
	
	public Node[] getChildren(){
		Node[] arr = ArrayUtils.copyArray(children);
		arr = ArrayUtils.condenseArray(arr);
		return arr;
	}

	public void setChildren(Node[][] nodes, int thisX, int thisY){

		Array<Node> arr = new Array<Node>();
		ArrayMap<Node, Vector2> map = new ArrayMap<Node, Vector2>();

		//RIGHT
		int x = thisX + 1;
		int y = thisY; 
		if(isValidNodeForParenting(nodes, x, y)){
			nodes[x][y].setParent(this);
			children[EAST] = nodes[x][y];
			arr.add(nodes[x][y]);
			map.put(nodes[x][y], new Vector2(x, y));
		}

		//TOP_RIGHT
		y++;
		if(isValidNodeForParenting(nodes, x, y)){
			nodes[x][y].setParent(this);
			children[NORTHEAST] = nodes[x][y];
			arr.add(nodes[x][y]);
			map.put(nodes[x][y], new Vector2(x, y));
		}

		//TOP
		x--;
		if(isValidNodeForParenting(nodes, x, y)){
			nodes[x][y].setParent(this);
			children[NORTH] = nodes[x][y];
			arr.add(nodes[x][y]);
			map.put(nodes[x][y], new Vector2(x, y));
		}

		//TOPLEFT
		x--;
		if(isValidNodeForParenting(nodes, x, y)){
			nodes[x][y].setParent(this);
			children[NORTHWEST] = nodes[x][y];
			arr.add(nodes[x][y]);
			map.put(nodes[x][y], new Vector2(x, y));
		}

		//LEFT
		y--;
		if(isValidNodeForParenting(nodes, x, y)){
			nodes[x][y].setParent(this);
			children[WEST] = nodes[x][y];
			arr.add(nodes[x][y]);
			map.put(nodes[x][y], new Vector2(x, y));
		}

		//BOTTOM_LEFT
		y--;
		if(isValidNodeForParenting(nodes, x, y)){
			nodes[x][y].setParent(this);
			children[SOUTHWEST] = nodes[x][y];
			arr.add(nodes[x][y]);
			map.put(nodes[x][y], new Vector2(x, y));
		}

		//BOTTOM
		x++;
		if(isValidNodeForParenting(nodes, x, y)){
			nodes[x][y].setParent(this);
			children[SOUTH] = nodes[x][y];
			arr.add(nodes[x][y]);
			map.put(nodes[x][y], new Vector2(x, y));
		}

		//BOTTOM_RIGHT
		x++;
		if(isValidNodeForParenting(nodes, x, y)){
			nodes[x][y].setParent(this);
			children[SOUTHEAST] = nodes[x][y];
			arr.add(nodes[x][y]);
			map.put(nodes[x][y], new Vector2(x, y));
		}

		for(Node n: arr){
			n.setChildren(nodes, Float.floatToIntBits(map.get(n).x), Float.floatToIntBits(map.get(n).y));
		}
	}

	public void setRoot(boolean isRoot){
		this.root = isRoot;
	}
	public boolean isRoot(){
		return root;
	}

	public Vector2 getPoint(){
		return new Vector2(x, y);
	}

	public float[] getFloatComponents(){
		return new float[]{x, y};
	}

	private boolean isValidNode(Node[][] check, int x, int y){
		if(x >= check.length || x < 0) return false;
		if(y >= check[0].length || y < 0) return false;
		if(check[x][y] == null) return false;
		return true;
	}

	private boolean isValidNodeForParenting(Node[][] check, int x, int y){
		if(!isValidNode(check, x, y)) return false;
		if(check[x][y].isRoot()) return false;
		if(check[x][y].hasParent()) return false;
		return true;
	}
}
