package com.treehouseelite.elitegdx.ai.pathfinding;

import java.util.ArrayDeque; 
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;

import com.badlogic.gdx.utils.Array;
import com.treehouseelite.elitegdx.ex.UnimplementedException;
/**
 * A List of Nodes leading to a final destination. <br />
 * <br />
 * <b><i>NOT</i></b> a Collection!
 * 
 * @author Matthew Crocco
 */
public class Path implements java.io.Serializable, java.lang.Iterable<Node>{

	private static final long serialVersionUID = -5614671235L;

	private final ArrayDeque<Node> steps;

	public Path(){
		steps = new ArrayDeque<Node>();
	}

	public Node next(){
		return steps.pollFirst();
	}

	public Node popLast(){
		return steps.pop();
	}

	public Node checkLast(){
		return steps.peekLast();
	}

	public Array<Node> getNodeArrayGDX(){
		return new Array<Node>(getNodeArray());
	}

	public Node[] getNodeArray(){
		return steps.<Node>toArray(new Node[steps.size()]);
	}

	public int size() {
		return steps.size();
	}

	public boolean isEmpty() {
		return (steps.size() == 0);
	}

	public boolean contains(Object o) {
		if(o instanceof Node) return false;
		return steps.contains(o);
	}

	@Override
	public Iterator<Node> iterator() {
		return new Iterator<Node>() {

			Deque<Node> pos = steps.clone();

			@Override
			public boolean hasNext() {
				return !pos.isEmpty();
			}

			@Override
			public Node next() {
				return pos.removeFirst();
			}

			@Override
			public void remove() {
			}
		};
	}

	public Node[] toArray() {
		return steps.<Node>toArray(new Node[steps.size()]);
	}

	public void append(Node e) {
		steps.add(e);
	}

	public void prepend(Node e){
		steps.push(e);
	}

	public void remove(Node o) {
		steps.remove(o);
	}

	public boolean addAll(Collection<? extends Node> c) {
		steps.addAll(c);
		return true;
	}

	public boolean removeAll(Collection<?> c) {
		steps.removeAll(c);
		return true;
	}

	public void clear() {
		steps.clear();
	}

}
