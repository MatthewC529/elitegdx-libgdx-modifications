package com.treehouseelite.elitegdx.ai.pathfinding;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.treehouseelite.elitegdx.ai.pathfinding.heuristics.Heuristic;
import com.treehouseelite.elitegdx.ex.UnimplementedException;

/**
 * Describes a Pathfinder
 * 
 * @author Matthew Crocco
 * @see com.treehouseelite.elitegdx.ai.pathfinding.AStarPathfinder AStarPathfinder.class
 */
public interface Pathfinder {

	Path findPath(float x, float y, float x2, float y2);

	Path findPath(Vector2 start, Vector2 target);

	Array<Node> findNodesForPath(float x, float y, float x2, float y2);

	Array<Node> findNodesForPath(Vector2 start, Vector2 target);

	Pathfinder setAllowDiagonal(boolean diagonal);
	
	Pathfinder setMaxSearchDistance(int distance);
	
	Heuristic getHeuristic();

	Pathfinder setHeuristic(Heuristic newHeuristic);

	Node getCurrentNode();

	String getPathfinderName();

}
