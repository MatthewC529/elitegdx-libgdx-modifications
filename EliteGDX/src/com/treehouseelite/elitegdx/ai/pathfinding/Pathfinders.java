package com.treehouseelite.elitegdx.ai.pathfinding;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.treehouseelite.elitegdx.ai.pathfinding.heuristics.Heuristic;
import com.treehouseelite.elitegdx.ex.UnimplementedException;

/**
 * A Utility Class for Pathfinders
 * 
 * @author matthew
 *
 */
public final class Pathfinders {

	private Pathfinders(){}
	
	/**
	 * Create a Node Map from a Tmx Map Layer
	 * @param layer
	 * @return
	 */
	public static Node[][] createNodesFromMapLayer(TiledMapTileLayer layer){
		int tileW = (int)layer.getTileWidth();
		int tileH = (int)layer.getTileHeight();

		Node[][] nodes = new Node[layer.getWidth()/tileW][layer.getHeight()/tileH];

		for(int y = 0; y < nodes[0].length; y++){
			for(int x = 0; x < nodes.length; x++){
				nodes[x][y] = new Node(x * tileW + (tileW/2), y * tileH + (tileH/2));	//Set Node at Center of Each Tile!
			}
		}

		return nodes;
	}

	/**
	 * Connect Nodes together from a Given Root Node
	 * @param nodes
	 * @param root
	 */
	public static void connectParents(Node[][] nodes, Vector2 root){

		int rX = Float.floatToIntBits(root.x);
		int rY = Float.floatToIntBits(root.y);

		nodes[rX][rY].setParent(null);
		nodes[rX][rY].setChildren(nodes, rX, rY);

	}
}
