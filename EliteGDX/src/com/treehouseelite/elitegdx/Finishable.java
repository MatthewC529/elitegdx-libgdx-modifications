package com.treehouseelite.elitegdx;

/**
 * Interface that allows a class to be PROPERLY dealt with in manager <br />
 * when switching between EliteGDX States (Uninitialized, Initialized, Managing, Sleeping, Dying)
 * 
 * @author Matthew
 */
public interface Finishable {

	/**
	 * Run when Manager is told to go into the sleep state.
	 */
	void sleep();

	/**
	 * Run when Manager is awoken out of the Sleep State
	 */
	void wakeUp();

	/**
	 * Run when Process is terminating (Closing Streams, De-allocating, De-referencing and so on)
	 */
	void finishUp();

	/**
	 * Is the Class in the sleep state?
	 * @return
	 */
	boolean isSleeping();

}
