package com.treehouseelite.elitegdx;

import java.text.DecimalFormat;  
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

import lombok.Getter;
import lombok.Setter;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.LongArray;
import com.badlogic.gdx.utils.LongMap;
import com.treehouseelite.elitegdx.math.Maths;
import com.treehouseelite.elitegdx.util.event.Message;
import com.treehouseelite.elitegdx.util.event.Message.MessageType;
import com.treehouseelite.elitegdx.util.event.MessageReceiver;

//TODO Update Encryption/Decryption
/**
 * The Organizer Manages ID's, ID's must be manually released on disposal of an Entity or Object! <br />
 * Alternatively, you can use the Message System to send a Message to Organizer of the type Entity_Death and with the ID as the content <br />
 * <br />
 * The Organizer manages the pool of available ID's and holds the Encryption Methods!
 * 
 * @author Matthew Crocco
 */
public final class Organizer implements MessageReceiver{

	private static @Getter @Setter int NumberOfUniqueIDs;
	private static @Getter @Setter Random randomGenerator;
	private volatile static LongArray available;
	private volatile static LongMap<Object> unavailable;
	private static @Getter int poolSize;
	private static PopulatorThread pt;

	protected Organizer(int uuidPoolSize){
		NumberOfUniqueIDs = uuidPoolSize;
		available = new LongArray(true, uuidPoolSize);
		unavailable = new LongMap<Object>();
		randomGenerator = new Random(System.currentTimeMillis() * System.nanoTime());
		poolSize = uuidPoolSize;

		//In Android the amount of ID's is severely limited, there are NO Threads in that case!
		if(poolSize > 1000000){
			pt = new PopulatorThread();
			pt.run();
		}else{
			Manager.getLogger();
			Logger.finest("Populating "+ poolSize +" ID's non-concurrently...");
			for(long i = 1; i < poolSize+1; i++){
				available.add(i);
			}
		}
	}

	/**
	 * Register a Unique, Available ID with the given Object from the Available ID pool
	 * @param o
	 * @return
	 */
	public long getUniqueID(Object o){
		long proposal = Maths.abs(randomGenerator.nextInt(poolSize));

		if(!available.contains(proposal) || unavailable.containsKey(proposal))
			return getUniqueID(o);
		else{
			available.removeValue(proposal);
			unavailable.put(proposal, o);
			return proposal;
		}
	}

	/**
	 * Get the Object associated with the Given ID, if Available.
	 * @param id
	 * @return
	 */
	public Object getHolderOfId(long id){
		if(unavailable.containsKey(id)){
			return unavailable.get(id);
		}else return null;
	}

	/**
	 * Get Object Associated with ID of the given Restriction Class Type.
	 * @param id
	 * @param restrict
	 * @return
	 */
	public Object getHolderOfId(long id, Class<?> restrict){
		if(getHolderOfId(id).getClass() != restrict)
			return null;
		else
			return getHolderOfId(id);
	}

	/**
	 * Get Object of the Given Type associated with the ID
	 * @param id
	 * @return
	 */
	public <T> T getRealHolderOfId(long id){
		if(unavailable.containsKey(id)){
			return (T)unavailable.get(id);
		}else return null;
	}

	/**
	 * Release the Given ID
	 * @param id
	 */
	public void releaseId(long id){
		if(!available.contains(id)){
			unavailable.remove(id);
			available.add(id);
		}
	}

	public Array<Object> getRegisteredEntities(){
		return unavailable.values().toArray();
	}

	//-- Message Handling and Populator Interaction ---------------

	@Override
	public void receiveMessage(Message<?> m){
		if(m.getMessageType() == MessageType.ENTITY_DEATH && m.getContent().getClass() == long.class){
			long id = (Long)m.getContent();

			unavailable.remove(id);
			available.add(id);
		}
	}

	public double getPercentPopulated(){
		if(pt != null)
			return pt.getPercentPopulated();
		else
			throw new IllegalStateException("Currently Impossible to Grab Populated Amount! Populator Never Ran...");
	}

	public String getPercentPopulatedAsString(){
		if(pt != null)
			return pt.getPercentPopulatedAsString();
		else
			throw new IllegalStateException("Currently Impossible to Grab Populated Amount! Populator Never Ran...");
	}

	//-- ENCRYPTION ----------------------------------------

	public enum Encryption{
		HEXADECIMAL,
		COMPOUND,
		BINARY;
	}

	/**
	 * A Convenience Method, Specify a Supported Encryption and the String will be encrypted as such.
	 * @param str -- String to encrypt
	 * @param enc -- Encryption Type
	 * @return Encrypted String
	 */
	public static String encode(String str, Encryption enc){
		if(enc == Encryption.HEXADECIMAL){
			return hexEncode(str);
		}else if(enc == Encryption.BINARY){
			return binaryEncode(str);
		}else if(enc == Encryption.COMPOUND){
			return compoundEncode(str);
		}

		throw new UnsupportedOperationException("The encryption type, " + enc.toString() + " is not supported!");
	}

	/**
	 * A Convenience Method, Specify a Supported Encryption and the Byte[] will be encrypted as such.
	 * @param in -- Byte[] to Encrypt
	 * @param enc -- Encryption Type
	 * @return Encrypted String from Byte[]
	 */
	public static String encode(byte[] in, Encryption enc){
		if(enc == Encryption.HEXADECIMAL){
			return hexEncode(in);
		}else if(enc == Encryption.BINARY){
			return binaryEncode(in);
		}else if(enc == Encryption.COMPOUND){
			return compoundEncode(in);
		}

		throw new UnsupportedOperationException("The encryption type, " + enc.toString() + " is not supported!");
	}

	/**
	 * Convert a String to Hexadecimal
	 * @param str -- String to Encode
	 * @return Hexadecimal String
	 */
	public static String hexEncode(String str){
		return hexEncode(str.getBytes());
	}

	/**
	 * Convert a byte[] to Hexadecimal String
	 * @param in -- Byte Array
	 * @return Hexadecimal String from Byte[]
	 */
	public static String hexEncode(byte[] in){
		StringBuilder result = new StringBuilder();
		char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
		for (byte b : in) {
			result.append(hexDigits[(b&0xf0) >> 4]);
			result.append(hexDigits[b&0x0f]);
		}

		return new String(result);
	}

	/**
	 * Convert a String to Binary
	 * @param str -- String to Convert to Binary
	 * @return Binary String
	 */
	public static String binaryEncode(String str){
		return binaryEncode(str.getBytes());
	}

	/**
	 * Convert a Byte[] to a Binary String
	 * @param in -- Byte[] to Convert to Binary
	 * @return Binary String from Byte[]
	 */
	public static String binaryEncode(byte[] in){
		StringBuilder result = new StringBuilder();
		for(byte b: in){
			int val = b;
			for(int i = 0;i < 8; i++){
				result.append((val & 128) == 0 ? 0 : 1);
				val <<= 1;
			}

			result.append(' ');
		}

		return new String(result);
	}

	public static String compoundEncode(String str){
		byte[] encoded = Base64.encodeBase64(str.getBytes());
		return hexEncode(new String(encoded));
	}

	public static String compoundEncode(byte[] str){
		return compoundEncode(new String(str));
	}

	public static String compoundDecode(String str){
		String decoded = hexDecode(str);
		byte[] result = Base64.decodeBase64(decoded);
		return new String(result);
	}

	private static String decode(String str, Encryption enc){
		switch(enc){
		case HEXADECIMAL:
			return hexDecode(str);
		case COMPOUND:
			return compoundDecode(str);
		default:
			return null;
		}
	}

	public static String hexDecode(String str){
		byte[] data;
		try {
			data = Hex.decodeHex(str.toCharArray());
			return new String(data);
		} catch (DecoderException e) {
			e.printStackTrace();
		}
		throw new GdxRuntimeException("Failure to Hex Encode!");
	}

	/**
	 * A Simple Runnable that Populates the Available ID List to try and limit the impact of the population process on performance. <br />
	 * as well as to limit wait times. <br />
	 * <br />
	 * Needs Fine Tuning
	 * 
	 * @author Matthew Crocco
	 */
	private class PopulatorThread implements Runnable{

		public final int size = poolSize;
		public final int steps = 50000;
		private DecimalFormat df = new DecimalFormat("##.00");
		private int populated = 0;
		private int iteration = 0;
		private AtomicBoolean running = new AtomicBoolean(false);

		@Override
		public void run(){
			if(running.get()) return;
			else running.set(true);

			if(Manager.getLogger()!= null && (iteration == 30 || populated + steps >= size)){
				Manager.getLogger();
				Logger.finest("Stepping ID Population ("+ df.format((((double)populated + (double)steps) / size) * 100) +"%)...");
				iteration = 0;
			}

			int thisTime = 0;
			while(populated < size && thisTime < steps){
				populated++;
				thisTime++;
				available.add((long)populated);
			}

			if(populated != size)
				Gdx.app.postRunnable(this);

			iteration++;
			running.set(false);
		}

		public double getPercentPopulated(){
			synchronized(this){
				return ((((double)populated + (double)steps) / size) * 100);
			}
		}

		public String getPercentPopulatedAsString(){
			return df.format(getPercentPopulated());
		}
	}

	@Override
	public boolean isTakingMessages() {
		return true;
	}
}
