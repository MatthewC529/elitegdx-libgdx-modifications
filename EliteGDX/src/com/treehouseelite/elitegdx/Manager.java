package com.treehouseelite.elitegdx;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.ConcurrentModificationException;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import lombok.Getter;
import lombok.Setter;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.treehouseelite.elitegdx.entity.cutscene.CutsceneManager;
import com.treehouseelite.elitegdx.entity.phys.PhysicsManager;
import com.treehouseelite.elitegdx.entity.phys.PhysicsUpdateTask;
import com.treehouseelite.elitegdx.math.Maths;
import com.treehouseelite.elitegdx.util.Analyzable;
import com.treehouseelite.elitegdx.util.Referenceable;
import com.treehouseelite.elitegdx.util.Utility;
import com.treehouseelite.elitegdx.util.Utility.Hook;
import com.treehouseelite.elitegdx.util.event.DisposalListener;
import com.treehouseelite.elitegdx.util.event.RepeatedTask;

/**
 * A simple manager class for the various functions of the EliteGDX Library
 * 
 * @author <b><i>Matthew Crocco (Treehouse Elite)</i></b>
 * @version <b><u>Elite Library v2.3.1a Alpha </u></b>
 * @since <b><u>May 25th, 2014</u></b>
 */
public final class Manager implements Analyzable {

	protected Manager(){}

	/**Library Name*/
	public static final String LIB_NAME = "EliteGDX -- LibGDX Extension";
	/**Library Version -- x.y.zz -- major.minor.build */
	public static final String LIB_VERSION = "v2.3.1 Alpha";
	/**State Management -- Ensures that Finalization is Not Run Twice! <br />
	 * This can occur if Escape Exiting occurs which calls Gdx.app.exit() which leads to a Runtime.exit() event <br />
	 * This library adds a shutdown hook that ensures finalization is run at exit. If run twice it can produce a GLContext Error, this prevents that.*/
	private static AtomicBoolean FINALIZATION_RAN = new AtomicBoolean(false);

	/**List of Finishable Objects */
	private static Array<Finishable> finalizers = new Array<Finishable>(false, 16, Finishable.class);
	/**Map of Class Types and their Respective Finalization Methods */
	private static ArrayMap<Class<?>, String> finalizerMethods = new ArrayMap<Class<?>, String>();
	/**Map of Instances and their Class Types, used to invoke Finalizer Methods */
	private static ArrayMap<Object, Class<?>> finalizerClasses = new ArrayMap<Object, Class<?>>();
	private static Array<Class<?>> singletonFinalizer = new Array<Class<?>>();
	/**List of Instances*/
	private static Array<Object> customFinalizers = new Array<Object>();
	protected static String AUTHOR;					//Author of Project
	protected static String AFFILIATION;			//Project/Author Affiliation
	protected static String COPYRIGHT;				//Ex: Object Oriented Games (c) 2014
	protected static String NAME;					//Project Name
	protected static String VERSION;				//Project Version
	protected static String LDIR;					//Project Log Directory

	protected static @Getter ResourceManager resourceManager;	//ResourceManager Instance
	protected static @Getter InputManager inputManager;			//InputManager Instance
	protected static @Getter Organizer organizer;				//Organizer Instance
	protected static @Getter @Setter Game gameInstance;			//Provided Game Instance
	protected static @Getter Referenceable gameReference;		//Provided Game Reference

	private static Logger logger;							//Logger Instance
	private static @Getter boolean sleeping = false;		//State Management -- Sleep
	private static @Getter @Setter Camera cam;				//Provided Camera for Managing Skeletal Entities

	public static void init(Referenceable ref, Game game){
		Manager.gameInstance = game;
		Manager.gameReference = ref;
		AUTHOR = ref.getAuthor();
		AFFILIATION = ref.getAffiliation();
		COPYRIGHT = ref.getCopyright() == null || ref.getCopyright().isEmpty() ? AFFILIATION + " (c) " + Calendar.getInstance().get(Calendar.YEAR) : ref.getCopyright();
		init(ref.getProjectName(), ref.getVersion(), ref.getLogDirectory().getAbsolutePath());
	}

	protected static void init(String ProjectName, String Version, String logDir) {
		NAME = ProjectName;
		VERSION = Version;
		if(LDIR == null) LDIR = logDir;	
		resourceManager = new ResourceManager();
		inputManager = new InputManager();
		logger = new Logger(ProjectName, Version, logDir);
		if(Gdx.app.getType() == ApplicationType.Android)
			organizer = new Organizer(10000);
		else
			organizer = new Organizer(10000000);
		CutsceneManager.init();
		Gdx.input.setInputProcessor(inputManager);
		addSingletonFinalizer(Logger.class, "finishUp");
		addSingletonFinalizer(PhysicsManager.class, "dispose");

		Logger.dev("Shutdown Hooks Added to Runtime and GDX Lifecycle! Finalization *mostly* Guaranteed!");
		Utility.putHook(new ManagerDisposalThread(), Hook.SHUTDOWN);
		Utility.<DisposalListener>putFunctionalHook(new ManagerDisposalListener());
		Utility.putHook(new StatisticsTask(10000, new Manager()), Hook.GDX_RENDER);
		Utility.putHook(new StatisticsTask(30000, Utility.<Analyzable>getHiddenObject(PhysicsManager.class)), Hook.GDX_RENDER);
		Utility.putHook(new PhysicsUpdateTask(), Hook.GDX_RENDER);
	}
	/**
	 * WARNING: Invoking this Method WILL Exit the Program! <br />
	 * <br />
	 * This method is invoked on Application Termination! <br />
	 * If you are looking to dispose of stored entities, PLEASE use runCleanUp().
	 */
	public static void finishUp() {
		if(FINALIZATION_RAN.get()) return;

		finishUpAllFinalizers();
		finishUpAllClasses();

		if(gameInstance != null){
			if(gameInstance.getScreen() != null){
				gameInstance.getScreen().dispose();
			}
		}

		finishUpAllSingletons();

		FINALIZATION_RAN.set(true);

		Gdx.app.exit();
	}

	//Will Only Clean Up Objects! Singletons are left alone.
	public static void runCleanUp(boolean disposeScreen){
		finishUpAllFinalizers();
		finishUpAllClasses();

		if(gameInstance != null)
			if(gameInstance.getScreen() != null && disposeScreen)
				gameInstance.getScreen().dispose();
	}

	public static void runCleanUp(){
		runCleanUp(false);
	}

	public static final byte FINALIZERS = 0x01, CLASSES = 0x02, SINGLETONS = 0x04;

	/**
	 * Finalize a Specific Group of Objects. <br />
	 * To indicate multiple types use the Bitwise OR operator '|' <br />
	 * <br />
	 * EXAMPLE: FINALIZERS | CLASSES will finalize all registered Custom Finalizers and Finalizers <br />
	 * EXAMPLE: FINALIZERS | SINGLETONS will finalize all registered Finalizers and SINGLETONS <br />
	 * EXAMPLE: FINALIZERS | SINGLETONS | CLASSES will finalize all groups.
	 * @param clean
	 */
	public static void runSpecificCleanUp(byte clean){
		switch(clean){
		case FINALIZERS:
			finishUpAllFinalizers();
			break;
		case CLASSES:
			finishUpAllClasses();
			break;
		case SINGLETONS:
			finishUpAllSingletons();
			break;
		case FINALIZERS | CLASSES:
			finishUpAllFinalizers();
			finishUpAllClasses();
			break;
		case FINALIZERS | SINGLETONS:
			finishUpAllFinalizers();
			finishUpAllSingletons();
			break;
		case CLASSES | SINGLETONS:
			finishUpAllClasses();
			finishUpAllSingletons();
			break;
		case CLASSES | FINALIZERS | SINGLETONS:
			finishUpAllClasses();
			finishUpAllSingletons();
			finishUpAllFinalizers();
			break;
		default:
			break;
		}
	}

	public static void exit(){
		try{
			Gdx.app.exit();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Finishes an Object if it has a finishUp method OR has a custom Finalizer that IS registered!
	 */
	public static void finishUpClass(Object instance){
		Class c = finalizerClasses.get(instance);

		if(!customFinalizers.contains(instance, false)){
			Logger.exception(new IllegalStateException("Instance " + instance + " of " + c.getName() + " is NOT registered with the Manager for Finalization!"));
		}else{
			try{
				finalizerClasses.get(instance).getMethod(finalizerMethods.get(c), (Class<?>) null).invoke(instance, (Object) null);
				removeCustomFinalizer(instance);
			}catch(Exception e){
				Logger.exception(e);
			}
		}
	}

	public static void finishUpAllClasses(){
		try{			
			for(Object o: customFinalizers){
				if(!o.getClass().getName().equalsIgnoreCase(Logger.class.getName())){
					try {
						o.getClass().getMethod(finalizerMethods.get(finalizerClasses.get(o)), (Class<?>) null).invoke(o, (Object)null);
					} catch (Exception e) {}
				}
			}
			
			customFinalizers.clear();
		}catch(ConcurrentModificationException e){}
	}

	public static void finishUpAllSingletons(){
		try{
			for(Class<?> c : singletonFinalizer){
				try {
					Method m = c.getDeclaredMethod(finalizerMethods.get(c), null);
					m.setAccessible(true);
					m.invoke(null, null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			singletonFinalizer.clear();
		}catch(ConcurrentModificationException e){}
	}

	public static void finishUpAllFinalizers(){

		while(finalizers.size != 0)
			finishUpFinalizer(finalizers.get(0));

	}

	public static void finishUpFinalizer(Finishable f){
		try{
			f.finishUp();
			if(finalizers.contains(f, false)){
				finalizers.removeValue(f, false);
			}
		}catch(Exception e){
			Logger.exception(e);
		}
	}

	/**
	 * Add an object implementing finishable to the Finalization List for when
	 * finishUp() is called.
	 * 
	 * @param f
	 *            - The object implementing finishable
	 * @return True if added successfully, false if the addition fails for some
	 *         reason and it will write the trace to the log file.
	 */
	public static boolean addFinalizer(Finishable f) {
		try {
			finalizers.add(f);
			return true;
		} catch (Exception e) {
			Logger.exception(e);
		}
		return false;
	}

	/**
	 * Removes the finishable object from the Finalization List
	 * 
	 * @param f
	 *            - Finishable Object to Remove
	 * @return True if the removal is successful, false if otherwise. Will print
	 *         stackTrace to the log file.
	 */
	public static boolean removeFinalizer(Finishable f) {
		System.out.println(f.getClass().getName());
		try {
			finalizers.removeValue(f, false);
			return true;
		} catch (Exception e) {
			Logger.exception(e);
		}

		if(finalizers.contains(f, false)) System.out.println("WE DID IT!");
		return false;
	}

	/**
	 * Add a class to the finalization sequence. <br />
	 * <br />
	 * <b><u><i>Requirements of Custom Classes that Can be Finalized by Manager:</i></u></b> <br />
	 * <ul>
	 * <li>Must have a sleep(); method that <b><i>TOGGLES</b></i> the sleep state, <b>we do not support a wakeUp-like method!</b></li><br />
	 * <li>Must be a class, not an interface or annotation, that either provides an Instance or is a <b>Singleton (In which case the instnace parameter = null)</b></li><br />
	 * <li>Must provide a <b>CORRECT</b> method name! If your method is <b><i>sleep();</b></i> then the method name is <b><i>"sleep"</i></b></li>
	 * </ul>
	 * <b>This is due to the finalization of custom classes being handled via the Java Reflection API</b><br />
	 * <br />
	 * 
	 * @param c
	 *            - Class to finalize
	 * @param methodName
	 *            - Custom method to finalize and toggleSleep with.
	 * @param instance 
	 * 			  - Instnace upon which the finalizing method and sleep() method are invoked. 
	 * @return True = Success or False = Failure
	 */
	public static boolean addCustomFinalizer(Class<?> c, Object instance, String methodName) {
		try{
			customFinalizers.add(instance);
			finalizerClasses.put(instance, c);
			finalizerMethods.put(c, methodName);
		}catch(Exception e){
			Logger.exception(e);
		}

		return false;
	}

	/**
	 * Remove a class from the Finalization Sequence, simply put the class.
	 * 
	 * @param instance - Remove Instance form Auto-Management Processes
	 * @return True = Success or False = Failure
	 */
	public static boolean removeCustomFinalizer(Object instance) {
		try {
			finalizerMethods.removeKey(finalizerClasses.get(instance));
			customFinalizers.removeValue(instance, false);
			finalizerClasses.removeKey(instance);
			return true;
		} catch (Exception e) {
			Logger.exception(e);
		}

		return false;
	}

	public static void addSingletonFinalizer(Class<?> clazz, String methodName){
		singletonFinalizer.add(clazz);
		finalizerMethods.put(clazz, methodName);
	}

	public static void removeSingletonFinalizer(Class<?> clazz){
		finalizerMethods.removeKey(clazz);
		singletonFinalizer.removeValue(clazz, true);
	}

	@Override
	public String getPrintableStatistics() {
		String FPS = Integer.toString(Gdx.graphics.getFramesPerSecond());
		String FSIZE = "" + finalizers.size;
		String CFSIZE = "" + customFinalizers.size;
		String SSIZE = "" + singletonFinalizer.size;
		String MEM_USE = Utility.getMemoryUsage();

		return String.format("FPS: %s | RegE: %s_%s_%s | MEM_USED: %s", FPS, FSIZE, CFSIZE, SSIZE, MEM_USE);
	}

	public static Logger getLogger(){
		return logger;
	}

	public static String getLibraryVersion(){
		return LIB_VERSION;
	}

	public static String getLibraryName(){
		return LIB_NAME;
	}

	public static String getAuthor(){
		return AUTHOR;
	}

	public static String getProjectName(){
		return NAME;
	}

	public static String getAuthorAffiliation(){
		return AFFILIATION;
	}

	public static String  getProjectVersion(){
		return VERSION;
	}

	public static String getCopyright(){
		return COPYRIGHT;
	}

	/**
	 * With the addition of Skeletal Entities 
	 * @return
	 */
	public static boolean canRenderSkeletalEntities(){
		return cam != null;
	}

	public static final class ResourceManager {

		private final Files fSys = Gdx.app.getFiles();
		private final ArrayMap<String, FileHandle> handles = new ArrayMap<String, FileHandle>();
		private final Array<Music> music = new Array<Music>();
		private final Array<Sound> sounds = new Array<Sound>();

		private ResourceManager() {
		}

		private ResourceManager(String[] names, String[] fileNames) {
			if (names.length <= fileNames.length) {
				Gdx.app.error(
						"[INIT_WARN]",
						"Not all Files Registed by ResourceManager due to the number of FileNames being fewer than the number of Names");
				for (int i = 0; i < names.length; i++)
					registerAsset(names[i], fileNames[i]);
			} else if (names.length > fileNames.length) {
				Gdx.app.error(
						"[INIT_WARN]",
						"Not all Files Registered due to the number of Names being fewer than the number of FileNames provided to ResourceManager!");
				for (int i = 0; i < fileNames.length; i++)
					registerAsset(names[i], fileNames[i]);
			} else {
				Gdx.app.log("[INIT_SUCCESS]",
						"Confirmed! All Files and Names Registered to ResourceManager!");
				for (int i = 0; i < fileNames.length; i++)
					registerAsset(names[i], fileNames[i]);
			}
		}

		/**
		 * Registers Asset that is in the Assets Directory of the Android
		 * Project.
		 * 
		 * @param name
		 *            - Asset Name
		 * @param fileName
		 *            - Actual Path relative to the Android Assets Directory
		 */
		public void registerAsset(String name, String fileName) {
			registerAsset(name, fSys.internal(fileName));
		}

		/**
		 * Registers Asset that is in the Assets Directory of the Android
		 * Project
		 * 
		 * @param name
		 *            - Asset Name
		 * @param file
		 *            - FileHandle of Path relative to Android Assets
		 */
		public void registerAsset(String name, FileHandle file) {
			if (handles.containsValue(file, false))
				handles.removeValue(file, false);
			handles.put(name, file);
		}

		/**
		 * Registers Asset using an Absolute Path
		 * 
		 * @param name
		 *            - Asset Name
		 * @param path
		 *            - Path to create a FileHandle for
		 */
		public void registerExternalAsset(String name, String path) {
			registerExternalAsset(name, fSys.absolute(path));
		}

		/**
		 * Registers Asset Using an Absolute Path
		 * 
		 * @param name
		 *            - Asset Name
		 * @param handle
		 *            - FileHandle of Absolute Path
		 */
		public void registerExternalAsset(String name, FileHandle handle) {
			if (handles.containsValue(handle, false))
				handles.removeValue(handle, false);
			handles.put(name, handle);
		}

		/**
		 * Registers Asset Relative to SD Card Root/Root Drive
		 * 
		 * @param name
		 *            - Name of Asset
		 * @param path
		 *            - Path of Asset
		 */
		public void registerLocalAsset(String name, String path) {
			registerLocalAsset(name, fSys.external(path));
		}

		/**
		 * Registers Asset Relative to SD Card Root/Root Drive
		 * 
		 * @param name
		 *            - Name of Asset
		 * @param file
		 *            - FileHandle of Asset
		 */
		public void registerLocalAsset(String name, FileHandle file) {
			if (handles.containsValue(file, false))
				handles.removeValue(file, false);
			handles.put(name, file);
		}

		/**
		 * De-Register Asset by Name
		 * 
		 * @param name
		 *            - Name of Asset
		 */
		public void removeAsset(String name) {
			handles.removeKey(name);
		}

		/**
		 * Get Asset By Name
		 * 
		 * @param name
		 *            - Name of Asset
		 * @return FileHandle of Asset
		 */
		public FileHandle getAsset(String name) {
			return handles.get(name);
		}

		/**
		 * Get Asset by Index
		 * 
		 * @param index
		 *            - Index of Asset
		 * @return FileHandle of Asset
		 */
		public FileHandle getAsset(int index) {
			return handles.getValueAt(index);
		}

		public Music getMusic(FileHandle musicFile) {
			String extension = musicFile.extension();
			if (extension.equalsIgnoreCase("mp3")
					|| extension.equalsIgnoreCase("wav")
					|| extension.equalsIgnoreCase("ogg")) {
				Music muse = (Gdx.audio.newMusic(musicFile));
				music.add(muse);
				return muse;
			}

			return null;
		}

		public Music getMusic(String name) {
			if (handles.containsKey(name)) {
				return getMusic(handles.get(name));
			} else {
				return null;
			}
		}

		public Sound getSound(String name) {
			if (handles.containsKey(name)) {
				return getSound(handles.get(name));
			} else {
				return null;
			}
		}

		public Sound getSound(FileHandle soundFile) {
			String extension = soundFile.extension();
			if (extension.equalsIgnoreCase("mp3")
					|| extension.equalsIgnoreCase("wav")
					|| extension.equalsIgnoreCase("off")) {
				Sound sonic = Gdx.audio.newSound(soundFile);
				sounds.add(sonic);
				return sonic;
			}


			return null;
		}

		/**
		 * Get All Registered Assets
		 * 
		 * @return Array Of Registered Assets as FileHandle's
		 */
		public Array<FileHandle> getFiles() {
			Array<FileHandle> files = new Array<FileHandle>();
			files.addAll(handles.values);
			return files;
		}

		/**
		 * Get All Registered Asset Names
		 * 
		 * @return Array of Registered Asset Names as String's
		 */
		public Array<String> getNames() {
			Array<String> names = new Array<String>();
			names.addAll(handles.keys);
			return names;
		}

	}

	public static final class InputManager implements InputProcessor{

		private InputState event;

		private InputManager(){
			event = new InputState();
		}

		public InputState getState(){
			return event;
		}

		public float getTouchChangeMagnitude(){
			return Maths.calcMagnitude(event.getDeltaTouch().x, event.getDeltaTouch().y);	
		}

		@Override
		public boolean keyDown(int keycode) {
			event.keyDown(keycode);
			return true;
		}

		@Override
		public boolean keyUp(int keycode) {
			event.keyUp(keycode);
			return true;
		}

		@Override
		public boolean keyTyped(char character) {
			StringBuilder sb = new StringBuilder("");
			sb.append(character);
			int key = Keys.valueOf(new String(sb));
			if(key == -1) return false;
			keyDown(key);
			return true;
		}

		@Override
		public boolean touchDown(int screenX, int screenY, int pointer, int button) {
			event.storeTouch(screenX, screenY);
			return event.toggleTouching();
		}

		@Override
		public boolean touchUp(int screenX, int screenY, int pointer, int button) {
			event.storeTouch(screenX, screenY);
			return event.toggleTouching();
		}

		/**
		 * Returns True if there was a change in position, else returns false
		 * @param screenX
		 * @param screenY
		 * @param pointer
		 */
		@Override
		public boolean touchDragged(int screenX, int screenY, int pointer) {
			event.storeTouchChange(screenX, screenY);
			return !event.getDeltaTouch().equals(Vector2.Zero);
		}

		/**
		 * Returns True if there was a change in position, else returns false
		 * @param screenX
		 * @param screenY
		 */
		@Override
		public boolean mouseMoved(int screenX, int screenY) {
			return event.storeMouse(screenX, screenY);
		}

		@Override
		public boolean scrolled(int amount) {
			event.scrollAmount(amount);
			return false;
		}

		public boolean isKeyTyped(char c){
			return event.isCharTyped(c);
		}

		public boolean isKeyDown(int key){
			return event.isKeyPressed(key);
		}

		public boolean isKeyNotDown(int key){
			return !event.isKeyPressed(key);
		}

	}

	public static class InputState {

		private boolean[] keys;
		private Vector2 mousePos;
		private Vector2 touchPos;
		private Vector2 deltaTouchPos;
		private boolean touching = false;
		private int scroll = 1;
		private float leniency = 5;

		public InputState(){
			keys = new boolean[310];
			mousePos = Vector2.Zero;
			touchPos = Vector2.Zero;
			deltaTouchPos = Vector2.Zero;
		}

		public void keyDown(int key){
			keys[key] = true;
		}

		public void keyUp(int key){
			keys[key] = false;
		}

		public void scrollAmount(int sA){
			if(sA == 1 && scroll < 1) scroll = 1;
			else scroll += sA;
		}

		public boolean storeMouse(float x, float y){
			if(Maths.abs(mousePos.x - x) > leniency || Maths.abs(mousePos.y - y) > leniency){
				mousePos.set(x, y);
				return true;
			}
			mousePos.set(x, y);
			return false;
		}

		public void setLeniency(float leniency){
			this.leniency = leniency;
		}

		public void storeTouch(float x, float y){
			touchPos.set(x, y);
		}

		public void storeTouchChange(float x, float y){
			deltaTouchPos.set(x, y);
		}

		public boolean toggleTouching(){
			return (touching = !touching);
		}

		public boolean isTouching(){
			return touching;
		}

		public boolean isKeyPressed(int key){
			if(key == -1) return true;
			return keys[key];
		}

		public boolean isKeyNotPressed(int key){
			return !keys[key];
		}

		public boolean isCharTyped(char key){
			StringBuilder sb = new StringBuilder("");
			sb.append(key);
			int k = Keys.valueOf(new String(sb));
			if(k == -1) return false;
			return keys[k];
		}

		public Vector2 getMousePos(){return mousePos;}
		public Vector2 getTouchPos(){return touchPos;}
		public Vector2 getDeltaTouch(){return touchPos.sub(deltaTouchPos);}
		public int getScrollAmount(){return scroll;}

	}

}

class StatisticsTask extends RepeatedTask{

	Analyzable analyze;

	public StatisticsTask(long intervalMillis, Analyzable analyze) {
		super(intervalMillis);
		this.analyze = analyze;
	}

	@Override
	public void process() {
		Logger.stat(analyze);
	}

}

class ManagerDisposalThread implements Runnable{

	@Override
	public void run() {
		Manager.finishUp();
	}
	
}

class ManagerDisposalListener implements DisposalListener{

	@Override
	public void dispose() {
		Manager.finishUp();
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}
	
}

