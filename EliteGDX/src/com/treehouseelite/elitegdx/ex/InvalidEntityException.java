package com.treehouseelite.elitegdx.ex;

import com.badlogic.gdx.utils.GdxRuntimeException;

public class InvalidEntityException extends GdxRuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5042161551246694668L;

	public InvalidEntityException(){
		super("Invalid Entity Given!");
	}

	public InvalidEntityException(String msg){
		super(msg);
	}

	public InvalidEntityException(String msg, Class<?> reporting){
		super(reporting.getSimpleName() +".class -- " + msg);
	}

	public InvalidEntityException(String msg, Class<?> reporting, Class<?> entity){
		super(reporting.getSimpleName() + ".class -- " + msg +" | The Class " + entity.getSimpleName() + " is an invalid entity class! It needs to be Collidable!");
	}

}
