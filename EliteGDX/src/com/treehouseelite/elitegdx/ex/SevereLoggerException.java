package com.treehouseelite.elitegdx.ex;

/**
 * An Exception indicative of A Severe Logging Error that is Unrecoverable in the Current State <br />
 * <br />
 * This is Likely to Be Removed when Statistic Support is Finalized and the Logger is modified <br />
 * to toggle off on a severe error instead of crashing the whole application.
 * 
 * @author Matthew Crocco
 */
public class SevereLoggerException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6022911512859893474L;

	public SevereLoggerException(){
		super();
	}

	public SevereLoggerException(String msg){
		super(msg);
	}

	public SevereLoggerException(Throwable t){
		super(t);
	}

	public SevereLoggerException(String msg, Throwable cause){
		super(msg, cause);
	}

}
