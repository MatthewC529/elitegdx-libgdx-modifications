package com.treehouseelite.elitegdx.ex;

import com.badlogic.gdx.utils.GdxRuntimeException;

public class FailedInvocationException extends GdxRuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8255484198597482109L;

	public FailedInvocationException(String msg){
		super(msg);
	}

	public FailedInvocationException(Throwable t){
		super(t);
	}

	public FailedInvocationException(String msg, Throwable cause){
		super(msg, cause);
	}

}
