package com.treehouseelite.elitegdx.ex;

import com.badlogic.gdx.utils.GdxRuntimeException;

/**
 * An Exception indicative of an Unimplemented Feature
 * @author Matthew Crocco
 *
 */
public class UnimplementedException extends GdxRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2102735466302202383L;

	public UnimplementedException(String msg){
		super(msg);
	}

	public UnimplementedException(Throwable t){
		super(t);
	}

	public UnimplementedException(String msg, Throwable cause){
		super(msg, cause);
	}

}
