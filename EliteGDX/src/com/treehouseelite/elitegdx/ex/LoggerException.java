package com.treehouseelite.elitegdx.ex;

import com.badlogic.gdx.utils.GdxRuntimeException;

/**
 * An Exception indicative of a Recoverable Logging Error that is Noteworthy but Surviviable
 * @author Matthew Crocco
 *
 */
public class LoggerException extends GdxRuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8371799085222768018L;

	public LoggerException(String msg){
		super(msg);
	}

	public LoggerException(Throwable t){
		super(t);
	}

	public LoggerException(String msg, Throwable cause){
		super(msg, cause);
	}

}
