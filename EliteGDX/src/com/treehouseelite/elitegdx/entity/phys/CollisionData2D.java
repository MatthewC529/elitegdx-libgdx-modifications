package com.treehouseelite.elitegdx.entity.phys;

import lombok.Data;

import com.badlogic.gdx.math.Vector2;
import com.treehouseelite.elitegdx.math.BoundingBox2D;

public @Data class CollisionData2D {

	public final Collidable<?> collider, collidee;
	public final Vector2 colliderPos, collideePos;
	public final BoundingBox2D colliderBox, collideeBox;

	public CollisionData2D(Collidable<?> collider, Collidable<?> collidee){
		this.collider = collider;
		this.collidee = collidee;
		this.colliderPos = collider.getCurPos();
		this.collideePos = collidee.getCurPos();
		this.colliderBox = collider.getCollisionShape();
		this.collideeBox = collidee.getCollisionShape();
	}

}
