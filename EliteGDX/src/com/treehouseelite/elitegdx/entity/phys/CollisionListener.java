package com.treehouseelite.elitegdx.entity.phys;

public interface CollisionListener {

	void onCollide(CollisionData2D data);
}
