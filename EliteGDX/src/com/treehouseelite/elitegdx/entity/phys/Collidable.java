package com.treehouseelite.elitegdx.entity.phys;

import com.treehouseelite.elitegdx.entity.Skeleton;
import com.treehouseelite.elitegdx.math.BoundingBox2D;

public interface Collidable<T extends Collidable> extends Skeleton {

	BoundingBox2D getCollisionShape();
	BoundingBox2D setCollisionShape(BoundingBox2D shape);
	BoundingBox2D updateCollisionShape();

	BoundingBox2D setCollisionShape();

}
