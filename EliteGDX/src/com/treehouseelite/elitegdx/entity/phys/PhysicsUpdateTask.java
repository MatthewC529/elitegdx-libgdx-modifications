package com.treehouseelite.elitegdx.entity.phys;

import java.util.concurrent.atomic.AtomicReferenceArray;

import com.treehouseelite.elitegdx.Manager;
import com.treehouseelite.elitegdx.util.collections.AtomicUtils;
import com.treehouseelite.elitegdx.util.event.RepeatedTask;

public class PhysicsUpdateTask extends RepeatedTask{

	private static AtomicReferenceArray<PhysicsUpdateHook> hooks = new AtomicReferenceArray<PhysicsUpdateHook>(100);
	private static int index = 0;

	public PhysicsUpdateTask() {
		super();
	}

	@Override
	public void process() {
		if(!Manager.getGameReference().isPlayerActive()) return;
		PhysicsManager.checkCollisions();
		for(int i = 0; i < index;i++)
			if(hooks.get(i) != null) hooks.get(i).run();
	}

	public static void addUpdateHook(PhysicsUpdateHook hook){
		if(index+1 > hooks.length()-1){
			hooks = AtomicUtils.<PhysicsUpdateHook>deflate(hooks);
			index = AtomicUtils.determineEndPointOfValues(hooks) + 1;
		}
		hooks.set(index, hook);
		index++;
	}

	public static void removeUpdateHook(PhysicsUpdateHook hook){
		int index = AtomicUtils.getIndexOf(hooks, hook);
		if(index != -1)
			hooks.set(index, null);
	}

}
