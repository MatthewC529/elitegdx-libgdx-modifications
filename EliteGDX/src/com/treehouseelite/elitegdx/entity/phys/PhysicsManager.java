package com.treehouseelite.elitegdx.entity.phys;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.Array;
import com.treehouseelite.elitegdx.entity.GhostEntity;
import com.treehouseelite.elitegdx.ex.InvalidEntityException;
import com.treehouseelite.elitegdx.math.Line;
import com.treehouseelite.elitegdx.math.Maths;
import com.treehouseelite.elitegdx.util.Analyzable;

public final class PhysicsManager implements Analyzable{

	public static Array<CollisionListener> entities = new Array<CollisionListener>(); 

	private PhysicsManager(){}

	public static void addCollisionListener(CollisionListener list){
		if(list instanceof GhostEntity || !(list instanceof Collidable)) throw new InvalidEntityException("Invalid Entity Provided! Does not implement Collidable!", PhysicsManager.class, list.getClass());
		entities.add(list);
	}

	public static void removeCollisionListener(CollisionListener collidable){
		entities.removeValue(collidable, false);
	}

	public static RayData raycast(Vector2 pos, Vector2 dir, float radians, float distance, Collidable<?> caller){
		dir.nor();
		dir.set(Maths.rotateVector(pos, dir, radians));

		Vector3 position = new Vector3(pos.x, pos.y, 0);
		Vector3 direction = new Vector3(dir.x, dir.y, 0);
		Vector3 end = new Vector3();

		Ray ray = new Ray(position, direction);
		end.set(ray.getEndPoint(end, distance));
		Line l = new Line(position, end);

		for(Vector2 point: l.getPoints())
			for(CollisionListener co: entities){
				Collidable<?> c = (Collidable<?>) co;
				if(c.getCollisionShape().getShape().contains(point) && c != caller)
					return new RayData(c, point);
			}


		return new RayData();
	}

	public static void checkCollisions(){
		Array<CollisionListener> temp = new Array<CollisionListener>(entities);
		for(CollisionListener curr: entities){
			for(CollisionListener targ: temp){
				Collidable<?> tar = (Collidable<?>) targ;
				Collidable<?> cur = (Collidable<?>) curr;
				if(curr == targ) continue;
				if(cur.getCollisionShape().intersects(tar.getCollisionShape().getShape())){
					curr.onCollide(new CollisionData2D(cur, tar));
				}
			}
			temp.removeValue(curr, false);//Slight Optimization since we know it isnt colliding with anything.
		}

		temp.clear();
	}

	public static void dispose(){
		entities.clear();
	}

	@Override
	public String getPrintableStatistics() {
		return "REG_PHYS_E = " + entities.size;
	}

}

class RayData{

	public final Collidable<?> entity;
	public final Vector2 collisionPoint;
	public final boolean hasCollision;

	public RayData(){
		hasCollision = false;
		entity = null;
		collisionPoint = null;
	}

	public RayData(Collidable<?> entity, Vector2 collisionPoint){
		hasCollision = true;
		this.entity = entity;
		this.collisionPoint = collisionPoint;
	}

}
