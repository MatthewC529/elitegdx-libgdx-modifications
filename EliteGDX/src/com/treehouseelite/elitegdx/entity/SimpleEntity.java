package com.treehouseelite.elitegdx.entity;

import java.util.concurrent.atomic.AtomicBoolean; 
import java.util.concurrent.atomic.AtomicInteger;

import lombok.Getter;
import lombok.Setter;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.treehouseelite.elitegdx.Finishable;
import com.treehouseelite.elitegdx.Manager;
import com.treehouseelite.elitegdx.entity.phys.Collidable;
import com.treehouseelite.elitegdx.math.BoundingBox2D;

/**
 * A Simple Implementation of an Entity in its Second Iteration. <br />
 * <br/ >
 * This entity implements the Skeleton, Collidable and Finishable interfaces. Finishable described how the object is disposed at exit-time. <br />
 * 
 * @author Matthew Crocco
 */
public abstract class SimpleEntity implements Collidable<SimpleEntity>, Finishable{
	
	public final long entityId;
	public static final @Getter AtomicInteger entityCount = new AtomicInteger(1);
	public static @Getter String ID;
	
	public final @Getter AtomicBoolean disposed = new AtomicBoolean(false);
	
	private @Getter @Setter Vector2 curPos;
	private @Getter Vector2 prevPos;
	private @Getter Vector2 spawnPos;
	
	public @Getter @Setter Direction direction;
	public @Getter @Setter Vector2 velocity;
	
	private final Texture tex;
	private final Sprite skin;
	
	private BoundingBox2D boundingShape;
	
	public SimpleEntity(final float x, final float y, final Texture tex){
		this.tex = tex;
		skin = new Sprite(tex);
		curPos = new Vector2(x, y);
		spawnPos = new Vector2(x, y);
		prevPos = new Vector2(x, y);
		velocity = new Vector2(0, 0);
		skin.setPosition(x, y);
		direction = Direction.UP;
		entityId = Manager.getOrganizer().getUniqueID(this);
		setCollisionShape();
		updateCollisionShape();
		entityCount.incrementAndGet();
		Manager.addFinalizer(this);
	}
	
	public SimpleEntity(final Vector2 pos, final Texture tex){
		this(pos.x, pos.y, tex);
	}
	
	@Override
	public Texture getSkin() {
		return tex;
	}
	
	@Override
	public Sprite getSprite(){
		return skin;
	}

	@Override
	public void draw(final SpriteBatch batch){
		batch.draw(this.skin, curPos.x, curPos.y);
	}
	
	@Override
	public void updateMovement(){
		curPos.add(velocity);
		this.skin.setPosition(curPos.x, curPos.y);
		updateCollisionShape();
	}
	
	@Override
	public BoundingBox2D setCollisionShape(final BoundingBox2D shape){
		return (boundingShape = shape);
	}
	
	@Override
	public BoundingBox2D getCollisionShape(){
		return boundingShape;
	}
	
	@Override
	public void finishUp(){
		Manager.getLogger().finest(String.format("ID%s - %s at (%s, %s) is being Disposed...", entityId, this.getClass().getSimpleName(), curPos.x, curPos.y)); 
		tex.dispose();
		Manager.getOrganizer().receiveMessage(new EntityDeathMessage(entityId));
		
		curPos = Vector2.Zero;
		prevPos = Vector2.Zero;
		spawnPos = Vector2.Zero;
		velocity = Vector2.Zero;
		entityCount.decrementAndGet();
		disposed.set(true);
		
		this.onFinish();
	}
	
	public BoundingBox2D setCollisionShape(){
		this.setCollisionShape(new BoundingBox2D(skin));
		return boundingShape;
	}
	
	public BoundingBox2D updateCollisionShape(){
		setCollisionShape();
		return boundingShape;
	}
	
	@Override
	public boolean equals(Object o){
		if(o.getClass() != getClass()) return false;
		
		SimpleEntity other = (SimpleEntity) o;
		
		if(other.entityId == this.entityId)
			return true;
		else
			return false;
	}
	

	public abstract void update(final float delta);
	public abstract void onFinish();
	public abstract void sleep();
	public abstract void wakeUp();
	public abstract boolean isSleeping();
	
}
