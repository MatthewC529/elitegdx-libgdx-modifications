package com.treehouseelite.elitegdx.entity;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import lombok.Getter;
import lombok.Setter;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.treehouseelite.elitegdx.Finishable;
import com.treehouseelite.elitegdx.Logger;
import com.treehouseelite.elitegdx.Manager;
import com.treehouseelite.elitegdx.util.Utility;

//TODO Ensure Equivalency to SimpleEntity, may be out of date!
/**
 * A Simple Entity that DOES NOT Collide. <br />
 * Otherwise it is very similar to SimpleEntity.
 * @author Matthew Crocco
 * @see com.treehouseelite.elitegdx.entity.SimpleEntity SimpleEntity.class
 */
public abstract class GhostEntity implements Skeleton, Finishable {

	public final long ghostId;
	private static @Getter AtomicInteger ghostCount = new AtomicInteger(0);

	private @Getter @Setter Vector2 curPos, velocity;
	private @Getter Vector2 prevPos, spawnPos;
	public @Getter @Setter Direction direction;

	private final Texture tex;
	private final Sprite skin;

	public @Getter @Setter AtomicBoolean disposed = new AtomicBoolean(false);

	public GhostEntity(float x, float y, Texture tex){
		this.tex = tex;
		skin = new Sprite(tex);
		curPos = new Vector2(x, y);
		prevPos = new Vector2(curPos);
		spawnPos = new Vector2(curPos);
		velocity = new Vector2(0, 0);
		skin.setPosition(x, y);
		direction = Direction.UP;
		ghostId = Utility.createUniqueID(this);
		ghostCount.getAndIncrement();
		Manager.addFinalizer(this);
	}

	public GhostEntity(Vector2 pos, Texture tex){
		this(pos.x, pos.y, tex);
	}

	@Override
	public void draw(SpriteBatch batch) {
		batch.draw(this.skin, curPos.x, curPos.y);
	}

	@Override
	public void updateMovement() {
		curPos.add(velocity);
		this.skin.setPosition(curPos.x, curPos.y);
	}

	@Override
	public Texture getSkin() {
		return tex;
	}

	@Override
	public Sprite getSprite() {
		return skin;
	}

	@Override
	public void finishUp(){
		Manager.getLogger();
		Logger.finest(String.format("ID%s - %s at (%s, %s) is being Disposed... [GHOST]", ghostId, this.getClass().getSimpleName(), curPos.x, curPos.y)); 
		tex.dispose();
		Utility.releaseUniqueID(ghostId);

		curPos = Vector2.Zero;
		prevPos = Vector2.Zero;
		spawnPos = Vector2.Zero;
		velocity = Vector2.Zero;
		ghostCount.decrementAndGet();
		disposed.set(true);
	}

	public abstract void update();

	@Override
	public boolean equals(Object o){
		if(o.getClass() != getClass()) return false;

		GhostEntity other = (GhostEntity) o;

		if(other.ghostId == this.ghostId)
			return true;
		else
			return false;
	}


}
