package com.treehouseelite.elitegdx.entity;

import com.treehouseelite.elitegdx.util.event.Message;

//TODO Replace with Listeners
@Deprecated
/**
 * A Legacy Class that remains temporarily... DO NOT USE!
 * @author matthew
 *
 */
public class EntityDeathMessage implements Message<Long>{

	public final long id;

	public EntityDeathMessage(long id){
		this.id = id;
	}

	@Override
	public Long getContent() {
		return id;
	}

	@Override
	public MessageType getMessageType() {
		return MessageType.ENTITY_DEATH;
	}

}
