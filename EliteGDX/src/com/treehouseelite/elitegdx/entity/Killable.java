package com.treehouseelite.elitegdx.entity;

import com.treehouseelite.elitegdx.Finishable;

public interface Killable extends Finishable{

	public void die();
	public boolean isDead();
	public void kill();
	public boolean isKillable();

}
