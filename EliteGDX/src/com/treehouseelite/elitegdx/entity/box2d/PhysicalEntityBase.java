package com.treehouseelite.elitegdx.entity.box2d;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.treehouseelite.elitegdx.math.Maths;

/**
 * An Abstract Adapter-Like Physical Entity that Auto-Implements Less Necessary Methods
 * @author Matthew Crocco
 *
 */
public abstract class PhysicalEntityBase implements PhysicalEntity{

	public void update(){
		getCurrentPos().set(getCenter());
		getSprite().setRotation(Maths.toDegrees(getBody().getAngle()));
		getSprite().setPosition(getCurrentPos().x, getCurrentPos().y);
	}
	
	public void draw(SpriteBatch batch){
		update();
		getSprite().draw(batch);
	}
	
	public void setBodyPosition(Vector2 v, boolean nullifyVelocity){
		getBody().getPosition().set(v);
		if(nullifyVelocity)
			getBody().getLinearVelocity().set(0, 0);
	}
	
	public Vector2 getVelocity(){
		if(getBody() == null) throw new NullPointerException("This Entity Doesn't Have a Physics Body!");
		return getBody().getLinearVelocity();
	}
	
	public float getAngularVelocity(){
		if(getBody() == null) throw new NullPointerException("This Entity Doesn't Have a Physics Body!");
		return getBody().getAngularVelocity();
	}
	
	public Vector2 getPosition(){
		if(getBody() == null) throw new NullPointerException("This Entity Doesn't Have a Physics Body!");
		return getBody().getPosition();
	}
	
	public Vector2 getCenter(){
		if(getBody() == null) throw new NullPointerException("This Entity Doesn't Have a Physics Body!");
		return getBody().getWorldCenter();
	}
	
}
