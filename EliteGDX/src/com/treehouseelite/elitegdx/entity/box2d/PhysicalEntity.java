package com.treehouseelite.elitegdx.entity.box2d;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.treehouseelite.elitegdx.Finishable;
import com.treehouseelite.elitegdx.math.Maths;

/**
 * Describes a Physical Entity
 * 
 * @author Matthew Crocco
 */
public interface PhysicalEntity extends Finishable{

	/** Apply Any Necessary Updates here! The draw() method should auto-call this. If you override draw() MAKE SURE YOU CALL THIS! */
	void update();

	/** Draw the Sprite of this entity. Any changes to sprite position and rotation should be done in update()!*/
	void draw(SpriteBatch batch);

	/**Add a Fixture to the Current Entity*/
	void addFixture(FixtureDef def);
	/**Remove a Fixture at the Given Index*/
	void removeFixture(int index);
	/**Remove a Fixture equivalent to the Given Fixture*/
	void removeFixture(Fixture fixture);
	/**Remove a Fixture with the given FixtureDefinition*/
	void removeFixture(FixtureDef def);
	/**Get the Current Number of Fixtures*/
	int getFixtureCount();

	/**Apply a Force to the Center Of Mass*/
	void applyForce(Vector2 force);
	/**Apply a Force at the given World Position*/
	void applyForce(Vector2 force, Vector2 pos);
	/**Apply an Impulse to the Center of Mass*/
	void applyImpulse(Vector2 impulse);
	/**Apply an Impulse to the Center of Mass*/
	void applyImpulse(Vector2 impulse, Vector2 pos);

	BodyDef getBodyDef();
	Body getBody();
	Texture getTex();
	Sprite getSprite();
	Vector2 getCurrentPos();
	Vector2 getSpawnPos();

	/**Set the Entities Position to a Given World Position.
	 * 
	 * @param v - World Position to Teleport To
	 * @param nullifyVelocity - Zero the Velocity
	 */
	void setBodyPosition(Vector2 v, boolean nullifyVelocity);

	/**Get the current Body's Linear Velocity*/
	Vector2 getVelocity();
	
	/**Get the current Body's Angular Velocity*/
	float getAngularVelocity();
	
	/**Get the current Body's World Position*/
	Vector2 getPosition();
	
	/**Get the current Body's Center of Mass*/
	Vector2 getCenter();
}
