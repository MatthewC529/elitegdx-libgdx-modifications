package com.treehouseelite.elitegdx.entity.box2d;

import java.util.concurrent.atomic.AtomicInteger;

import lombok.Getter;
import lombok.Setter;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.treehouseelite.elitegdx.Manager;
import com.treehouseelite.elitegdx.util.Utility;
import com.treehouseelite.elitegdx.util.collections.ArrayUtils;

public class SimplePhysical extends PhysicalEntityBase{

	private static final AtomicInteger eCount = new AtomicInteger(0);

	private final @Getter long entityId;
	private @Getter @Setter BodyDef bodyDef;
	private @Getter Body body;
	private @Getter BodyType bodyType;
	private @Getter @Setter Texture tex;
	private @Getter @Setter Sprite sprite;

	private @Getter Vector2 currentPos, spawnPos;
	private FixtureData[] fixtures = new FixtureData[5];
	private int fixIndex = 0;

	public SimplePhysical(Texture tex, BodyType type, Vector2 pos,Vector2 linearVelocity, float angle, float gravityScale, boolean bullet, boolean canRotate){
		bodyDef = new BodyDef();
		bodyDef.position.set(pos);
		bodyDef.linearVelocity.set(linearVelocity);
		bodyDef.angle = angle;
		bodyDef.gravityScale = gravityScale;
		bodyDef.bullet = bullet;
		bodyDef.fixedRotation = !canRotate;
		bodyDef.type = type;

		this.bodyType = type;
		this.tex = tex;
		this.sprite = new Sprite(tex);
		this.currentPos = pos;
		this.spawnPos = pos;
		this.entityId = Utility.createUniqueID(this);

		body = Box2DManager.constructBody(bodyDef);
		Manager.addFinalizer(this);
		eCount.incrementAndGet();
	}

	public SimplePhysical(BodyDef def, Texture tex){
		this.tex = tex;
		this.bodyType = def.type;
		this.entityId = Utility.createUniqueID(this);
		this.sprite = new Sprite(tex);
		this.currentPos = def.position;
		this.spawnPos = def.position;
		body = Box2DManager.constructBody(def);
		Manager.addFinalizer(this);
		eCount.incrementAndGet();
	}

	public SimplePhysical(Body body, Texture tex){
		bodyDef = new BodyDef();
		bodyDef.type = body.getType();
		bodyDef.position.set(body.getPosition());
		bodyDef.angle = body.getAngle();
		bodyDef.gravityScale = body.getGravityScale();
		bodyDef.bullet = body.isBullet();
		bodyDef.fixedRotation = body.isFixedRotation();
		bodyDef.linearVelocity.set(body.getLinearVelocity());
		bodyDef.active = body.isActive();
		bodyDef.awake = body.isAwake();
		bodyDef.allowSleep = body.isSleepingAllowed();
		bodyDef.angularDamping = body.getAngularDamping();
		bodyDef.angularVelocity = body.getAngularVelocity();
		bodyDef.linearDamping = body.getLinearDamping();

		this.bodyType = bodyDef.type;
		this.tex = tex;
		this.sprite = new Sprite(tex);
		this.entityId = Utility.createUniqueID(this);
		this.currentPos = body.getPosition();
		this.spawnPos = body.getPosition();

		for(Fixture f: body.getFixtureList())
			addFixture(f);

		Manager.addFinalizer(this);
		eCount.incrementAndGet();
	}

	@Override
	public void addFixture(FixtureDef fixture){
		if(fixIndex == fixtures.length)
			fixtures = ArrayUtils.<FixtureData>resizeArray(fixtures, fixtures.length+5);

		fixtures[fixIndex] = new FixtureData(fixture, body.createFixture(fixture));
		fixIndex++;
	}

	private void addFixture(Fixture f){
		FixtureDef fd = new FixtureDef();
		fd.density = f.getDensity();
		fd.filter.categoryBits = f.getFilterData().categoryBits;
		fd.filter.groupIndex = f.getFilterData().groupIndex;
		fd.filter.maskBits = f.getFilterData().maskBits;
		fd.friction = f.getFriction();
		fd.isSensor = f.isSensor();
		fd.restitution = f.getRestitution();
		fd.shape = f.getShape();

		if(fixIndex == fixtures.length)
			fixtures = ArrayUtils.<FixtureData>resizeArray(fixtures, fixtures.length+5);

		fixtures[fixIndex] = new FixtureData(fd, f);
		fixIndex++;
	}

	@Override
	public void removeFixture(int index){
		body.destroyFixture(fixtures[index].fixture);
		fixtures[index] = null;
		fixIndex--;
		fixtures = ArrayUtils.<FixtureData>condenseArray(fixtures);
	}

	@Override
	public void removeFixture(Fixture fixture){
		for(int i = 0; i < fixtures.length; i ++){
			FixtureData fix = fixtures[i];
			if(fix.fixture.equals(fixture) || fix.fixture == fixture){
				removeFixture(i);
				break;
			}
		}
	}

	@Override
	public void removeFixture(FixtureDef def){
		for(int i = 0; i < fixtures.length; i++){
			FixtureData fix = fixtures[i];
			if(fix.definition.equals(def) || fix.definition == def){
				removeFixture(i);
				break;
			}
		}
	}

	@Override
	public int getFixtureCount(){
		return fixIndex;
	}

	@Override
	public void applyForce(Vector2 force){
		body.applyForceToCenter(force, true);
	}

	@Override
	public void applyForce(Vector2 force, Vector2 pos){
		body.applyForce(force, pos, true);
	}

	@Override
	public void applyImpulse(Vector2 impulse){
		body.applyLinearImpulse(impulse, body.getWorldCenter(), true);
	}

	@Override
	public void applyImpulse(Vector2 impulse, Vector2 position){
		body.applyLinearImpulse(impulse, position, true);
	}

	@Override
	public void finishUp() {
		fixtures = null;
		currentPos = Vector2.Zero;
		spawnPos = Vector2.Zero;
		tex.dispose();
		Utility.releaseUniqueID(entityId);
		eCount.decrementAndGet();
	}

	public static int getEntityCount(){
		return eCount.intValue();
	}

	@Override
	public void sleep() {
		
	}

	@Override
	public void wakeUp() {
		
	}

	@Override
	public boolean isSleeping() {
		return false;
	}

}
