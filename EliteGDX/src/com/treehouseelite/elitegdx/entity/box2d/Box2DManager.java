package com.treehouseelite.elitegdx.entity.box2d;

import java.util.concurrent.atomic.AtomicBoolean;

import lombok.Getter;
import lombok.Setter;
import aurelienribon.bodyeditor.BodyEditorLoader;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.treehouseelite.elitegdx.Manager;
import com.treehouseelite.elitegdx.util.Utility;
import com.treehouseelite.elitegdx.util.Utility.Hook;
import com.treehouseelite.elitegdx.util.event.RepeatedTask;

/**
 * Manages Box2D Entities and World data.
 * 
 * @author Matthew Crocco
 */
public class Box2DManager {

	private static @Getter @Setter World world;
	private static @Getter @Setter float worldStep;
	private static @Getter @Setter int bodyIterations = 4, velocityIterations = 8;
	private static @Getter Box2DDebugRenderer debugRenderer;
	private static @Getter @Setter long updateInterval = 1;

	/** Controls Debug Rendering for Box2D shapes*/
	public static final AtomicBoolean debug = new AtomicBoolean(false);

	/**
	 * Initialize Box2DManager to allow use!
	 * @param worldStep
	 * @param gravity
	 */
	public static void init(float worldStep, Vector2 gravity){
		world = new World(gravity, true);
		Box2DManager.worldStep = worldStep;
		debugRenderer = new Box2DDebugRenderer();
		Utility.putHook(new PhysicsUpdateTask(updateInterval), Hook.GDX_RENDER);
	}

	/**
	 * Initialize Box2DManager to Allow Use! <br />
	 * <br />
	 * Defaults WorldStep to 1/100
	 * @param gravity
	 */
	public static void init(Vector2 gravity){
		init(1/100f, gravity);
	}

	/**
	 * Initialize Box2DManager to Allow Use! <br />
	 * <br />
	 * Defaults WorldStep to 1/100 <br />
	 * Defaults Gravity to (0, -10)
	 */
	public static void init(){
		init(1/100f, new Vector2(0, -10));
	}

	/**
	 * Consturct Body in Physics World
	 * @param definition
	 * @return
	 */
	public static Body constructBody(BodyDef definition){
		return world.createBody(definition);
	}

	/**
	 * Construct Body using BodyEditory Files
	 * @param json
	 * @param fixtureName
	 * @param type
	 * @param pos
	 * @param friction
	 * @param density
	 * @param restitution
	 * @param scale
	 * @return
	 */
	public static LoadedBodyInfo constructBody(FileHandle json, String fixtureName, BodyType type, Vector2 pos, float friction, float density, float restitution, float scale){
		BodyEditorLoader loader = new BodyEditorLoader(json);

		BodyDef bd = new BodyDef();
		bd.type = type;
		bd.position.set(pos);

		FixtureDef fd = new FixtureDef();
		fd.density = density;
		fd.friction = friction;
		fd.restitution = restitution;

		Body body = world.createBody(bd);
		loader.attachFixture(body, fixtureName, fd, scale);

		return new LoadedBodyInfo(body, bd, fd);
	}

	/**
	 * Construct Body from BodyEditory Files
	 * @param json
	 * @param fixtureName
	 * @param bDef
	 * @param fDef
	 * @param scale
	 * @return
	 */
	public static LoadedBodyInfo constructBody(FileHandle json, String fixtureName, BodyDef bDef, FixtureDef fDef, float scale){
		BodyEditorLoader loader = new BodyEditorLoader(json);
		Body body = world.createBody(bDef);
		loader.attachFixture(body, fixtureName, fDef, scale);

		return new LoadedBodyInfo(body, bDef, fDef);
	}

	/**
	 * Construct Simple Physical Entity from Body and Texture
	 * @param body
	 * @param tex
	 * @return
	 */
	public static SimplePhysical constructEntity(Body body, Texture tex){
		return new SimplePhysical(body, tex);
	}
	
	/**
	 * Constructs a Simple Physical Entity using BodyEditory Files
	 * @param json
	 * @param tex
	 * @param fixtureName
	 * @param type
	 * @param pos
	 * @param friction
	 * @param density
	 * @param restitution
	 * @param scale
	 * @return
	 */
	public static SimplePhysical constructEntity(FileHandle json, Texture tex, String fixtureName, BodyType type, Vector2 pos, float friction, float density, float restitution, float scale){
		return new SimplePhysical(constructBody(json, fixtureName, type, pos, friction, density, restitution, scale).body, tex);
	}

	/**
	 * Constucts Simple Physical Entity using BodyEditory Files
	 * @param json
	 * @param fixtureName
	 * @param tex
	 * @param bDef
	 * @param fDef
	 * @param scale
	 * @return
	 */
	public static SimplePhysical constructEntity(FileHandle json, String fixtureName, Texture tex, BodyDef bDef, FixtureDef fDef, float scale){
		return new SimplePhysical(constructBody(json, fixtureName, bDef, fDef, scale).body, tex);
	}

	protected static void step(){
		world.step(worldStep, velocityIterations, bodyIterations);
		if(debug.get())
			debugRenderer.render(world, Manager.getCam().combined);
	}

}

class PhysicsUpdateTask extends RepeatedTask{

	public PhysicsUpdateTask(long interval) {
		super(interval);
	}

	@Override
	public void process() {
		Box2DManager.step();
	}

}