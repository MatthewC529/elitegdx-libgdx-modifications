package com.treehouseelite.elitegdx.entity.box2d;

import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;

/**
 * Fixture Data Container
 * 
 * @author Matthew Crocco
 */
public class FixtureData {

	public final FixtureDef definition;
	public final Fixture fixture;

	public FixtureData(FixtureDef def, Fixture fix){
		definition = def;
		fixture = fix;
	}

}
