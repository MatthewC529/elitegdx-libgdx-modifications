package com.treehouseelite.elitegdx.entity.box2d;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;

/**
 * BodyEditor Loaded Data Container
 * 
 * @author Matthew Crocco
 */
public class LoadedBodyInfo {

	public final Body body;
	public final BodyDef bodyDef;
	public final FixtureDef fixtureDef;

	public LoadedBodyInfo(Body body, BodyDef bodyDef, FixtureDef fixtureDef){
		this.body = body;
		this.bodyDef = bodyDef;
		this.fixtureDef = fixtureDef;
	}

}
