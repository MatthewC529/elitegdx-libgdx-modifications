package com.treehouseelite.elitegdx.entity;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.treehouseelite.elitegdx.ex.UnimplementedException;

/**
 * The Skeletal Description of an EliteGDX Entity (Does Not Collide by Default)
 * @author Matthew Crocco
 *
 * @param <T>
 */
public interface Skeleton{

	/**
	 * The Direction the Entity is Moving
	 * @author Matthew Crocco
	 */
	public enum Direction{
		UP,
		DOWN,
		LEFT,
		RIGHT;
	}

	void draw(SpriteBatch batch);
	void update(float delta);
	void updateMovement();
	Texture getSkin();
	Sprite getSprite();
	Direction getDirection();
	Vector2 getCurPos();
	void setCurPos(Vector2 pos);

}
