package com.treehouseelite.elitegdx.entity.cutscene;

import java.util.ArrayDeque;
import java.util.Arrays;

public class CutsceneManager {

	private CutsceneManager(){}

	private static ArrayDeque<Cutscene> cutscenes;

	public static void init(){
		if(cutscenes != null) return;

		cutscenes = new ArrayDeque<Cutscene>();
	}

	public static void appendCutscene(Cutscene scene){
		cutscenes.add(scene);
	}

	public static void prependCutscene(Cutscene scene){
		cutscenes.push(scene);
	}

	public static void insertCutscene(Cutscene scene, int index){
		Cutscene[] temp = new Cutscene[cutscenes.size()+1];
		temp[index] = scene;
		for(int i = 0; i < cutscenes.size(); i++){
			if(i != index){
				temp[i] = cutscenes.pollFirst();
			}
		}

		if(cutscenes.size() != 0) cutscenes.clear();
		cutscenes.addAll(Arrays.asList(temp));
	}

	public static Cutscene getNextCutscene(){
		return cutscenes.pollFirst();
	}

	public static Cutscene getLastCutscene(){
		return cutscenes.pollLast();
	}

	public static Cutscene getCutsceneAt(int index){
		Cutscene scene = cutscenes.toArray(new Cutscene[cutscenes.size()])[index];
		cutscenes.remove(scene);
		return scene;
	}
}
