package com.treehouseelite.elitegdx.entity.cutscene;

import com.badlogic.gdx.math.Vector2;

public class Keyframe {

	public final Vector2 vector;
	public final float x, y;

	public Keyframe(float x, float y){
		this.x = x;
		this.y = y;
		this.vector = new Vector2(x, y);
	}

	public Keyframe(Vector2 vector){
		this.vector = vector;
		this.x = vector.x;
		this.y = vector.y;
	}

	@Override
	public boolean equals(Object o){
		if(o instanceof Keyframe){

			Keyframe other = (Keyframe)o;

			if(other.vector.equals(vector))
				return true;
		}

		return false;
	}

}
