package com.treehouseelite.elitegdx.entity.cutscene;

import lombok.NonNull;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteCache;
import com.badlogic.gdx.utils.Array;
import com.treehouseelite.elitegdx.Finishable;
import com.treehouseelite.elitegdx.Manager;

public class Cutscene implements Finishable{
	
	private final Array<Actor> actors;
	private final Array<Actor> dynamic;
	private float speed;
	private boolean started = false;
	private SpriteCache cache;
	private SpriteBatch batch;
	private int cacheID;
	private final Camera cam;
	
	public Cutscene(float speed,@NonNull Camera cam){
		this.actors = new Array<Actor>();
		this.dynamic = new Array<Actor>();
		this.cam = cam;
		this.speed = speed;
		cache = new SpriteCache();
		batch = new SpriteBatch();
		cache.setProjectionMatrix(cam.combined);
		batch.setProjectionMatrix(cam.combined);
		Manager.addFinalizer(this);
	}
	
	public Cutscene(float speed, @NonNull Camera cam, Array<Actor> actors){
		this.actors = actors;
		this.dynamic = new Array<Actor>();
		this.cam = cam;
		this.speed = speed;
		cache = new SpriteCache();
		batch = new SpriteBatch();
		cache.setProjectionMatrix(cam.combined);
		batch.setProjectionMatrix(cam.combined);
		Manager.addFinalizer(this);
	}
	
	public Cutscene(float speed, @NonNull Camera cam, Actor... actors){
		this.actors = new Array<Actor>(actors);
		this.dynamic = new Array<Actor>();
		this.cam = cam;
		this.speed = speed;
		cache = new SpriteCache();
		batch = new SpriteBatch();
		cache.setProjectionMatrix(cam.combined);
		batch.setProjectionMatrix(cam.combined);
		Manager.addFinalizer(this);
	}

	public void updateScene(){
		if(!started){
			start();
			started = true;
		}else{
			if(Gdx.graphics.isGL30Available()){
				Gdx.gl30.glEnable(GL30.GL_BLEND);
				Gdx.gl30.glBlendFunc(GL30.GL_SRC_ALPHA, GL30.GL_ONE_MINUS_SRC_ALPHA);
			}else{
				Gdx.gl20.glEnable(GL20.GL_BLEND);
				Gdx.gl20.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
			}
			cache.begin();
			cache.draw(cacheID);
			cache.end();
			
			batch.begin();
			for(Actor a: dynamic){
				a.moveToNextFrame();
				a.draw(batch);
			}
			batch.end();
			
		}
	}
	
	private void start(){
		Array<Actor> temp = new Array<Actor>();
		cache.beginCache();
		batch.begin();
		for(Actor a: actors){
			a.interpolateMovements();
			if(a.type == Actor.Type.STATIC)
				a.drawStatic(cache);
			else{
				a.draw(batch);
				temp.add(a);
			}
		}
		
		batch.end();
		this.cacheID = cache.endCache();

		dynamic.addAll(temp);
		
	}

	@Override
	public void finishUp() {
		dynamic.clear();
		for(Actor a: actors){
			a.dispose();
		}
		actors.clear();
		cache.dispose();
		batch.dispose();
		Manager.removeFinalizer(this);
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof Cutscene){
			
			Cutscene other = (Cutscene) o;
			
			if(actors.equals(other.actors) && dynamic.equals(other.dynamic) && cam.equals(other.cam) && cacheID == other.cacheID && cache.equals(other.cache) && batch.equals(other.batch))
				return true;
			
		}
		
		return false;
		
	}

	@Override
	public void sleep() {
		
	}

	@Override
	public void wakeUp() {
		
	}

	@Override
	public boolean isSleeping() {
		return false;
	}
}
