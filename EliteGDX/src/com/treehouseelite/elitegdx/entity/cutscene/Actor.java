package com.treehouseelite.elitegdx.entity.cutscene;

import java.util.concurrent.atomic.AtomicInteger;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteCache;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.NumberUtils;
import com.treehouseelite.elitegdx.math.Interpolate;
import com.treehouseelite.elitegdx.math.Maths;
import com.treehouseelite.elitegdx.util.Utility;

public class Actor implements Disposable{

	public enum Type{
		DYNAMIC,
		STATIC;
	}

	private final long id;
	private Vector2 curPos;
	private Texture tex;
	private Sprite sprite;
	public final Type type;

	private final Array<Keyframe> keyframes = new Array<Keyframe>();
	private AtomicInteger currentFrame = new AtomicInteger(0);
	private Interpolate interpolate;

	public Actor(float x, float y, Texture tex, Interpolate movement, Type type) {
		curPos = new Vector2(x, y);
		this.tex = tex;
		this.sprite = new Sprite(this.tex);
		this.interpolate = movement;
		this.type = type;
		this.id = Utility.createUniqueID(this);
	}

	public void draw(SpriteBatch batch){
		sprite.setPosition(curPos.x, curPos.y);
		batch.draw(tex, curPos.x, curPos.y);
	}

	public void drawStatic(SpriteCache cache){
		sprite.setPosition(curPos.x, curPos.y);
		cache.add(sprite);
	}

	public void addKeyframe(Keyframe key){
		keyframes.add(key);
	}

	public void removeKeyframe(Keyframe key){
		keyframes.removeValue(key, false);
	}

	public void moveToNextFrame(){
		Keyframe next = getNextFrame();
		curPos.set(next.vector);
	}

	public Keyframe getNextFrame(){
		if(type == Type.STATIC)
			return keyframes.get(0);
		else
			return keyframes.get(currentFrame.getAndIncrement());
	}

	public int getFrameNumber(){
		return currentFrame.get();
	}

	public Vector2 getCurrentPosition(){
		return curPos;
	}

	public void setCurrentPosition(Vector2 pos){
		curPos.set(pos);
	}

	public void setCurrentPosition(float x, float y){
		curPos.set(x, y);
	}

	public void setMovement(Interpolate movement){
		this.interpolate = movement;
	}

	public Interpolate getMovement(){
		return interpolate;
	}

	protected void interpolateMovements(){
		if(type == Type.STATIC) return;

		Array<Keyframe> tmp = new Array<Keyframe>();

		while(keyframes.size != 0){
			Keyframe first = keyframes.get(0);
			Keyframe second = keyframes.get(1);
			tmp.add(first);

			int x = NumberUtils.floatToIntBits(first.x)+1;		//Get Initial X
			int finalX = NumberUtils.floatToIntBits(second.x);	//Get Final X
			int deltaX = Maths.abs(finalX - x);					//Get Distance Between Them

			while(x < finalX){			//Fill Keyframes Between the two
				keyframes.add(new Keyframe(x, interpolate.apply(x/deltaX)));
				x++;
			}

			keyframes.removeIndex(0);
			keyframes.removeIndex(1);
		}

		keyframes.clear();
		keyframes.addAll(tmp);
	}

	@Override
	public void dispose(){
		tex.dispose();
		sprite = null;
		curPos = Vector2.Zero;
		keyframes.clear();
		currentFrame = null;
		interpolate = null;
		Utility.releaseUniqueID(id);
	}

	@Override
	public boolean equals(Object o){
		if(o instanceof Actor){

			if(((Actor) o).id == id) return true;

		}

		return false;
	}

}
