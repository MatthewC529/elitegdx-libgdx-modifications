package com.treehouseelite.elitegdx.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.LifecycleListener;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.sun.corba.se.impl.io.ObjectStreamClass;
import com.treehouseelite.elitegdx.Logger;
import com.treehouseelite.elitegdx.Manager;
import com.treehouseelite.elitegdx.Organizer;
import com.treehouseelite.elitegdx.Organizer.Encryption;
import com.treehouseelite.elitegdx.ai.pathfinding.Node;
import com.treehouseelite.elitegdx.entity.EntityDeathMessage;
import com.treehouseelite.elitegdx.entity.phys.PhysicsUpdateHook;
import com.treehouseelite.elitegdx.entity.phys.PhysicsUpdateTask;
import com.treehouseelite.elitegdx.ex.UnimplementedException;
import com.treehouseelite.elitegdx.util.event.ProteusTask;

/**
 * A Variety of Utilities <br />
 * <br />
 * Ranging from Low Level Modifications to Simple Conversions. <br />
 * This is likely to be expanded as extra Miscellaneous utilities are written.
 * @author Matthew Crocco
 */
public final class Utility {

	/**Currently Supported Hook Types<br />
	 * <br />
	 * <b><u>Currently Supported Operations: </u></b>
	 * <ul>
	 * <li><u><i>SHUTDOWN</i></u> - Add the runnable as a Shutdown Hook, forcing it to run at Termination</li>
	 * <li><u><i>GDX_RENDER</i></u> - Post to run after the next Render() call. IT IS ONLY CALLED ONCE! Unless it constantly posts itself!</li>
	 * <li><u><i>TIMER_TASK</i></u> - Post a Task to the Timer Instance! Proteus Task and RepeatedTask are examples of Implementations! </li>
	 * <li><u><i>GDX_LIFECYCLE</i></u> - Post a Listener to the GDX Lifecycle. You can either do this using the automatic methods or using ProteusTask! </li>
	 * <li><u><u>ELITE_PHYSICS</i></u> - Hook on to the Physics Update Loop. You can do this using PhysicsUpdateHook which implements Runnable! </li>
	 * </ul>*/
	public enum Hook{
		SHUTDOWN(true),
		TIMER_TASK(false),
		ELITE_PHYSICS(true),
		GDX_LIFECYCLE(true),
		GDX_RENDER(false);

		public final boolean removable;

		private Hook(boolean removable){
			this.removable = removable;
		}

		public boolean isRemovable(){
			return removable;
		}
	}

	private Utility(){}

	/**
	 * Convert A Collection to a LibGDX Array Object.<br />
	 * <br />
	 * Collection<? extends T> -> Array<T>
	 * @param c
	 * @return
	 */
	public static <T> Array<T> toArrayGDX(Collection<? extends T> c){
		Array<T> arr = new Array<T>();
		Iterator<? extends T> iter = c.iterator();

		while(iter.hasNext()){
			arr.add(iter.next());
		}

		return arr;
	}

	/**
	 * Crate a LibGDX Array with the given Element Array.
	 * @param arr
	 * @return
	 */
	public static <T> Array<T> toArrayGDX(T... arr){
		return new Array<T>(arr);
	}

	/**
	 * Attach a Thread Hook onto the given "Hook"-able process. Or at least what is currently supported.
	 * @param hook -- Runnable Process
	 * @param place -- Type of Hook (As Is Supported)
	 * @param threadName -- Name of Thread
	 */
	public static void putHook(Runnable hook, Hook place, String threadName){
		switch(place){
		case SHUTDOWN:
			if(threadName != null) 
				Runtime.getRuntime().addShutdownHook(new Thread(hook, threadName));
			else 
				Runtime.getRuntime().addShutdownHook(new Thread(hook));
			break;
		case GDX_RENDER:
			Gdx.app.postRunnable(hook);
			break;
		case TIMER_TASK:
			if(hook instanceof ProteusTask){
				ProteusTask task = (ProteusTask) hook;
				if(task.seconds != null && task.interval == null){
					Timer.schedule(task, task.seconds);
				}else if(task.seconds != null && task.interval != null){
					Timer.schedule(task, task.seconds, task.interval);
				}
			}else{
				Manager.getLogger();
				Logger.warn("Failure to attach ProteusTask Hook to TIMER_TASK! -- Attaching Runnable to GDX_RENDER!");
				Gdx.app.postRunnable(hook);
			}
			break;
		case GDX_LIFECYCLE:
			if(hook instanceof ProteusTask){
				ProteusTask task = (ProteusTask) hook;
				Gdx.app.addLifecycleListener(task);
			}else{
				Manager.getLogger();
				Logger.warn("Failure to attach ProteusTask Hook to GDX_LIFECYCLE! -- Attaching Runnable to GDX_RENDER!");
				Gdx.app.postRunnable(hook);
			}
			break;
		case ELITE_PHYSICS:
			if(hook instanceof PhysicsUpdateHook)
				PhysicsUpdateTask.addUpdateHook((PhysicsUpdateHook) hook);
			else{
				Manager.getLogger();
				Logger.warn("Failure to hook PhysicsUpdateHook to ELITE_PHYSICS! -- Attaching to GDX_RENDER!");
				Gdx.app.postRunnable(hook);
			}
			break;
		}
	}

	/**
	 * Attach a Thread Hook on to the GDX App Lifecycle
	 * @param listener
	 */
	public static void putHook(LifecycleListener listener){
		Gdx.app.addLifecycleListener(listener);
	}

	/**
	 * Attach a EliteGDX Functional Hook on to the GDX App Lifecycle
	 * @param listener
	 */
	public static <T extends LifecycleListener> void putFunctionalHook(T listener){
		Gdx.app.addLifecycleListener(listener);
	}

	/**
	 * Remove an EliteGDX Functional Hook from the GDX App Lifecycle
	 * @param listener
	 */
	public static <T extends LifecycleListener> void removeSpecificHook(T listener){
		Gdx.app.addLifecycleListener(listener);
	}

	/**
	 * Remove a Thread Hook from a Removable Hook Location
	 * @param hook
	 * @param place
	 * @param name
	 */
	public static void removeHook(Runnable hook, Hook place, String name){
		if(!place.removable){
			Manager.getLogger();
			Logger.alert("Attempt to Remove a Hook that is NOT removable! -- Hook = " + place.name() + "...");
			return;
		}

		switch(place){
		case SHUTDOWN:
			if(name != null){
				Runtime.getRuntime().removeShutdownHook(new Thread(hook, name));
			}else{
				Runtime.getRuntime().removeShutdownHook(new Thread(hook));
			}
			break;
		case GDX_LIFECYCLE:
			if(hook instanceof LifecycleListener)
				Gdx.app.removeLifecycleListener((LifecycleListener)hook);
			break;
		case ELITE_PHYSICS:
			if(hook instanceof PhysicsUpdateHook)
				PhysicsUpdateTask.removeUpdateHook((PhysicsUpdateHook) hook);
			break;
		}
	}

	public static void removeHook(Runnable hook, Hook place){
		removeHook(hook, place, null);
	}

	/**
	 * Remove a LifecycleListener from the GDX App Lifecycle
	 * @param listener
	 */
	public static void removeHook(LifecycleListener listener){
		Gdx.app.removeLifecycleListener(listener);
	}

	/**
	 * Attach a Thread Hook onto the given "Hook"-able process. Or at least what is currently supported. <br />
	 * Defaults to no Thread Name (Since it Should be Optional)
	 * @param hook -- Runnable Process
	 * @param place -- Type of Hook (As Is Supported)
	 */
	public static void putHook(Runnable hook, Hook place){
		putHook(hook, place, null);
	}

	public static Array<Node> createNodeListFromTiledMap(){
		//TODO -- Implement Tiled Map Support and This!
		throw new UnimplementedException("Tiled Map Support is Not Implemented in " + Manager.LIB_NAME + " v" + Manager.LIB_VERSION + " At This Time!");
	}

	/**
	 * Use provided Sun classes to generate a Serial Version UID, equivalent to using the java "serialver" command.
	 * @param clazz -- Class to get the SerialVersionUID for
	 * @return Unique SerialVersionUID
	 */
	public static long generateSerialVersionUID(Class<?> clazz){
		return ObjectStreamClass.getActualSerialVersionUID(clazz);
	}

	/**
	 * Beg The Almighty Garbage Collector Overlord to Do a Run... Your Request is NOT Guaranteed to be Answered... <br />
	 * Calls Runtime.getRuntime.gc(); //Equivalent to System.gc();
	 */
	public static void collectGarbage(){
		Runtime.getRuntime().gc();
	}

	public static long createUniqueID(Object o){
		return Manager.getOrganizer().getUniqueID(o);
	}

	public static void releaseUniqueID(long id){
		Manager.getOrganizer().receiveMessage(new EntityDeathMessage(id));
	}

	/**
	 * Attempts to Generate A Truly Unique ID. <br />
	 * <br />
	 * The "quick" method is using the Java UUID class to create a random UUID as a Unique ID. <br />
	 * The more expensive method is using SecureRandom, MessageDigest and Hexadecimal Encoding to create a somewhat readable Unique ID. <br />
	 * In Most Cases, this should be unnecessary. In some cases, Objects need extremely specific ID's and so this exists as a non-recommended, but available, option.
	 * @param quick -- Conduct the Faster Method using built-in Java classes.
	 * @return Unique ID String. This is a Hexadecimal SHA-1 Encryption if quick = false or a UUID Generated by java if quick = true or the SHA-1 Encryption fails.
	 */
	public static String createSecureUniqueID(boolean quick){
		if(quick){
			return UUID.randomUUID().toString();
		}else{
			try {
				SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");
				String rand = Integer.toString(prng.nextInt());
				MessageDigest digest = MessageDigest.getInstance("SHA-1");
				byte[] result = digest.digest(rand.getBytes());
				return Organizer.encode(result, Encryption.HEXADECIMAL);
			} catch (NoSuchAlgorithmException e) {
				Logger.error("Bad Code in Manager! -- SHA-1 PRNG Algorithm Not Found!");
				Logger.exception(e);
			}

			return Utility.createSecureUniqueID(true);
		}
	}

	/**
	 * Get The Ratio of Used Memory<br />
	 * <br />
	 * 1 - (freeMemory/totalMemory) = Used Memory Ratio
	 * @return
	 */
	public static float getMemoryUsageRatio(){
		Runtime runtime = Runtime.getRuntime();
		return 1 - ((float)runtime.freeMemory() / (float)runtime.totalMemory());
	}

	/**
	 * Get the Percentage of Memory Used <br />
	 * <br />
	 * (1 - (freeMemory/totalMemory))*100 = Percentage Memory Used
	 * @return
	 */
	public static float getMemoryUsagePercentage(){
		return getMemoryUsageRatio() * 100;
	}

	/**
	 * Get the Percentage of Memory Used, Formatted to a Precision of Two, as a String.
	 * @return
	 */
	public static String getMemoryUsage(){
		DecimalFormat df = new DecimalFormat("##.00");
		return df.format(getMemoryUsagePercentage()) + "%";
	}

	/**
	 * Get Bytes of Free Memory
	 * @return
	 */
	public static long getFreeMemory(){
		return Runtime.getRuntime().freeMemory();
	}

	/**
	 * Gets an Instance of the desired class, even if it is a private constructor. Assumes Default!
	 * @param clazz
	 * @return
	 */
	public static <T> T getHiddenObject(Class<?> clazz) {
		T obj = null;
		boolean wasAccessible = true;

		Constructor[] c = clazz.getDeclaredConstructors();
		if(!c[0].isAccessible()){
			c[0].setAccessible(true);
			wasAccessible = false;
		}
		try {
			obj = (T) c[0].newInstance(null);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(!wasAccessible)
			c[0].setAccessible(false);

		return obj;
	}
	
	/**
	 * Gets an Instance of the desired class even if it uses Inaccesible Constructors. <br />
	 * The constructor used is determined by the supplied Parameter Types. Use null for the default constructor. <br />
	 * <br />
	 * If no constructor is found, we attempt to use the default public constructor.
	 * 
	 * @param clazz
	 * @param parameters
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static <T> T getHiddenObject(Class<?> clazz, Class<?>... parameters) throws InstantiationException, IllegalAccessException{
		Constructor[] cons = clazz.getDeclaredConstructors();
		
		try{
			for(Constructor c: cons){
				boolean correct = true;
				Class<?>[] params = c.getParameterTypes();
				
				if(params.length != parameters.length) continue;
				
				if(params.length == 0 && parameters[0] == null){
					c.setAccessible(true);
					return (T) c.newInstance(null);
				}			
				
				for(int i = 0; i < params.length; i++){
					if(!params[i].equals(parameters[i])){
						correct = false;
						break;
					}
				}
				
				if(correct){
					c.setAccessible(true);
					return (T) c.newInstance(parameters);
				}
			}
		}catch(Exception e){
			Manager.getLogger().exception(e);
		}
		
		Manager.getLogger().error("Failure to get Hidden Object of class " + clazz.getName() + ".class! Attempting to return Default Constructor!");
		return (T) clazz.newInstance();
	}

	public static void deflate(FileHandle f, int bufferSize) throws IOException{
		f.file();
		File tmp = File.createTempFile(f.name(), ".tidtmp");
		tmp.deleteOnExit();
		byte[] buffer = new byte[bufferSize];

		InputStream fis = new FileInputStream(f.file());
		OutputStream fos = new FileOutputStream(tmp);
		DeflaterOutputStream dos = new DeflaterOutputStream(fos);

		while((fis.read(buffer)) != -1){
			dos.write(buffer);
		}

		fis.close();
		dos.close();

		fis = new FileInputStream(tmp);
		fos = new FileOutputStream(f.file());

		while((fis.read(buffer)) != -1){
			fos.write(buffer);
		}

		tmp.delete();
		fis.close();
		fos.close();

	}

	public static void inflate(FileHandle f,int bufferSize) throws IOException{
		Array<String> lines = new Array<String>();
		byte[] buffer = new byte[bufferSize];

		InputStream fis = new FileInputStream(f.file());
		FileWriter fw;
		InflaterInputStream iis = new InflaterInputStream(fis);

		while(iis.read(buffer) != -1){
			String str = new String(buffer);
			for(String s: str.split(" "))
				lines.add(s);
		}

		iis.close();
		fis.close();
		fw = new FileWriter(f.file());

		for(int i = 0; i < lines.size; i++){
			if(i % 50 == 0 && i != 0)
				fw.write(lines.get(i) + "\n");
			else
				fw.write(lines.get(i) + " ");
		}

		fw.close();

	}
	
	/**
	 * Gets Mouse Position with Origin at the Bottom Left. (Corrects Gdx.input.getY())
	 * @return
	 */
	public static Vector2 getMousePosition(){
		float x = Gdx.input.getX(), y = Gdx.input.getY();
		y = Gdx.graphics.getHeight()-y;
		
		return new Vector2(x, y);
	}

}
