package com.treehouseelite.elitegdx.util;


/**
 * For all those extra String Utilities!
 * 
 * @author Matthew Crocco (Treehouse Elite)
 * 
 */
public final class StringUtils {

	public static String separator(int length, char sepChar) {
		char[] sep = new char[length];
		for (int i = 0; i < sep.length; i++)
			sep[i] = sepChar;

		return new String(sep);
	}

	public static String separator(int length) {
		return separator(length, '-');
	}

	/**
	 * Attempts to Cleanly Generate a Separator with a length of maxLength, Title of info
	 * and with separator characters of sepChar.
	 * 
	 * @param maxLength
	 *            - Length of Separator
	 * @param info
	 *            - Title of Separator
	 * @param sepChar
	 *            - Character to use as the Separator Line i.e.: '-' in
	 *            "------------TITLE--------------"
	 * @return
	 */
	public static String separator(int maxLength, String info, char sepChar) {

		if (info.length() > maxLength)
			throw new IllegalArgumentException("Separator Length is Smaller than the Provided Info String!");

		char[] setup = new char[maxLength];
		int nonReserved = maxLength - info.length();

		if (nonReserved % 2 != 0)
			nonReserved--;

		for (int i = 0; i < maxLength; i++)
			if (i < nonReserved / 2 || i > nonReserved / 2)
				setup[i] = sepChar;
			else if (i == nonReserved / 2) {
				int r = 0;
				char[] temp = info.toCharArray();
				for (int x = i; x <= i + temp.length; x++) {
					if (x == i + temp.length) {
						i = x;
						break;
					}
					setup[x] = temp[r];
					r++;
				}
			}

		return new String(setup);
	}

	/**
	 * Generates a separator with a Length of MaxLength, a title of info and
	 * with '-' as the separator character. This defaults the separator
	 * character to '-' for you, a slight speed boost.
	 * 
	 * @param maxLength
	 *            - Length of the separator
	 * @param info
	 *            - Title of the separator
	 * @return
	 */
	public static String separator(int maxLength, String info) {
		return separator(maxLength, info, '-');
	}

	public static boolean containsDigits(String str){
		return str.matches(".*\\d+.*");
	}

	public static String makeAcronym(String str, String separator){

		StringBuilder sb = new StringBuilder("");
		String[] bits = str.split(separator);

		for(String s: bits)
			if(s.length() >= 3)
				sb.append(Character.toUpperCase(s.charAt(0)));
			else
				sb.append(Character.toLowerCase(s.charAt(0)));

		return sb.toString();
	}

	public static String replaceAll(String subject, char instance, char replacement){
		char[] chars = subject.toCharArray();
		for(int i = 0; i < chars.length; i++)
			if(chars[i] == instance) chars[i] = replacement;

		return new String(chars);
	}

	public static int getInstancesOf(String identifier, String subject){
		if(!subject.contains(identifier)) return 0;

		int count = 1;

		while(subject.contains(identifier)){
			subject.substring(subject.indexOf(identifier) + identifier.length() + 1);
			count++;
		}

		return count;
	}
}
