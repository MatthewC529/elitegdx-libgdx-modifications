package com.treehouseelite.elitegdx.util;

import java.io.File;
import java.util.Calendar;

public interface Referenceable {

	String getAuthor();
	String getAffiliation();
	String getProjectName();
	String getVersion();
	String getContributors();
	File getLogDirectory();
	boolean isPlayerActive();
	String getCopyright();

}
