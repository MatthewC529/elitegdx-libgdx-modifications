package com.treehouseelite.elitegdx.util.event;

/**Please Use Listeners Where Possible!*/
public interface MessageReceiver {

	void receiveMessage(Message<?> m);

	boolean isTakingMessages();
}
