package com.treehouseelite.elitegdx.util.event;

import com.badlogic.gdx.LifecycleListener;
import com.badlogic.gdx.utils.Timer.Task;

/**
 * A Shape-Shifting Class. <br />
 * Acts as a Runnable, Timer.Task, and LifecycleListener! <br />
 * <br />
 * This allows you to add a Hook onto the LibGDX Lifecycle, LibGDX Render Loop, LibGDX Timer or Program Termination Sequence!
 * <br />
 * Just override the methods as necessary!
 * 
 * @author Matthew Crocco
 */
public abstract class ProteusTask extends Task implements LifecycleListener{

	public final Float seconds;
	public final Float interval;

	public ProteusTask(){
		seconds = null;
		interval = null;
	}

	public ProteusTask(float secondsDelay){
		seconds = secondsDelay;
		interval = null;
	}

	public ProteusTask(float secondsDelay, float waitInterval){
		seconds = secondsDelay;
		interval = waitInterval;
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {

	}

	@Override
	public void run() {

	}

}
