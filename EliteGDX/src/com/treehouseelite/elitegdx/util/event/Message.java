package com.treehouseelite.elitegdx.util.event;

/**Please Use Listeners Where Possible!*/
public interface Message<T> {

	public enum MessageType{
		ENTITY_DEATH,
		ENTITY_SPAWN,
		DISPOSAL,
		CREATION,
		COLLISION,
		MISC;
	}

	T getContent();
	MessageType getMessageType();

}
