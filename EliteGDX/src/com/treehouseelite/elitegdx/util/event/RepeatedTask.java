package com.treehouseelite.elitegdx.util.event;

import com.badlogic.gdx.utils.Timer.Task;
import com.treehouseelite.elitegdx.util.Utility;
import com.treehouseelite.elitegdx.util.Utility.Hook;

/**
 * A Task that extends LibGDX's Task class as well as implementing Runnable <br />
 * This means it can be used in a Thread as well as a Task for LibGDX's Timer. <br />
 * <br />
 * This Task will run every time the designated interval elapses and whenever it is allowed to run.<br />
 * Physics Updates and Statistics Updates run via this class for instance.
 * 
 * @author Matthew Crocco
 *
 */
public abstract class RepeatedTask extends Task implements Runnable{

	private final long waitInterval;
	private long current, target;
	private int repeatCount, currentIteration = 0;

	/**
	 * Run As Soon as the Opportunity is Available! <br />
	 * Useful when using Gdx.app.postRunnable() since it will run as soon as rendering is finished.
	 */
	public RepeatedTask(){
		this(1);
	}

	/**
	 * Run Everytime the Specified Interval Elapses from Last Run and when Opportunity is Available! <br />
	 * If called, this task will not run if the interval has not elapsed.<br />
	 * @param intervalMillis - Time to wait in Milliseconds
	 */
	public RepeatedTask(long intervalMillis){
		waitInterval = intervalMillis;
		repeatCount = -1;

		current = System.currentTimeMillis();
		target = current + waitInterval;
	}

	/**
	 * Repeat This Task repeatCount amount of Times when the Interval has Elapsed and when the Opportunity is Available! <br />
	 * If called, this task will not run if the interval has not elapsed. <br />
	 * A failed run will NOT count towards an iteration. <br />
	 * 
	 * @param intervalMillis
	 * @param repeatCount
	 */
	public RepeatedTask(long intervalMillis, int repeatCount){
		this(intervalMillis);
		this.repeatCount = repeatCount;
	}

	@Override
	public void run(){
		if((current = System.currentTimeMillis()) >= target && canContinue()){
			process();
			currentIteration += (repeatCount != -1 ? 1 : 0);
			target = current + waitInterval;
		}

		if(canContinue()){
			Utility.putHook(this, Hook.GDX_RENDER);
		}
	}

	private boolean canContinue(){
		if(repeatCount == -1) return true;

		if(currentIteration < repeatCount) return true;
		else return false;
	}

	public abstract void process();

}

