package com.treehouseelite.elitegdx.util.event;

import com.badlogic.gdx.LifecycleListener;

public interface DisposalListener extends LifecycleListener{

	@Override
	void dispose();

	@Override
	void pause();

	@Override
	void resume();

}
