package com.treehouseelite.elitegdx.util;

import java.util.AbstractMap; 
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import lombok.EqualsAndHashCode;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;

public final @EqualsAndHashCode(callSuper = false) class Colour extends Color {

	/** rgba8888 value of the colour */
	private int val8;

	/**
	 * Red, Green, Blue and Alpha components of this Colour, indices
	 * corresponding to r, g, b, and a respectively.
	 */
	private float[] components = new float[4];

	public ColourComponentMap componentMap;

	/**
	 * An adaptation of the LibGDX Color class that provides extra functionality
	 * with instant access to rgba8888 values and comonents. <br />
	 * More importantly, it provides hex support.
	 */
	public Colour() {
		super(Color.WHITE);
		deriveValues();
	}

	/**
	 * An adaptation of the LibGDX Color class that provides extra functionality
	 * with instant access to rgba8888 values and comonents. <br />
	 * More importantly, it provides hex support.
	 */
	public Colour(float r, float g, float b, float a) {
		super(r, g, b, a);
		deriveValues();
	}

	/**
	 * An adaptation of the LibGDX Color class that provides extra functionality
	 * with instant access to rgba8888 values and comonents. <br />
	 * More importantly, it provides hex support.
	 */
	public Colour(float r, float g, float b) {
		super(r, g, b, 1.0f);
		deriveValues();
	}

	/**
	 * An adaptation of the LibGDX Color class that provides extra functionality
	 * with instant access to rgba8888 values and comonents. <br />
	 * More importantly, it provides hex support.
	 */
	public Colour(String hex, float a) {
		super(convertHex(hex, a));
	}

	/**
	 * An adaptation of the LibGDX Color class that provides extra functionality
	 * with instant access to rgba8888 values and comonents. <br />
	 * More importantly, it provides hex support.
	 */
	public Colour(String hex) {
		super(convertHex(hex, 1.0f));
	}

	/** Derives the rgba8888 value and the red, blue, green, and alpha values */
	private void deriveValues() {
		val8 = Color.rgba8888(this);
		components[0] = this.r;
		components[1] = this.g;
		components[2] = this.b;
		components[3] = this.a;
		componentMap = new ColourComponentMap(this);
	}

	public int getRGBA8888(){
		return val8;
	}

	public int getVal8(){
		return val8;
	}

	/**
	 * Converts hex string into its r g b components and attaches the given
	 * alpha value to derive an rgba8888 integer value
	 */
	private static int convertHex(String hex, float a) {
		StringBuilder sb = new StringBuilder(hex);

		if (hex.contains("#")) {
			sb.deleteCharAt(sb.indexOf("#"));
			hex = new String(sb);
			hex.trim();
		}

		float r = Integer.parseInt(hex.substring(0, 2), 16) / 255f;
		float g = Integer.parseInt(hex.substring(2, 4), 16) / 255f;
		float b = Integer.parseInt(hex.substring(4, 6), 16) / 255f;

		return rgba8888(r, g, b, a);
	}

	private class ColourComponentMap extends AbstractMap<String, Float>{

		private Set<java.util.Map.Entry<String, Float>> set = new EnterSet<java.util.Map.Entry<String, Float>>();

		public ColourComponentMap(Colour c){
			set.add(new ColourEntry("r", c.r));
			set.add(new ColourEntry("g", c.g));
			set.add(new ColourEntry("b", c.b));
			set.add(new ColourEntry("a", c.a));
		}

		@Override
		public java.util.Set<java.util.Map.Entry<String, Float>> entrySet() {
			return set;
		}

		@SuppressWarnings("hiding")
		private class EnterSet<ColourEntry> extends AbstractSet<ColourEntry>{

			private Array<ColourEntry> vals = new Array<ColourEntry>();

			@Override
			public boolean add(ColourEntry e){
				if(vals.contains(e, false)) return false;
				else{
					vals.add(e);
					return true;
				}
			}

			@SuppressWarnings("unchecked")
			@Override
			public boolean remove(Object o){
				if(getClass() != o.getClass()) return false;

				try{
					vals.removeValue((ColourEntry)o, false);
					return true;
				}catch(Exception e){
					return false;
				}
			}

			@Override
			public boolean removeAll(Collection<?> c){
				try{
					for(Object o: c){
						remove(o);
					}
					return true;
				}catch(Exception e){
					return false;
				}
			}

			@Override
			public boolean retainAll(Collection<?> c){
				try{
					for(ColourEntry ce: vals){
						if(!c.contains(ce)){
							vals.removeValue(ce, false);
						}
					}
					return true;
				}catch(Exception e){
					return false;
				}
			}

			@Override
			public ColourEntry[] toArray(){
				return vals.toArray();
			}

			@Override
			public <T> T[] toArray(T[] arr){
				return vals.toArray(arr.getClass());
			}

			@Override
			public void clear(){
				vals.clear();
			}

			@Override
			public String toString(){
				return String.format("[ %s %s %s ]", vals.get(0), vals.get(1), vals.get(2), vals.get(3));
			}

			@Override
			public Iterator<ColourEntry> iterator() {
				return new Iterator<ColourEntry>() {

					int index = 0;
					int getdex = 0;

					@Override
					public boolean hasNext() {
						try{
							if(vals.get(index) != null){
								index++;
								return true;
							}else{
								index = 0;
								getdex = 0;
								return false;
							}
						}catch(Exception e){
							return false;
						}catch(Error e){
							return false;
						}
					}

					@Override
					public ColourEntry next() {
						ColourEntry ce = vals.get(getdex);
						getdex++;
						return ce;
					}

					@Override
					public void remove() {
						
					}
				};
			}

			@Override
			public int size() {
				return vals.size;
			}

		}

		private class ColourEntry implements Map.Entry<String, Float>{

			private final String key;
			private Float val;

			public ColourEntry(String k, Float v){
				this.key = k;
				this.val = v;
			}

			@Override
			public String getKey() {
				return key;
			}

			@Override
			public Float getValue() {
				return val;
			}

			@Override
			public Float setValue(Float value) {
				return (val = value);
			}

			@Override
			public String toString(){
				return String.format("%s=%s", key, val);
			}

			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + getOuterType().hashCode();
				result = prime * result + ((key == null) ? 0 : key.hashCode());
				result = prime * result + ((val == null) ? 0 : val.hashCode());
				return result;
			}

			@Override
			public boolean equals(Object obj) {
				if (this == obj)
					return true;
				if (obj == null)
					return false;
				if (getClass() != obj.getClass())
					return false;
				ColourEntry other = (ColourEntry) obj;
				if (!getOuterType().equals(other.getOuterType()))
					return false;
				if (key == null) {
					if (other.key != null)
						return false;
				} else if (!key.equals(other.key))
					return false;
				if (val == null) {
					if (other.val != null)
						return false;
				} else if (!val.equals(other.val))
					return false;
				return true;
			}

			private ColourComponentMap getOuterType() {
				return ColourComponentMap.this;
			}


		}
	}


}
