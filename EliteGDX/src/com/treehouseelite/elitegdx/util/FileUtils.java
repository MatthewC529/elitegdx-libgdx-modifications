package com.treehouseelite.elitegdx.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import lombok.Cleanup;

import com.badlogic.gdx.utils.Array;
import com.treehouseelite.elitegdx.Logger;

public class FileUtils {

	/**
	 * Simply scans through a file and puts every lines into a String array.
	 * 
	 * @param file
	 *            - The file to scan through
	 * @param ignoreBlankLines
	 *            - Whether or not blank lines should be ignored
	 * @return String array of all of the files lines.
	 */
	public static Array<String> lineArray(File file, boolean ignoreBlankLines) {
		try {
			Array<String> temp = new Array<String>();
			Scanner intern = new Scanner(file);
			
			while (intern.hasNextLine()) {
				String s = intern.nextLine();
				if (!s.isEmpty() && ignoreBlankLines)
					temp.add(s);
				else
					temp.add(s);
			}
			intern.close();
			return temp;
		} catch (Exception e) {
			e.printStackTrace();
			Logger.dev("Surprise! Couldnt make a Line Array!");
			Logger.exception(e);
		}

		return null;
	}

	/**
	 * Simply scans through the file and puts every line into a string array.
	 * This method defaults to ignore blank lines.
	 * 
	 * @param file
	 *            - File to scan through
	 * @return String array of all of the files lines
	 */
	public static Array<String> lineArray(File file) {
		return lineArray(file, true);
	}

	/**
	 * Returns the length of file in lines
	 * 
	 * @param file - File to scan through
	 * @return Length of file as an integer
	 * @throws IOException
	 */
	public static int getFileLength(File file) throws IOException {
		@Cleanup InputStream is = new BufferedInputStream(new FileInputStream(file));
		byte[] c = new byte[1024];
		int count = 0;
		int chars = 0;
		boolean endsWithNewLine = true;
		while((chars = is.read(c)) != -1){
			for(int i = 0; i < chars; i++){
				if(c[i] == '\n') count++;
				endsWithNewLine = (c[chars-1] == '\n');
			}
		}

		if(!endsWithNewLine) count++;
		return count;
	}
}
