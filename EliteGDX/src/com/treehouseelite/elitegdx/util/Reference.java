package com.treehouseelite.elitegdx.util;


public class Reference {

	public static final String AUTHOR = "Matthew Crocco";
	public static final String ASSOCIATIVE_NAME = "Object Oriented Games";
	public static final String LOGGING_SYSTEM = "EliteGDX Logger";
	public static final String LOGGING_VERSION = "2.1a";
	public static final String COPYRIGHT = ASSOCIATIVE_NAME + " (c) 2014";

}
