package com.treehouseelite.elitegdx.util;


/**
 * A Very Quick Statistics Interface -- Will Be Improved
 * @author Matthew Crocco
 *
 */
public interface Analyzable {

	String getPrintableStatistics();

}
