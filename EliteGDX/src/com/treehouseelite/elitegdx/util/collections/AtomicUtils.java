package com.treehouseelite.elitegdx.util.collections;

import java.util.concurrent.atomic.AtomicReferenceArray;

import com.badlogic.gdx.utils.Array;

public final class AtomicUtils {

	private AtomicUtils(){}

	/**
	 * Get the Index of The Object in the AtomicReferenceArray
	 * @param objs
	 * @param o
	 * @return
	 */
	public static int getIndexOf(AtomicReferenceArray<?> objs, Object o){
		for(int i = 0; i < objs.length(); i++){
			if(objs.get(i) == o) return i;
		}
		return -1;
	}

	/**
	 * Remove all Nulls Between Values
	 * @param objs
	 * @return
	 */
	public static <T> AtomicReferenceArray<T> deflate(AtomicReferenceArray<T> objs){
		Array<T> temp = new Array<T>();
		int skips = 0;

		for(int i = 0; i < objs.length(); i++){
			if(objs.get(i) != null)
				temp.set(i - skips, objs.getAndSet(i, null));
			else
				skips++;
		}

		for(int i = 0; i < temp.size; i++)
			objs.set(i, temp.get(i));

		return objs;
	}

	/**
	 * Condense values in array, make one complete line of values. <br />
	 * Remove all Nulls between Values.
	 * @param objs
	 * @return
	 */
	public static AtomicReferenceArray<Object> deflate_obj(AtomicReferenceArray<Object> objs){
		Array<Object> temp = new Array<Object>();
		int skips = 0;
		for(int i = 0; i < objs.length(); i++){
			if(objs.get(i) != null)
				temp.set(i - skips, objs.getAndSet(i, null));
			else
				skips++;
		}

		for(int i = 0; i < temp.size; i++)
			objs.set(i, temp.get(i));


		return objs;
	}

	/**
	 * Returns the Index of the Last Consecutive Non-Null Value.<br />
	 * <br />
	 * 1. If the Array is Empty and Index = 0 is Null then return 0
	 * 2. If a non-zero end point is found, return index
	 * 3. If no end point is found, return -1, Array is Full!
	 * @param objs
	 * @return
	 */
	public static int determineEndPointOfValues(AtomicReferenceArray<?> objs){
		for(int i = 0; i < objs.length(); i++){
			if(objs.get(i) == null){
				if(i == 0) return 0;
				return i-1;
			}
		}

		return -1;
	}

}
