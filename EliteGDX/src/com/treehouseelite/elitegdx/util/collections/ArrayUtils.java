package com.treehouseelite.elitegdx.util.collections;

import java.util.Arrays;
import java.util.Comparator;

import com.badlogic.gdx.utils.reflect.ArrayReflection;

public class ArrayUtils {

	/**
	 * Shrinks or Expands an Array to a Given Size.
	 * @param arr
	 * @param newSize
	 * @return
	 */
	public static <T> T[] resizeArray(T[] arr, int newSize){
		T[] temp = (T[]) ArrayReflection.newInstance(arr.getClass().getComponentType(), newSize);

		if(newSize < arr.length)
			System.arraycopy(arr, 0, temp, 0, temp.length);
		else
			System.arraycopy(arr, 0, temp, 0, arr.length);

		return temp;
	}

	/**
	 * Creates and returns an exact copy of this array
	 * @param arr
	 * @return
	 */
	public static <T> T[] copyArray(T[] arr){
		T[] temp = (T[]) ArrayUtils.newArray(arr.getClass().getComponentType(), arr.length);
		System.arraycopy(arr, 0, temp, 0, arr.length);
		return temp;
	}

	/**
	 * Creates and returns an exact copy of this multi-dimensional array
	 * @param arr
	 * @return
	 */
	public static <T> T[][] copyTwoDimensionalArray(T[][] arr){
		T[][] temp = (T[][])ArrayUtils.newArray(arr.getClass().getComponentType(), arr.length, arr[0].length);

		for(int i = 0; i < temp.length; i++)
			System.arraycopy(arr[i], 0, temp[i], 0, arr[0].length);

		return temp;
	}

	/**
	 * Condense Array by Removing Any Null Values
	 * @param arr
	 * @return
	 */
	public static <T> T[] condenseArray(T[] arr){
		T[] temp = (T[]) ArrayReflection.newInstance(arr.getClass().getComponentType(), arr.length - countOf(arr, null, true));
		int cur = 0;

		for (T o : arr) {
			if(o != null){
				temp[cur] = o;
				cur++;
			}
		}

		return temp;
	}

	public static <T> T[][] condenseTwoDimensionalArray(T[][] arr){
		T[][] temp = ArrayUtils.copyTwoDimensionalArray(arr);

		for(int i = 0 ; i < temp.length; i++){
			temp[i] = ArrayUtils.condenseArray(temp[i]);
		}

		return temp;
	}

	/**
	 * Number of Instances of o in arr.
	 * @param arr
	 * @param o
	 * @param identity -- If true, compare using "==", else use ".equals()"
	 * @return
	 */
	public static int countOf(Object[] arr, Object o, boolean identity){
		int count = 0;

		for(Object targ: arr){
			if(!identity && o.equals(targ)) count++;
			if(identity && o == targ) count++;
		}

		return count;
	}

	/**
	 * Create a New Array of the given Type
	 * @param type
	 * @param size
	 * @return
	 */
	public static Object newArray(Class<?> type, int size){
		return java.lang.reflect.Array.newInstance(type, size);
	}

	/**
	 * Create a New Multi-Dimensional Array of the Given Type and Dimensions
	 * @param type
	 * @param dimensions
	 * @return
	 */
	public static Object newArray(Class<?> type, int... dimensions){
		return java.lang.reflect.Array.newInstance(type, dimensions);
	}
	
	/**
	 * A Single Pivot Quick Sort Algorithm <br />
	 * <br />
	 * Will default to {@link Arrays#sort(byte[])} if Items exceeds 300,000. <br />
	 * Will default to {@link Arrays#parallelSort(byte[])} of Items exceeds 1,000,000 <br />
	 * @param arr
	 */
	public static void quickSort(byte[] arr){
		if(arr.length > 300000){
			Arrays.sort(arr);
			return;
		}
		
		Sort.quickSort(arr, 0, arr.length-1);	
	}
	
	/**
	 * A Single Pivot Quick Sort Algorithm <br />
	 * <br />
	 * Will default to {@link Arrays#sort(char[])} if Items exceeds 300,000. <br />
	 * Will default to {@link Arrays#parallelSort(char[])} of Items exceeds 1,000,000 <br />
	 * @param arr
	 */
	public static void quickSort(char[] arr){
		if(arr.length > 300000){
			Arrays.sort(arr);
			return;
		}
		
		Sort.quickSort(arr, 0, arr.length-1);	
	}
	
	/**
	 * A Single Pivot Quick Sort Algorithm <br />
	 * <br />
	 * Will default to {@link Arrays#sort(short[])} if Items exceeds 300,000. <br />
	 * Will default to {@link Arrays#parallelSort(short[])} of Items exceeds 1,000,000 <br />
	 * @param arr
	 */
	public static void quickSort(short[] arr){
		if(arr.length > 300000){
			Arrays.sort(arr);
			return;
		}
		
		Sort.quickSort(arr, 0, arr.length-1);	
	}
	
	/**
	 * A Single Pivot Quick Sort Algorithm <br />
	 * <br />
	 * Will default to {@link Arrays#sort(int[])} if Items exceeds 300,000. <br />
	 * Will default to {@link Arrays#parallelSort(int[])} of Items exceeds 1,000,000 <br />
	 * @param arr
	 */
	public static void quickSort(int[] arr){
		if(arr.length > 300000){
			Arrays.sort(arr);
			return;
		}
		
		Sort.quickSort(arr, 0, arr.length-1);	
	}
	
	/**
	 * A Single Pivot Quick Sort Algorithm <br />
	 * <br />
	 * Will default to {@link Arrays#sort(long[])} if Items exceeds 300,000. <br />
	 * Will default to {@link Arrays#parallelSort(long[])} of Items exceeds 1,000,000 <br />
	 * @param arr
	 */
	public static void quickSort(long[] arr){
		if(arr.length > 300000){
			Arrays.sort(arr);
			return;
		}
		
		Sort.quickSort(arr, 0, arr.length);
	}
	
	/**
	 * A Single Pivot Quick Sort Algorithm <br />
	 * <br />
	 * Will default to {@link Arrays#sort(float[])} if Items exceeds 300,000. <br />
	 * Will default to {@link Arrays#parallelSort(float[])} of Items exceeds 1,000,000 <br />
	 * @param arr
	 */
	public static void quickSort(float[] arr){
		if(arr.length > 300000){
			Arrays.sort(arr);
			return;
		}
		
		Sort.quickSort(arr, 0, arr.length-1);	
	}
	
	/**
	 * A Single Pivot Quick Sort Algorithm <br />
	 * <br />
	 * Will default to {@link Arrays#sort(double[])} if Items exceeds 300,000. <br />
	 * Will default to {@link Arrays#parallelSort(double[])} of Items exceeds 1,000,000 <br />
	 * @param arr
	 */
	public static void quickSort(double[] arr){
		if(arr.length > 300000){
			Arrays.sort(arr);
			return;
		}
		
		Sort.quickSort(arr, 0, arr.length-1);	
	}
	
	/**
	 * Sort an Array Of Comparable Objects using a Timsort Algorithm
	 * @param arr
	 */
	public static <T extends Comparable<T>> void sort(T[] arr){
		Arrays.sort(arr);
	}
	
	/**
	 * Sort an Array Of Comparable Objects using a Timsort Algorithm
	 * @param arr
	 * @param comparator
	 */
	public static <T> void sort(T[] arr, Comparator<? super T> comparator){
		Arrays.sort(arr, comparator);
	}
	
	/**
	 * A Shell Sort Algorithm <br />
	 * <br />
	 * Will default to {@link ArrayUtils#quickSort(byte[])} if Items exceeds 5000.
	 * @param arr
	 */
	public static void sort(byte[] arr){
		if(arr.length > 5000){
			ArrayUtils.quickSort(arr);
			return;
		}
		
		Sort.shellSort(arr);
	}
	
	/**
	 * A Shell Sort Algorithm <br />
	 * <br />
	 * Will default to {@link ArrayUtils#quickSort(char[])} if Items exceeds 5000.
	 * @param arr
	 */
	public static void sort(char[] arr){
		if(arr.length > 5000){
			ArrayUtils.quickSort(arr);
			return;
		}
		
		Sort.shellSort(arr);
	}
	
	/**
	 * A Shell Sort Algorithm <br />
	 * <br />
	 * Will default to {@link ArrayUtils#quickSort(short[])} if Items exceeds 5000.
	 * @param arr
	 */
	public static void sort(short[] arr){
		if(arr.length > 5000){
			ArrayUtils.quickSort(arr);
			return;
		}
		
		Sort.shellSort(arr);
	}
	
	/**
	 * A Shell Sort Algorithm <br />
	 * <br />
	 * Will default to {@link ArrayUtils#quickSort(int[])} if Items exceeds 5000.
	 * @param arr
	 */
	public static void sort(int[] arr){
		if(arr.length > 5000){
			ArrayUtils.quickSort(arr);
			return;
		}
		
		Sort.shellSort(arr);
	}
	
	/**
	 * A Shell Sort Algorithm <br />
	 * <br />
	 * Will default to {@link ArrayUtils#quickSort(long[])} if Items exceeds 5000.
	 * @param arr
	 */
	public static void sort(long[] arr){
		if(arr.length > 5000){
			ArrayUtils.quickSort(arr);
			return;
		}
		
		Sort.shellSort(arr);
	}
	
	/**
	 * A Shell Sort Algorithm <br />
	 * <br />
	 * Will default to {@link ArrayUtils#quickSort(float[])} if Items exceeds 5000.
	 * @param arr
	 */
	public static void sort(float[] arr){
		if(arr.length > 5000){
			ArrayUtils.quickSort(arr);
			return;
		}
		
		Sort.shellSort(arr);
	}
	
	/**
	 * A Shell Sort Algorithm <br />
	 * <br />
	 * Will default to {@link ArrayUtils#quickSort(double[])} if Items exceeds 5000.
	 * @param arr
	 */
	public static void sort(double[] arr){
		if(arr.length > 5000){
			ArrayUtils.quickSort(arr);
			return;
		}
		
		Sort.shellSort(arr);
	}
}

class Sort{
	
	static void shellSort(int[] arr){
		if(arr.length > 5000){
			ArrayUtils.quickSort(arr);
			return;
		}
		
		int inner, outer;
		int temp;
		int h = 1;
		
		while(h <= arr.length / 3)
			h = h * 3 + 1;
		
		while(h > 0){
			for(outer = h; outer < arr.length; outer++){
				temp = arr[outer];
				inner = outer;
				while(inner > h - 1 && arr[inner - h] >= temp){
					arr[inner] = arr[inner - h];
					inner -= h;
				}
				arr[inner] = temp;
			}
			
			h = (h-1)/3;
		}
		
		return;
	}
	
	static void shellSort(long[] arr){
		int inner, outer;
		long temp;
		int h = 1;
		
		while(h <= arr.length / 3)
			h = h * 3 + 1;
		
		while(h > 0){
			for(outer = h; outer < arr.length; outer++){
				temp = arr[outer];
				inner = outer;
				while(inner > h - 1 && arr[inner - h] >= temp){
					arr[inner] = arr[inner - h];
					inner -= h;
				}
				arr[inner] = temp;
			}
			
			h = (h-1)/3;
		}
		
		return;
	}
	
	static void shellSort(float[] arr){
		int inner, outer;
		float temp;
		int h = 1;
		
		while(h <= arr.length / 3)
			h = h * 3 + 1;
		
		while(h > 0){
			for(outer = h; outer < arr.length; outer++){
				temp = arr[outer];
				inner = outer;
				while(inner > h - 1 && arr[inner - h] >= temp){
					arr[inner] = arr[inner - h];
					inner -= h;
				}
				arr[inner] = temp;
			}
			
			h = (h-1)/3;
		}
		
		return;
	}
	
	static void shellSort(double[] arr){
		int inner, outer;
		double temp;
		int h = 1;
		
		while(h <= arr.length / 3)
			h = h * 3 + 1;
		
		while(h > 0){
			for(outer = h; outer < arr.length; outer++){
				temp = arr[outer];
				inner = outer;
				while(inner > h - 1 && arr[inner - h] >= temp){
					arr[inner] = arr[inner - h];
					inner -= h;
				}
				arr[inner] = temp;
			}
			
			h = (h-1)/3;
		}
		
		return;
	}
	
	static void shellSort(byte[] arr){
		int inner, outer;
		byte temp;
		int h = 1;
		
		while(h <= arr.length / 3)
			h = h * 3 + 1;
		
		while(h > 0){
			for(outer = h; outer < arr.length; outer++){
				temp = arr[outer];
				inner = outer;
				while(inner > h - 1 && arr[inner - h] >= temp){
					arr[inner] = arr[inner - h];
					inner -= h;
				}
				arr[inner] = temp;
			}
			
			h = (h-1)/3;
		}
		
		return;
	}
	
	static void shellSort(char[] arr){
		int inner, outer;
		char temp;
		int h = 1;
		
		while(h <= arr.length / 3)
			h = h * 3 + 1;
		
		while(h > 0){
			for(outer = h; outer < arr.length; outer++){
				temp = arr[outer];
				inner = outer;
				while(inner > h - 1 && arr[inner - h] >= temp){
					arr[inner] = arr[inner - h];
					inner -= h;
				}
				arr[inner] = temp;
			}
			
			h = (h-1)/3;
		}
		
		return;
	}
	
	static void shellSort(short[] arr){
		int inner, outer;
		short temp;
		int h = 1;
		
		while(h <= arr.length / 3)
			h = h * 3 + 1;
		
		while(h > 0){
			for(outer = h; outer < arr.length; outer++){
				temp = arr[outer];
				inner = outer;
				while(inner > h - 1 && arr[inner - h] >= temp){
					arr[inner] = arr[inner - h];
					inner -= h;
				}
				arr[inner] = temp;
			}
			
			h = (h-1)/3;
		}
		
		return;
	}
	
	static void quickSort(int[] arr, int start, int end){
		if(start < end){
			int x = Partition.partition(arr, start, end);
			quickSort(arr, start, x-1);
			quickSort(arr, x+1, end);
		}
	}
	
	static void quickSort(long[] arr, int start, int end){
		if(start < end){
			int x = Partition.partition(arr, start, end);
			quickSort(arr, start, x-1);
			quickSort(arr, x+1, end);
		}
	}
	
	static void quickSort(float[] arr, int start, int end){
		if(start<end){
			int x = Partition.partition(arr, start, end);
			quickSort(arr, start, x-1);
			quickSort(arr, x+1, end);
		}
	}
	
	static void quickSort(double[] arr, int start, int end){
		if(start<end){
			int x = Partition.partition(arr, start, end);
			quickSort(arr, start, x-1);
			quickSort(arr, x+1, end);
		}
	}
	
	static void quickSort(short[] arr, int start, int end){
		if(start<end){
			int x = Partition.partition(arr, start, end);
			quickSort(arr, start, x-1);
			quickSort(arr, x+1, end);
		}
	}
	
	static void quickSort(byte[] arr, int start, int end){
		if(start<end){
			int x = Partition.partition(arr, start, end);
			quickSort(arr, start, x-1);
			quickSort(arr, x+1, end);
		}
	}
	
	static void quickSort(char[] arr, int start, int end){
		if(start<end){
			int x = Partition.partition(arr, start, end);
			quickSort(arr, start, x-1);
			quickSort(arr, x+1, end);
		}
	}
	
}

class Partition{
	
	static int partition(int[] a, int p, int r){
		int x = a[r];
		int i = p - 1;
		int temp = 0;
		
		for(int j = p; j < r; j++){
			if(a[j] <= x){
				i++;
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
		
		temp = a[i+1];
		a[i + 1] = a[r];
		a[r] = temp;
		return i + 1;
	}
	
	static int partition(long[] a, int p, int r){
		long x = a[r];
		int i = p - 1;
		long temp = 0;
		
		for(int j = p; j < r; j++){
			if(a[j] <= x){
				i++;
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
		
		temp = a[i+1];
		a[i + 1] = a[r];
		a[r] = temp;
		return i + 1;
	}
	
	static int partition(byte[] a, int p, int r){
		int x = a[r];
		int i = p - 1;
		byte temp = 0;
		
		for(int j = p; j < r; j++){
			if(a[j] <= x){
				i++;
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
		
		temp = a[i+1];
		a[i + 1] = a[r];
		a[r] = temp;
		return i + 1;
	}
	
	static int partition(float[] a, int p, int r){
		float x = a[r];
		int i = p - 1;
		float temp = 0;
		
		for(int j = p; j < r; j++){
			if(a[j] <= x){
				i++;
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
		
		temp = a[i+1];
		a[i + 1] = a[r];
		a[r] = temp;
		return i + 1;
	}
	
	static int partition(double[] a, int p, int r){
		double x = a[r];
		int i = p - 1;
		double temp = 0;
		
		for(int j = p; j < r; j++){
			if(a[j] <= x){
				i++;
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
		
		temp = a[i+1];
		a[i + 1] = a[r];
		a[r] = temp;
		return i + 1;
	}
	
	static int partition(short[] a, int p, int r){
		int x = a[r];
		int i = p - 1;
		short temp = 0;
		
		for(int j = p; j < r; j++){
			if(a[j] <= x){
				i++;
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
		
		temp = a[i+1];
		a[i + 1] = a[r];
		a[r] = temp;
		return i + 1;
	}
	
	static int partition(char[] a, int p, int r){
		char x = a[r];
		int i = p - 1;
		char temp = 0;
		
		for(int j = p; j < r; j++){
			if(a[j] <= x){
				i++;
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
		
		temp = a[i+1];
		a[i + 1] = a[r];
		a[r] = temp;
		return i + 1;
	}
	
}
