package com.treehouseelite.elitegdx.util.collections;

import java.util.Objects;

public class ModifiedItem<T> {
	public final T val;
	public final long arrivalTime;

	public ModifiedItem(T val){
		this.val = val;
		arrivalTime = System.nanoTime();
	}

	@Override
	public boolean equals(Object o1){
		if(o1.getClass() != getClass()) return false;

		ModifiedItem item = (ModifiedItem) o1;

		return Objects.equals(item.val, val);
	}
}
