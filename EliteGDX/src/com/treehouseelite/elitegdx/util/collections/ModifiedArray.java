package com.treehouseelite.elitegdx.util.collections;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Objects;
import java.util.Random;

import com.badlogic.gdx.utils.Array;
import com.treehouseelite.elitegdx.math.Maths;

/**
 * An array that is sorted based on the time of arrival of items. <br />
 * The values are wrapped in a ModifiedItem object that consists of the value and its time of arrival (creation time). <br />
 * <br />
 * Unlike Array, which truncates from the back (removing the latest items, this Array truncates from the front (removing the oldest items). <br />
 * The default comparator used to sort is the ModifiedComparator which was designed for this purpose. <br />
 * <br />
 * The comparator was designed so that there is never an "equality case" consistent with the Comparator specification. <br/ >
 * <br />
 * This class is a <b>subclass</> of the <b><i>LibGDX Array</i></b>
 * @author Matthew Crocco
 *
 * @param <T> -- The Reference or Primitive the Array consists of.
 */
public class ModifiedArray<T> extends Array<ModifiedItem<T>> {

	/**The Current Comparator **/
	private Comparator<ModifiedItem> comparator;

	/**
	 * Standard Constructor -- Creates an Empty ModifiedArray with the Default ModifiedComparator Comparator.
	 */
	public ModifiedArray(){
		super();
		comparator = new ModifiedComparator(); 
	}

	/**
	 * VarArg Constructor -- Creates a Pre-Populated ModifiedArray (Consisting of the Provided Vales) with the Default ModifiedComparator Comparator
	 * @param initialVals -- Initial Values to Pre-Populate the Array with.
	 */
	public ModifiedArray(T... initialVals){
		super();
		comparator = new ModifiedComparator();
		addItems(initialVals);
	}

	/**
	 * Collections Constructor -- Creates a Pre-Populated ModifiedArray (Consisting of the Provided Collection Values) with the Default ModifiedComparator Comparator
	 * @param vals -- Initial Values ot Pre-Popualte the Array with.
	 */
	public ModifiedArray(Collection<? extends T> vals){
		super();
		comparator = new ModifiedComparator();
		addItems(vals);
	}

	/**
	 * Add Item to the Array
	 * @param val
	 */
	public void addItem(T val){
		super.add(new ModifiedItem<T>(val));
	}

	/**
	 * Add Multiple Items to the Array
	 * @param vals
	 */
	public void addItems(T... vals){
		for(T val: vals){
			addItem(val);
		}
	}

	/**
	 * Add Values from a Collection to the Array
	 * @param vals
	 */
	public void addItems(Collection<? extends T> vals){
		for(T val : vals)
			addItem(val);
	}

	/**
	 * Get Item at Index
	 * @param index
	 * @return
	 */
	public T getItem(int index){
		return super.get(index).val;
	}

	/**
	 * Get all Items from the indices [initial, fin] (Inclusive)
	 * @param initial -- Initial Index
	 * @param fin -- Final Index
	 * @return Array of Values from Initial -> Final
	 */
	public T[] getItemsInRange(int initial, int fin){
		if(initial > fin) throw new IllegalArgumentException("The Initial of the Range MUST be Less than or Equal To the Final Value of the Range!");
		if(fin > size || initial > size) throw new ArrayIndexOutOfBoundsException();

		Array<T> arr = new Array<T>();

		for(int i = initial; i <= fin; i++){
			arr.add(super.get(i).val);
		}

		return arr.toArray();
	}

	/**
	 * Get all Items from the indices [initial, initial + count] (inclusive)
	 * @param index -- Initial Index
	 * @param count -- Values to Grab from Initial Index
	 * @return
	 */
	public T[] getItems(int index, int count){
		return getItemsInRange(index, index+count);
	}

	/**
	 * Deletes Item from Array -- Does Nothing if Item not Found
	 * @param val
	 */
	public void removeItem(T val){
		ModifiedItem<T> item = new ModifiedItem<T>(val);
		if(!super.contains(item, false)) return;
		super.removeValue(item, false);
	}

	/**
	 * Truncate the Size of the Array moving from the Front to the Back <br />
	 * Opposed to Back to Front (As LibGDX Array does) <br />
	 * <br />
	 * This leaves the latest additions intact.
	 * @param newSize
	 */
	@Override
	public void truncate(int newSize){
		if(size <= newSize) return;
		int trimSize = newSize - size;

		for(int i = 0; i < trimSize; i++){
			items[i] = null;
		}

		Arrays.sort(items, comparator);

		size = newSize;
	}

	/**
	 * Set the Current Comparator
	 * @param comparator
	 */
	public void setComparator(Comparator<ModifiedItem> comparator){
		this.comparator = comparator;
	}

	/**
	 * Force an Array Resort
	 */
	public void forceSort(){
		resort();
	}

	/**
	 * Sort the Array with a different Comparator ONCE
	 * @param comparator
	 */
	public void sortItems(Comparator<ModifiedItem> comparator){
		Arrays.sort(items, comparator);
	}

	private void resort(){
		Arrays.sort(items, comparator);
	}

	/**
	 * The goal of the ModifiedComparator is to sort ModifiedItems, forcing null items to the top of the array <br />
	 * and sorting items by time of arrival. The latest items appear at the top, the earliest at the bottom. <br />
	 * <br />
	 * This was made specifically for the ModifiedArray
	 * @author Matthew Crocco
	 *
	 */
	public class ModifiedComparator implements Comparator<ModifiedItem>{

		/**
		 * A Special Compare Method that guarantees no equalities <br />
		 * Moves Null to the top of the Array while the Earliest are at the bottom and the Latest are below the Null values.
		 */
		@Override
		public int compare(ModifiedItem o1, ModifiedItem o2) {
			if(o1 == null) return 1;
			if(o2 == null) return -1;

			if(o1.arrivalTime > o2.arrivalTime)
				return 1;
			else if(o1.arrivalTime < o2.arrivalTime)
				return -1;

			Random r = new Random();
			r.setSeed(System.currentTimeMillis());
			double random = r.nextInt(100) + 1 * Maths.abs(r.nextGaussian());

			if(random >= 50)
				return 1;
			else
				return -1;

		}
	}

}
