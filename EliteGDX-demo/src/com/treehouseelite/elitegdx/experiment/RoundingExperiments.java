package com.treehouseelite.elitegdx.experiment;

import com.treehouseelite.elitegdx.math.Maths;

public class RoundingExperiments {
	
	public static void main(String[] args){
		System.out.println(Maths.PI);
		System.out.println(Maths.round(Maths.PI, 5));
		System.out.println(Math.PI);
		System.out.println(Maths.round(Math.PI, 5));
	}
	
}
