package com.treehouseelite.elitegdx.experiment;

import com.treehouseelite.elitegdx.math.Interpolate;

public class InterpolationExperiments {
	
	public static void main(String args[]) throws Exception{
		InterpolationExperiments exp = new InterpolationExperiments();
		exp.writeInterpolation();
	}
	
	public void writeInterpolation() throws InterruptedException{
		float end = 100f;
		float start = -100f;
		float cur = 0.1f;
		
		for(int i = 0; i < 1000; i ++){
			start = Interpolate.circle.apply(start, end, cur);

			System.out.println(start +"\t--\t"+cur);
			Thread.sleep(100);
		}
	}
	
}
